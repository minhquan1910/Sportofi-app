# Sportofi-app
# 🚀 Quick Start

📄 Clone project:

```sh
git clone https://github.com/Metapolis21/Sportofi-app.git
```

💿 Install all dependencies:

```sh
npm install
```

✏ Rename `.env.example` to `.env` in the main folder and add API
```
REACT_APP_HOUSE_ADDR= <ADDRESS_CONTRACT>
REACT_APP_PRIVATE_KEY= <PRIVATE_KEY>
```
✏ Automatically update deploy status badge: [![Netlify Status](https://api.netlify.com/api/v1/badges/8d1d80ba-a2ff-4d23-a8c9-6b5ec2e3481b/deploy-status)](https://app.netlify.com/sites/beta-alpha-spotofi/deploys)
````
🚴‍♂️ Run your App:
```sh
npm start
````
