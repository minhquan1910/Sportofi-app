import internationalWhiteIcon from "../assets/Flags/international-white.svg";
import uefaIcon from "../assets/images/uefa.png";
import englandIcon from "../assets/images/england.png";
import spainIcon from "../assets/images/spain.png";
import italyIcon from "../assets/images/italy.png";
import franceIcon from "../assets/images/france.png";
import germanyIcon from "../assets/images/germany.png";

export const DUMMY_ODDS = [
  {
    id: "id1",
    nameCompetion: "FIFA World Cup 2022",
    image: internationalWhiteIcon,
    matchs: [
      {
        idMatch: "id_match1",
        firstCountry: "Qatar",
        secondCountry: "Ecuador",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType111",
            nameOdd: "Qatar",
            odds: 3.55,
          },
          {
            idMatchType: "id_matchType112",
            nameOdd: "Draw",
            odds: 3.3,
          },
          {
            idMatchType: "id_matchType113",
            nameOdd: "Ecuador",
            odds: 2.08,
          },
        ],
      },
      {
        idMatch: "id_match2",
        firstCountry: "England",
        secondCountry: "Iran",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType121",
            nameOdd: "England",
            odds: 1.32,
          },
          {
            idMatchType: "id_matchType122",
            nameOdd: "Draw",
            odds: 4.9,
          },
          {
            idMatchType: "id_matchType123",
            nameOdd: "Iran",
            odds: 11.0,
          },
        ],
      },
      {
        idMatch: "id_match3",
        firstCountry: "Senegal",
        secondCountry: "Netherlands",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType131",
            nameOdd: "Senegal",
            odds: 5.6,
          },
          {
            idMatchType: "id_matchType132",
            nameOdd: "Draw",
            odds: 3.8,
          },
          {
            idMatchType: "id_matchType133",
            nameOdd: "Netherlands",
            odds: 1.64,
          },
        ],
      },
      {
        idMatch: "id_match4",
        firstCountry: "USA",
        secondCountry: "Wales",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType141",
            nameOdd: "USA",
            odds: 2.5,
          },
          {
            idMatchType: "id_matchType142",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType143",
            nameOdd: "Wales",
            odds: 2.9,
          },
        ],
      },
    ],
  },
  {
    id: "id2",
    nameCompetion: "UEFA Champions League",
    image: uefaIcon,
    matchs: [
      {
        idMatch: "id_match1",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType211",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType212",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType213",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match2",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType221",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType222",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType223",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match3",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType231",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType232",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType233",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match4",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType241",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType242",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType243",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match5",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType251",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType252",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType253",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match6",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match7",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match8",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
    ],
  },
  {
    id: "id3",
    nameCompetion: "UEFA Europa League",
    image: uefaIcon,
    matchs: [
      {
        idMatch: "id_match1",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match2",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match3",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match4",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match5",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match6",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match7",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match8",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
    ],
  },
  {
    id: "id4",
    nameCompetion: "Premier League",
    image: englandIcon,
    matchs: [
      {
        idMatch: "id_match9",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match10",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match11",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match12",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match13",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match14",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match15",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match16",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
    ],
  },
  {
    id: "id5",
    nameCompetion: "LaLiga",
    image: spainIcon,
    matchs: [
      {
        idMatch: "id_match1",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match2",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match3",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match4",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match5",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match6",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match7",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match8",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
    ],
  },
  {
    id: "id6",
    nameCompetion: "Serie A",
    image: italyIcon,
    matchs: [
      {
        idMatch: "id_match1",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match2",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match3",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match4",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match5",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match6",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match7",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match8",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
    ],
  },
  {
    id: "id7",
    nameCompetion: "Ligue 1",
    image: franceIcon,
    matchs: [
      {
        idMatch: "id_match1",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match2",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match3",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match4",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match5",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match6",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match7",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match8",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
    ],
  },
  {
    id: "id8",
    nameCompetion: "Bundesliga",
    image: germanyIcon,
    matchs: [
      {
        idMatch: "id_match1",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match2",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match3",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match4",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match5",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match6",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
      {
        idMatch: "id_match7",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            nameOdd: "Cerezo Osaka",
            odds: 2.78,
          },
          {
            idMatchType: "id_matchType12",
            nameOdd: "Draw",
            odds: 3.1,
          },
          {
            idMatchType: "id_matchType13",
            nameOdd: "Urawa",
            odds: 2.44,
          },
        ],
      },
      {
        idMatch: "id_match8",
        firstCountry: "Seattle Sounders",
        secondCountry: "FC Cincinnati",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType21",
            nameOdd: "Seattle Sounders",
            odds: 3.05,
          },
          {
            idMatchType: "id_matchType22",
            nameOdd: "Draw",
            odds: 1.99,
          },
          {
            idMatchType: "id_matchType23",
            nameOdd: "FC Cincinnati",
            odds: 4.1,
          },
        ],
      },
    ],
  },
];
