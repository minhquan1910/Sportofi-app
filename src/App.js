import React, { useEffect } from "react";
import Footer from "./components/Layout/Footer";
import Header from "./components/Layout/Header";
import Sports from "./components/Sports/Sports";
import WalletModal from "./components/Wallet/Modal";
import Tournament from "./components/Tournament/Tournament";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./index.css";

import { Route, Routes, Navigate } from "react-router-dom";
// import InPlay from "./components/Layout/InPlay/InPlay";
import Match from "./components/Match/Match";
import SlideMobile from "./components/SlideMobile/SlideMobile";
import { useSelector } from "react-redux";
import BetHistory from "./components/BetHistory/BetHistory";
import History from "./components/History/History";
import HistoryMain from "./components/AllHistory/HistoryMain";
import FaucetMain from "./components/Faucet/FaucetMain";

function App() {
  const googleTranslateElementInit = () => {
    new window.google.translate.TranslateElement(
      {
        pageLanguage: "en",
        autoDisplay: false,
        layout: window.google.translate.TranslateElement.InlineLayout.SIMPLE,
      },
      "google_translate_element"
    );
  };
  useEffect(() => {
    var addScript = document.createElement("script");
    addScript.setAttribute(
      "src",
      "//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"
    );
    document.body.appendChild(addScript);
    window.googleTranslateElementInit = googleTranslateElementInit;
  }, []);

  const showConnectWallet = useSelector(
    (state) => state.ui.connectWalletIsVisible
  );

  return (
    <React.Fragment>
      {showConnectWallet && <WalletModal />}

      <div className="heading">
        <Header />
      </div>

      <Routes>
        <Route path="/invite/:referralCode" element={<Sports />}></Route>

        <Route
          path="/tournament/:tournamentId"
          element={<Tournament />}
        ></Route>
        <Route
          path="/tournament/:tournamentId/match/:matchId"
          element={<Match />}
        />
        <Route path="/sport" element={<Sports />} />
        <Route path="/profile/bet-history" element={<BetHistory />} />

        <Route path="/" element={<Sports />} />
        <Route path="/:code" element={<Sports />} />
        <Route path="*" element={<Navigate replace to="/" />} />
        <Route path="/history" element={<HistoryMain />} />
        <Route path="/faucet" element={<FaucetMain/>}/>
      </Routes>

      <SlideMobile />
      <Footer />
      {/* <SubMenu /> */}
    </React.Fragment>
  );
}

export default App;
