import rpc from "./rpc";
import { Bet2WinABI, AuthorityABI, GTokenABI, TreasuryABI, FaucetABI, PMtokenABI } from "./abi";
import {
    Bet2WinAddress,
    AuthorityAddress,
    GTokenAddress,
    TreasuryAddress,
    FaucetAddress,
    PMtokenAddress
} from "./address";
import match_status from "./match_statuses";
import games from "./games";
import targetNetwork from "./networks";
import chain from "./chain";
import provider from "./provider";
const CURRENT_NETWORK = "testnet";
const constants = {
    TARGET_NETWORK: targetNetwork[CURRENT_NETWORK], // "mainnet | testnet"
    BET_TO_WIN_ABI: JSON.parse(Bet2WinABI),
    BET_TO_WIN_ADDRESS: Bet2WinAddress[CURRENT_NETWORK],
    AUTHORITY_ABI: JSON.parse(AuthorityABI),
    AUTHORITY_ADDRESS: AuthorityAddress[CURRENT_NETWORK],
    GTOKEN_ABI: JSON.parse(GTokenABI),
    GTOKEN_ADDRESS: GTokenAddress[CURRENT_NETWORK],
    TREASURY_ABI: JSON.parse(TreasuryABI),
    TREASURY_ADDRESS: TreasuryAddress[CURRENT_NETWORK],
    FAUCET_ABI: JSON.parse(FaucetABI),
    FAUCET_ADDRESS: FaucetAddress[CURRENT_NETWORK],
    PMTOKEN_ABI: JSON.parse(PMtokenABI),
    PMTOKEN_ADDRESS: PMtokenAddress[CURRENT_NETWORK],
    RPC_URL: rpc[CURRENT_NETWORK],
    CHAIN: chain[CURRENT_NETWORK],
    PROVIDER: provider,
    settleStatus: {
        FIRST_HALF: 1,
        SECOND_HALF: 2,
        FULL_MATCH: 3,
    },
    apiConfigs: {
        DOMAIN_NAME: "https://api-football-v1.p.rapidapi.com/v3",
        endpoints: {
            FIXTURES: "/fixtures",
        },
        headers: {
            X_RAPID_API_HOST: "X-RapidAPI-Host",
            X_RAPID_API_KEY: "X-RapidAPI-Key"
        },
        method: {
            GET: "get",
            POST: "post",
        },
        API_HOST: "api-football-v1.p.rapidapi.com",
    },
    sides: {
        HOME: {
            name: "Home",
            code: 1
        },
        DRAW: {
            name: "Draw",
            code: 2
        },
        AWAY: {
            name: "Away",
            code: 3
        },
    },
    matchStatuses: match_status,
    TIME_END: 100,
    game: games,
    storage: {
        periodic: {
            EVERY_5_MINUTES: "EVERY_5_MINUTES",
            DAILY: "DAILY",
        },
        key: {
            MATCH_IDS: "MATCH_ID",
        }
    },
    PERCENTAGE_FRACTION: 10000,
    ///@dev value is equal to keccak256("Permit(address user,uint256 betId,uint256 amount,address paymentToken,uint256 deadline,uint256 nonce)")
    TYPE_HASH_PERMIT: "0xe5e51ef2e78cfc0f352bab62ce7f0638fbc905821e660a187064efd4453c4e22",
    NODE_REAL_IO: {
        HTTPS: "https://bsc-testnet.nodereal.io/v1/",
        WSS: "wss://bsc-testnet.nodereal.io/ws/v1/"
    },

    apiCloudConfigs: {
        DOMAIN: "https://alpha.sportofi.com/",
        SUB_DOMAIN: ".netlify/functions/",
        endpoints: {
            GET_RANKS: "getRanks",
            SAVE_HISTORY: "saveHistory-background",
            GET_HISTORY: "getHistory",
            UPDATE_HISTORY: "updateHistory-background",
            GET_ACCOUNT: "getAccount",
            SAVE_ACCOUNT: "saveAccount-background",
        }
    },
    DEFAULTS: {
        BOOKMARKER: {
            code: 1,
            name: "10Bet",
        },
        BET_TYPE: {
            code: 1,
            name: "Match Winner",
        }
    }
}
export default constants;