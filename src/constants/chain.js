const chain = {
    testnet: {
    bscChain : {
        id: 97,
        name: 'BSC',
        network: 'BSC Testnet',
        nativeCurrency: {
          decimals: 18,
          name: 'BSC',
          symbol: 'BSC',
        },
        rpcUrls: {
          default: 'https://data-seed-prebsc-1-s1.binance.org:8545/',
        },
        blockExplorers: {
          default: { name: 'Bscscan', url: 'https://testnet.bscscan.com/' },
        },
        testnet: true,
      }
    }
}

export default chain