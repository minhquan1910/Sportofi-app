export const league = {
  world: {
    worldCup: "World Cup",
    championsLeague: "UEFA Champions League",
    europaLeague: "UEFA Europa League",
    friendliesCup: "Friendlies Club",
  },
  england: {
    premierLeague: "Premier League",
    championship: "Championship",
    leagueTwo: "League Two",
    fACup: "FA Cup",
    leagueOne: "League One",
    eFLTrophy: "EFL Trophy",
  },
  spain: {
    laligaLeague: "La Liga",
  },
  italy: {
    serieALeague: "Serie A",
    serieBLeague: "Serie B",
  },
  germany: {
    bundesligaLeague: "Bundesliga",
  },
  france: {
    ligue1League: "Ligue 1",
    ligue2League: "Ligue 2",
  },
};
