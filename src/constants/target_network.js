const targetNetwork = {
    testnet: {
        chainName: "Binance Smart Chain Testnet",
        chainId: "0x61",
        nativeCurrency: {
            name: "BNB",
            symbol: "tBNB",
            decimals: 18
        },
        rpcUrls: ["https://data-seed-prebsc-1-s1.binance.org:8545/"],
        blockExplorerUrls: ["https://testnet.bscscan.com/"]    
    },
    mainnet: {
        chainName: "Binance Smart Chain",
        chainId: "0x38",
        nativeCurrency: {
            name: "BNB",
            symbol: "BNB",
            decimals: 18
        },
        rpcUrls: ["https://bsc-dataseed.binance.org/"],
        blockExplorerUrls: ["https://bscscan.com/"]
    }
}
export default targetNetwork;