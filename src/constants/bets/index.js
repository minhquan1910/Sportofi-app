import market from "../market";
import BET_TYPE from "../betTypes";
const betList = {
    WINNER: [
        {
            id: 1,
            name: "Match Winner",
            market: market.FULL_MATCH,
            types: [BET_TYPE.WIN, BET_TYPE.DRAW],
        },
        {
            id: 3,
            name: "Second Half Winner",
            market: market.SECOND_HALF,
            types: [BET_TYPE.WIN, BET_TYPE.DRAW],
        },
        {
            id: 13,
            name: "First Half Winner",
            market: market.FIRST_HALF,
            types: [BET_TYPE.WIN, BET_TYPE.DRAW],
        },
    ],
    GOAL_UNDER_OVER: [
        {
            id: 5,
            name: "Goals Over/Under",
            market: market.FULL_MATCH,
            types: [BET_TYPE.OVER, BET_TYPE.UNDER],
        },
        {
            id: 6,
            name: "Goals Over/Under First Half",
            market: market.FIRST_HALF, 
            types: [BET_TYPE.OVER, BET_TYPE.UNDER],
        },
        {
            id: 26,
            name: "Goals Over/Under - Second Half",
            market: market.SECOND_HALF,
            types: [BET_TYPE.OVER, BET_TYPE.UNDER],
        },
    ],
    BOTH_SCORE: [

        {
            id: 8,
            name: "Both Teams Score",
            market: market.FULL_MATCH,
            types: [BET_TYPE.WIN],
        },
        {
            id: 34,
            name: "Both Teams Score - First Half",
            market: market.FIRST_HALF,
            types: [BET_TYPE.WIN],
        },
        {
            id: 35,
            name: "Both Teams To Score - Second Half",
            market: market.SECOND_HALF,
            types: [BET_TYPE.WIN],
        },
        {
            id: 24,
            name: "Results/Both Teams Score",
            market: market.FULL_MATCH,
            types: [BET_TYPE.WIN, BET_TYPE.DRAW],
        },
        {
            id: 52,
            name: "Halftime Result/Both Teams Score",
            market: market.FIRST_HALF,
            types: [BET_TYPE.WIN, BET_TYPE.DRAW],
        },
    ],
    EXACT_SCORE: [

        {
            id: 10,
            name: "Exact Score",
            market: market.FULL_MATCH,
            types: [BET_TYPE.CORRECT_SCORE],
        },
        {
            id: 31,
            name: "Correct Score - First Half",
            market: market.FIRST_HALF,
            types: [BET_TYPE.CORRECT_SCORE],
        },
        {
            id: 62,
            name: "Correct Score - Second Half",
            market: market.SECOND_HALF,
            types: [BET_TYPE.CORRECT_SCORE],
        },
    ],
    EXACT_GOAL: [
        {
            id: 38,
            name: "Exact Goals Number",
            market: market.FULL_MATCH,
            types: [BET_TYPE.TOTAL],
        },
        {
            id: 40,
            name: "Home Team Exact Goals Number",
            market: market.FULL_MATCH,
            types: [BET_TYPE.TOTAL],
        },
        {
            id: 41,
            name: "Away Team Exact Goals Number",
            market: market.FULL_MATCH,
            types: [BET_TYPE.TOTAL],
        },
        {
            id: 42,
            name: "Second Half Exact Goals Number",
            market: market.SECOND_HALF,
            types: [BET_TYPE.TOTAL],
        },
        {
            id: 46,
            name: "Exact Goals Number - First Half",
            market: market.FIRST_HALF,
            types: [BET_TYPE.TOTAL],
        },
    ],
    CORNER: [
        {
            id: 45,
            name: "Corners Over Under",
            market: market.FULL_MATCH,
            types: [BET_TYPE.OVER, BET_TYPE.UNDER],
        },
        {
            id: 57,
            name: "Home Corners Over/Under",
            market: market.FULL_MATCH,
            types: [BET_TYPE.OVER, BET_TYPE.UNDER],
        },
        {
            id: 58,
            name: "Away Corners Over/Under",
            market: market.FULL_MATCH,
            types: [BET_TYPE.OVER, BET_TYPE.UNDER],
        },
    ],
    CARD: [
        {
            id: 80,
            name: "Cards Over/Under",
            market: market.FULL_MATCH,
            types: [BET_TYPE.OVER, BET_TYPE.UNDER],
        },
        {
            id: 82,
            name: "Home Team Total Cards",
            market: market.FULL_MATCH,
            types: [BET_TYPE.TOTAL],
        },
        {
            id: 83,
            name: "Away Team Total Cards",
            market: market.FULL_MATCH,
            types: [BET_TYPE.TOTAL],
        },

    ],
    ASIAN_HANDICAP: [
        {
            id: 4,
            name: "Asian Handicap",
            market: market.FULL_MATCH,
            types: [BET_TYPE.HANDICAP],
        },
        {
            id: 9,
            name: "Handicap Result",
            market: market.FULL_MATCH,
            types: [BET_TYPE.HANDICAP],
        },
        {
            id: 18,
            name: "Handicap Result - First Half",
            market: market.FIRST_HALF,
            types: [BET_TYPE.HANDICAP],
        },
        {
            id: 19,
            name: "Asian Handicap First Half",
            market: market.FIRST_HALF,
            types: [BET_TYPE.HANDICAP],
        },
        {
            id: 56,
            name: "Corners Asian Handicap",
            market: market.FULL_MATCH,
            types: [BET_TYPE.HANDICAP],
        },
        {
            id: 79,
            name: "Cards European Handicap",
            market: market.FULL_MATCH,
            types: [BET_TYPE.HANDICAP],
        },
        {
            id: 81,
            name: "Cards Asian Handicap",
            market: market.FULL_MATCH,
            types: [BET_TYPE.HANDICAP],
        },

    ]
}

export default betList;