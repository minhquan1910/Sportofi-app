import moment from "moment";
import React, { useState, useEffect } from "react";
import constants from "../../constants";
import classes from "./HistoryItem.module.css";
import { useAccount, useSigner, useBalance } from "wagmi";
import { ethers } from "ethers";
import { Button, Tag } from "antd";
import { successModal, failureModal } from "../../helpers/modals";
import { useTranslation } from "react-i18next";
const HistoryItem = ({ betHistory, isRecentTrade }) => {
  // console.log("bethistory", betHistory);
  const { t } = useTranslation();
  const { address, isConnected } = useAccount();
  const { data } = useBalance({
    addressOrName: address,
    chainId: constants.CHAIN.bscChain.id,
    token: constants.GTOKEN_ADDRESS,
    watch: true,
  });
  const { data: wagmiSigner } = useSigner({
    chainId: constants.CHAIN.bscChain.id,
  });
  const withdraw = async (gameId, matchId, status) => {
    const signer = await wagmiSigner;
    const houseContract = new ethers.Contract(
      constants.BET_TO_WIN_ADDRESS,
      constants.BET_TO_WIN_ABI,
      signer
    );
    try {
      let settle = await houseContract.settleBet(gameId, matchId, status);
      let result = await settle.wait();
      console.log("Result ", result.events[0]);
      successModal("Claimed", " Successfully");
    } catch (err) {
      const error = { err };
      let reason = error.err.reason.substr(28);
      let reasonString = `${reason}`;
      if (reasonString.localeCompare("UNSETTLED_BET")) {
        failureModal("Error ", "Wrong bet !!!");
      } else if (reasonString.localeCompare("ALREADY_PAID")) {
        failureModal("Error ", "Already claimed !!!");
      }
    }
  };
  const formatTime = (unix) => {
    const date = moment(unix * 1000);
    const dateStr = `${date.format("YYYY-MM-DD").toString()}`;
    const hourStr = `${date.format("HH:mm")}`;
    return {
      dateStr,
      hourStr,
    };
  };
  const [claimTitle, setClaimTitle] = useState("Claim");
  const [claimDisable, setClaimDisable] = useState(false);
  const [isYourAddress, setIsYourAddress] = useState(false);
  const [sideName, setSideName] = useState("");
  useEffect(() => {
    if (isConnected && address === betHistory.address) {
      setIsYourAddress(true);
    } else {
      setIsYourAddress(false);
    }
    if (betHistory) {
      const side = betHistory?.side;
      const settleStatus = betHistory?.settleStatus;
      if (side) {
        if (settleStatus?.code === constants.DEFAULTS.BET_TYPE.code) {
          let sideKey = "draw";
          Object.keys(constants.sides).forEach((key) => {
            if (constants.sides[key].code === side.code) {
              sideKey = key.toLowerCase();
            }
          });
          setSideName(
            betHistory?.matchDetails?.teams?.[sideKey]?.name ?? "DRAW"
          );
        } else {
          setSideName(side.name);
        }
      }
      if (betHistory?.isPaid) {
        setClaimTitle("Already Claimed");
        setClaimDisable(true);
      } else {
        if (betHistory?.isSolved === false) {
          setClaimTitle("Pending");
          setClaimDisable(true);
        } else {
          setClaimTitle("Claim");
          setClaimDisable(false);
        }
      }
    }
  }, [betHistory, isConnected, address]);
  // console.log("bethistory", betHistory);
  const shortAddress = (address) => {
    return `${address.slice(0, 6)}...${address.slice(-4)}`;
  };
  const shortBetId = (betId) => {
    if (betId) {
      const betIdStr = betId.toString();
      if (betIdStr.length >= 5) {
        return `${betIdStr.slice(0, 4)}...${betIdStr.slice(-4)}`;
      } else {
        return betId.toString();
      }
    }
  };
  const shortName = (name) => {
    if (name) {
      if (name.length > 10) {
        return `${name.slice(0, 10)}...`;
      } else {
        return name;
      }
    }
  };
  return (
    <div className={`${classes.slider} ${classes.sliderHistory}`}>
      <div className={classes.floatLeft}>
        {isRecentTrade && (
          <p className={classes.floatRight}>
            {shortAddress(betHistory.address)}
          </p>
        )}
        <p className={`${classes.betid}`}>
          {t("description.betid")}: {shortBetId(betHistory?.betId) ?? "unknown"}
        </p>
        {/* <p className={classes.matchValue}>
          <b className={classes.numberValue}>
            {shortBetId(betHistory?.betId) ?? "unknown"}
          </b>
        </p> */}
      </div>
      <div className={classes.tradeMatch}>
        <div className={`${classes.floatLeft} ${classes.textLeft}`}>
          {/* <p className={classes.matchHeading}>Match</p> */}
          <div className={classes.matchValue}>
            <div className={`${classes.matchTitle} ${classes.mainTitle0}`}>
              <img
                className={classes.matchTeam}
                src={betHistory?.matchDetails?.teams?.home?.logo}
                alt="home-team"
              />
              <p className={classes.match1}>
                {shortName(betHistory?.matchDetails?.teams?.home?.name) ??
                  "unknown"}
              </p>
            </div>
            <div>
              <p className={`${classes.px2}`}>Vs</p>
            </div>
            <div className={`${classes.matchTitle} ${classes.mainTitle1} ml-2`}>
              <img
                className={classes.matchTeam}
                src={betHistory?.matchDetails?.teams?.away?.logo}
                alt="away-team"
              />
              <p className={`${classes.match2} `}>
                {shortName(betHistory?.matchDetails?.teams?.away?.name) ??
                  "unknown"}
              </p>
            </div>
          </div>
        </div>
        {/* <div className={`${classes.textRight}`}>
          <p className={`${classes.matchHeading} px-1`}>Betted Time</p>

          <span className={classes.historyTime}>
            {formatTime(betHistory?.placedBetTime).dateStr ?? "unknown"}
          </span>
          <br />
          <span className={classes.historyDate}>
            {formatTime(betHistory?.placedBetTime).hourStr ?? "unknown"}
          </span>
        </div> */}
      </div>
      {/* <hr className="mt-0" /> */}
      <div className={classes.tradeTeam}>
        <div className={classes.tradeTeamName}>
          <p className={`${classes.singleTeamName} ${classes.singleTeamName1}`}>
            {sideName ?? "unknown"}
          </p>
        </div>
        <div className={`${classes.mainTitle}`}>
          <span className={classes.titleRight}>
            <img
              src="https://assets.betswap.gg/icons/sports/6046-b.svg"
              alt=""
            />
          </span>
          <b>{t("description.football")}</b>
        </div>
      </div>
      {/*  */}
      <div className={classes.tradeStake}>
        <ul>
          <li>
            <p className={classes.oddText}>{t("description.stake")}</p>
            <p className={classes.oddValue}>
              {(betHistory?.amount?.toString() ?? "unknown") +
                " " +
                // (data?.symbol?.toString() ?? "SPORT")}
                "SPORTO"}
            </p>
          </li>
          <li className={classes.textCenter}>
            <p className={classes.oddText}>{t("description.odds")}</p>
            <p className={classes.oddValue}>
              {betHistory?.odd ?? "unknown"}
            </p>{" "}
          </li>
          <li className={classes.textRight}>
            <p className={classes.oddText}>{t("description.bettype")}</p>
            <Tag className={classes["oddValue-lay"]}>
              {betHistory?.settleStatus?.name ?? "unknown"}
            </Tag>
          </li>
        </ul>
      </div>

      <div className={classes.startTime}>
        <div className={classes.mainHeading}>
          <p className={`${classes.matchHeading} px-1`}>
            {t("description.bettime")}
          </p>

          <span className={classes.historyTime}>
            {formatTime(betHistory?.placedBetTime).dateStr ?? "unknown"}
          </span>
          <br />
          <span className={classes.historyDate}>
            {formatTime(betHistory?.placedBetTime).hourStr ?? "unknown"}
          </span>
        </div>
        <div>
          <p className={`${classes.matchHeading} px-1`}>
            {t("description.startsin")}
          </p>
          <span className={classes.historyTime}>
            {formatTime(betHistory?.matchDetails?.fixture?.timestamp).dateStr ??
              "unknown"}
          </span>
          <br />
          <span className={classes.historyDate}>
            {formatTime(betHistory?.matchDetails?.fixture?.timestamp).hourStr ??
              "unknown"}
          </span>
        </div>
      </div>

      {isYourAddress && !isRecentTrade && (
        <Button
          className={classes.buttonClaim}
          type="primary"
          style={{
            borderRadius: "10px",
            width: "100%",
            marginTop: "10px",
          }}
          disabled={claimDisable}
          onClick={async () =>
            await withdraw(
              1,
              betHistory?.matchId,
              betHistory?.settleStatus?.code
            )
          }
        >
          {claimTitle}
        </Button>
      )}
    </div>
  );
};

export default HistoryItem;
