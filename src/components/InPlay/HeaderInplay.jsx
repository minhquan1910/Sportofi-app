import React from "react";

import sportofiLogoOriginal from "../../../assets/images/sportofi_logo_original.png";
import NavLink from "../Header/NavLink";

import classes from "../Header.module.css";
import SubHeaderComponentMain from "../Header/SubHeaderComponentMain";
import MenuItem from "../Header/MenuItem";

const HeaderInplay = () => {
  return (
    <React.Fragment>
      <header className={`${classes.header}`}>
        <MenuItem />
        <img src={sportofiLogoOriginal} alt="Sportofi Logo" />
        <NavLink />
      </header>
      <SubHeaderComponentMain />
    </React.Fragment>
  );
};

export default HeaderInplay;
