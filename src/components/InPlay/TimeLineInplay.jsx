import React from "react";
import classes from "./TimeLineInplay.module.css";
import RightBlock from "../../UI/RightBlock";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import deleteBin from "../../../assets/Flags/delete.svg";
import shapeThree from "../../../assets/images/shape-3.png";
import { useSelector } from "react-redux";

function SampleNextArrow(props) {
  const { className, style } = props;
  return <div className={className} style={{ ...style, display: "none" }} />;
}

function SamplePrevArrow(props) {
  const { className, style } = props;
  return <div className={className} style={{ ...style, display: "none" }} />;
}
const TimeLineInplay = (props) => {
  const dataOdd = useSelector((state) => state.cart.cartItem);
  const totalOdd = useSelector((state) => state.cart.totalOdd);
  const liability = useSelector((state) => state.cart.liability);
  const profit = useSelector((state) => state.cart.profit);

  var settings = {
    dots: true,
    dotWidth: "30px",
    infinite: true,
    speed: 500,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    dotsClass: `slick-dots ${classes.dots}`,

    appendDots: (dots) => (
      <div
        style={{
          borderRadius: "10px",
          padding: "10px",
        }}
      >
        <ul style={{ margin: "0px" }}>{dots} </ul>
      </div>
    ),

    // dotsClass: `slick-dots ${classes.dots}`,
  };

  return (
    <RightBlock>
      <section className={classes.right}>
        <div className={classes.navigation}>
          <div className={classes.timeLine}>
            <nav className={classes.nav}>
              <li>
                <button className={classes.active}>Betslip</button>
              </li>
              <li>
                <button>Open Bets</button>
              </li>
              <li>
                <button>Bet History</button>
              </li>
            </nav>
            <div className={classes["tab-content"]}>
              <div className={classes["heading-tab"]}>
                <div>
                  <p>Selection</p>
                  <span>
                    <img src={deleteBin} alt="Delete Bin" />
                  </span>
                </div>
              </div>
              <div className={classes["body-tab"]}>
                <div className={classes["back-div"]}>
                  <ul>
                    <li>
                      <p>Back (Bet For)</p>
                    </li>
                    <li>
                      <p>Odds</p>
                      <p>Stake</p>
                    </li>
                  </ul>
                </div>
                <div className={classes["back-data"]}>
                  <ul>
                    <li>
                      <div>
                        <p style={{ fontWeight: "bold", fontSize: "13px" }}>
                          {dataOdd[0]?.name}
                        </p>
                        <p>Market 1x2</p>
                      </div>
                    </li>
                    <li>
                      <p>
                        <input
                          type="number"
                          maxLength={10}
                          data-index={0}
                          step={0.01}
                          defaultValue={dataOdd[0]?.odd}
                        />
                      </p>
                      <p>
                        <input
                          type="number"
                          maxLength={10}
                          data-index={0}
                          step={1}
                          defaultValue={dataOdd[0]?.odd ? 1 : null}
                        />
                      </p>
                    </li>
                  </ul>
                </div>
                <div className={classes["payout-amt"]}>
                  <ul>
                    <li></li>
                    <li>
                      <p>Payout</p>
                      <p>${totalOdd}</p>
                    </li>
                  </ul>
                </div>
                <div className={classes["profit"]}>
                  <p>
                    <span>Liability:</span>
                    <span>${liability}</span>
                  </p>
                  <p>
                    <span>Profit :</span>
                    <span>${profit}</span>
                  </p>
                </div>
                <div className={classes["shape-img"]}>
                  <img src={shapeThree} alt="Shape 3" />
                </div>
                <div className={classes["place-betsBtn"]}>
                  <button>Place Bet</button>
                </div>
              </div>
            </div>
          </div>

          {/* <div className={classes.exchangeBet}>
            <select className={classes.selection} name="" id="">
              <option className={classes.option} value="#">
                Exchange Bets
              </option>
            </select>
          </div>
          <div className={classes.match}>
            <button className={`${classes.btnMatch}, ${classes.active}`}>
              Matched
            </button>
            <button className={classes.btnMatch}>Unmatched</button>
          </div>
          <div className={classes.alert}>
            <span className={classes.desc}>
              Connect Wallet to view your Open Bets
            </span>
          </div> */}
        </div>
        <div className={classes.headerTraded}>
          <span className={classes.recentlyTraded}>
            Recently Traded History
          </span>
          <span className={classes.tradeIcon} />
        </div>
        {/*  */}

        <div className={`${classes.tradeHistory} ${classes.tradeGap}`}>
          <div className={classes.collapseOne}>
            <div class={classes.slick_slider}>
              <Slider className={classes.sliderMain} {...settings}>
                <div className={classes.slider}>
                  <p className={classes.floatLeft}>
                    <span className={classes.historyTime}>2022/09/22</span>
                    <br />
                    <span className={classes.historyDate}>12:49</span>
                  </p>
                  <p className={classes.floatRight}>0xc60...f95a</p>
                  <div className={classes.tradeMatch}>
                    <div className={`${classes.floatLeft} ${classes.textLeft}`}>
                      <p className={classes.matchHeading}>Match</p>
                      <p className={classes.matchValue}>
                        <div className={classes.matchTitle}>
                          <p className={classes.match1}>England W</p>
                          <p className={classes.px2}>Vs</p>
                          <p className={classes.match2}>India W</p>
                        </div>
                      </p>
                    </div>
                    <div className={`${classes.textRight}`}>
                      <p className={`${classes.betid}`}>Bet ID</p>
                      <p className={classes.matchValue}>
                        <b className={classes.numberValue}>4024</b>
                      </p>
                    </div>
                  </div>
                  {/* <hr className="mt-0" /> */}
                  <div className={classes.tradeTeam}>
                    <div className={classes.tradeTeamName}>
                      <p className={classes.singleTeamName}>shakur stevenson</p>
                    </div>
                    <div className={`${classes.mainTitle}`}>
                      <span className={classes.titleRight}>
                        <img
                          src="https://assets.betswap.gg/icons/sports/154919-b.svg"
                          alt=""
                        />
                      </span>
                      <b>Boxing</b>
                    </div>
                  </div>
                  {/*  */}
                  <div className={classes.tradeStake}>
                    <ul>
                      <li>
                        <p className={classes.oddText}>Stake</p>
                        <p className={classes.oddValue}>$1.02</p>
                      </li>
                      <li className={classes.textCenter}>
                        <p className={classes.oddText}>Odds</p>
                        <p className={classes.oddValue}>$1.02</p>{" "}
                      </li>
                      <li className={classes.textRight}>
                        <p className={classes.oddText}>Bet Type</p>
                        {/* <p className={classes.oddValue}>$1.02</p> */}
                        <p className={classes["oddValue-lay"]}>Back</p>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className={classes.slider}>
                  <p className={classes.floatLeft}>
                    <span className={classes.historyTime}>2022/09/22</span>
                    <br />
                    <span className={classes.historyDate}>12:49</span>
                  </p>
                  <p className={classes.floatRight}>0xc60...f95a</p>
                  <div className={classes.tradeMatch}>
                    <div className={`${classes.floatLeft} ${classes.textLeft}`}>
                      <p className={classes.matchHeading}>Match</p>
                      <p className={classes.matchValue}>
                        <div className={classes.matchTitle}>
                          <p className={classes.match1}>England W</p>
                          <p className={classes.px2}>Vs</p>
                          <p className={classes.match2}>India W</p>
                        </div>
                      </p>
                    </div>

                    <div className={`${classes.textRight}`}>
                      <p className={`${classes.betid} ${classes.matchHeading}`}>
                        Bet ID
                      </p>
                      <p className={classes.matchValue}>
                        <b className={classes.numberValue}>4024</b>
                      </p>
                    </div>
                  </div>

                  <div className={classes.tradeTeam}>
                    <div className={classes.tradeTeamName}>
                      <p className={classes.singleTeamName}>shakur stevenson</p>
                    </div>
                    <div className={`${classes.mainTitle}`}>
                      <span className={classes.titleRight}>
                        <img
                          src="https://assets.betswap.gg/icons/sports/154919-b.svg"
                          alt=""
                        />
                      </span>
                      <b>Boxing</b>
                    </div>
                  </div>

                  <div className={classes.tradeStake}>
                    <ul>
                      <li>
                        <p className={classes.oddText}>Stake</p>
                        <p className={classes.oddValue}>$1.02</p>
                      </li>
                      <li className={classes.textCenter}>
                        <p className={classes.oddText}>Odds</p>
                        <p className={classes.oddValue}>$1.02</p>{" "}
                      </li>
                      <li className={classes.textRight}>
                        <p className={classes.oddText}>Bet Type</p>
                        <p className={classes.oddValue}>$1.02</p>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className={classes.slider}>
                  <p className={classes.floatLeft}>
                    <span className={classes.historyTime}>2022/09/22</span>
                    <br />
                    <span className={classes.historyDate}>12:49</span>
                  </p>
                  <p className={classes.floatRight}>0xc60...f95a</p>
                  <div className={classes.tradeMatch}>
                    <div className={`${classes.floatLeft} ${classes.textLeft}`}>
                      <p className={classes.matchHeading}>Match</p>
                      <p className={classes.matchValue}>
                        <div className={classes.matchTitle}>
                          <p className={classes.match1}>England W</p>
                          <p className={classes.px2}>Vs</p>
                          <p className={classes.match2}>India W</p>
                        </div>
                      </p>
                    </div>
                    <div className={`${classes.textRight}`}>
                      <p className={`${classes.betid} ${classes.matchHeading}`}>
                        Bet ID
                      </p>
                      <p className={classes.matchValue}>
                        <b className={classes.numberValue}>4024</b>
                      </p>
                    </div>
                  </div>

                  <div className={classes.tradeTeam}>
                    <div className={classes.tradeTeamName}>
                      <p className={classes.singleTeamName}>shakur stevenson</p>
                    </div>
                    <div className={`${classes.mainTitle}`}>
                      <span className={classes.titleRight}>
                        <img
                          src="https://assets.betswap.gg/icons/sports/154919-b.svg"
                          alt=""
                        />
                      </span>
                      <b>Boxing</b>
                    </div>
                  </div>

                  <div className={classes.tradeStake}>
                    <ul>
                      <li>
                        <p className={classes.oddText}>Stake</p>
                        <p className={classes.oddValue}>$1.02</p>
                      </li>
                      <li className={classes.textCenter}>
                        <p className={classes.oddText}>Stake</p>
                        <p className={classes.oddValue}>$1.02</p>{" "}
                      </li>
                      <li className={classes.textRight}>
                        <p className={classes.oddText}>Stake</p>
                        <p className={classes.oddValue}>$1.02</p>
                      </li>
                    </ul>
                  </div>
                </div>
              </Slider>
            </div>
          </div>
        </div>
      </section>
    </RightBlock>
  );
};

export default TimeLineInplay;
