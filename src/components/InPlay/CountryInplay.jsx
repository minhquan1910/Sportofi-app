import React, { useState } from "react";
import internationalIcon from "../../../assets/Flags/international-icon.svg";
import peruIcon from "../../../assets/Flags/peru.png";
import england from "../../../assets/Flags/england.png";
import spain from "../../../assets/Flags/spain.png";
import germany from "../../../assets/Flags/germany.png";
import france from "../../../assets/Flags/france.png";
import italy from "../../../assets/Flags/italy.png";
import unitedStateIcon from "../../../assets/Flags/united-states.png";
import austria from "../../../assets/Flags/austria.png";
import norway from "../../../assets/Flags/norway.png";
import mexicoIcon from "../../../assets/Flags/mexico.png";
import greece from "../../../assets/Flags/greece.png";
import brazil from "../../../assets/Flags/brazil.png";
import sweden from "../../../assets/Flags/sweden.png";
import chile from "../../../assets/Flags/chile.png";
import uruguay from "../../../assets/Flags/uruguay.png";
import australia from "../../../assets/Flags/australia.png";
import portugal from "../../../assets/Flags/portugal.png";
import japan from "../../../assets/Flags/japan.png";
import ireland from "../../../assets/Flags/ireland.png";
import colombia from "../../../assets/Flags/colombia.png";
import belgium from "../../../assets/Flags/belgium.png";
import iceland from "../../../assets/Flags/iceland.png";
import costaRica from "../../../assets/Flags/costa-rica.png";
import scotland from "../../../assets/Flags/scotland.png";
import china from "../../../assets/Flags/china.png";
import finland from "../../../assets/Flags/finland.png";
import denmark from "../../../assets/Flags/denmark.png";
import switzerland from "../../../assets/Flags/switzerland.png";
import argentina from "../../../assets/Flags/argentina.png";
import turkey from "../../../assets/Flags/turkey.png";
import netherlands from "../../../assets/Flags/netherlands.png";
import classes from "./ContryInPlay.module.css";
import upArrowIcon from "../../../assets/Flags/up-arrow.svg";

// const DUMMY_COUNTRY = [
//   {
//     id: "id1",
//     name: "International",
//     image: internationalIcon,
//     competitionNumber: 9,
//     isShow: false,
//     competition: [
//       {
//         competitionID: "comInId1",
//         competitionName: "UEFA Nations League",
//       },
//       {
//         competitionID: "comInId2",
//         competitionName: "FIFA World Cup 2022",
//       },
//       {
//         competitionID: "comInId3",
//         competitionName: "Friendlies National Teams",
//       },
//       {
//         competitionID: "comInId4",
//         competitionName: "CONMEBOL Copa Libertadores",
//       },
//     ],
//   },
//   {
//     id: "id2",
//     name: "Peru",
//     image: peruIcon,
//     competitionNumber: 1,
//     isShow: false,
//     competition: [
//       {
//         competitionID: "comPeId1",
//         competitionName: "Primera División",
//       },
//     ],
//   },
//   {
//     id: "id3",
//     name: "United States",
//     image: unitedStateIcon,
//     competitionNumber: 1,
//     isShow: false,
//     competition: [
//       {
//         competitionID: "comUniId1",
//         competitionName: "Major League Soccer",
//       },
//     ],
//   },
//   {
//     id: "id4",
//     name: "Mexico",
//     image: mexicoIcon,
//     competitionNumber: 1,
//     isShow: false,
//     competition: [
//       {
//         competitionID: "comMexId1",
//         competitionName: "Liga MX",
//       },
//     ],
//   },
// ];

const CountryInplay = () => {
  const [showCompetition, setShowCompetition] = useState(false);

  const showCompetitionHandler = () => {
    // setShowCompetition((prevShowCompetition) => [...prevShowCompetition, ]);
    setShowCompetition((prevShowCompetition) => !prevShowCompetition);
  };

  const internationalChamipons = (
    <ul>
      <li>
        <a href="/">
          <span>FIFA World Cup 2022</span>
          <svg
            className="svg-inline--fa fa-chevron-right fa-w-10"
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="chevron-right"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 320 512"
            data-fa-i2svg=""
          >
            <path
              fill="currentColor"
              d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"
            ></path>
          </svg>
        </a>
      </li>
      <li>
        <a href="/">
          <span>UEFA Nations League</span>
          <svg
            className="svg-inline--fa fa-chevron-right fa-w-10"
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="chevron-right"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 320 512"
            data-fa-i2svg=""
          >
            <path
              fill="currentColor"
              d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"
            ></path>
          </svg>
        </a>
      </li>
      <li>
        <a href="/">
          <span>UEFA Europa League</span>
          <svg
            className="svg-inline--fa fa-chevron-right fa-w-10"
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="chevron-right"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 320 512"
            data-fa-i2svg=""
          >
            <path
              fill="currentColor"
              d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"
            ></path>
          </svg>
        </a>
      </li>
      <li>
        <a href="/">
          <span>UEFA Champions League</span>
          <svg
            className="svg-inline--fa fa-chevron-right fa-w-10"
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="chevron-right"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 320 512"
            data-fa-i2svg=""
          >
            <path
              fill="currentColor"
              d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"
            ></path>
          </svg>
        </a>
      </li>
    </ul>
  );

  const upArrow = (
    <img className={classes["up-arrow"]} src={upArrowIcon} alt="up row" />
  );

  const downArrow = (
    <svg
      className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
      aria-hidden="true"
      focusable="false"
      data-prefix="fa"
      data-icon="chevron-down"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 448 512"
      data-fa-i2svg=""
    >
      <path
        fill="currentColor"
        d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
      ></path>
    </svg>
  );

  return (
    <div className={classes.country}>
      <h1>Choose by Country or Organisation</h1>
      <div className={classes.accordion}>
        <div className={classes["accordian-heading"]}>
          {/* {DUMMY_COUNTRY.map((country) => (
            <button
              key={country.id}
              onClick={showCompetitionHandler(country.id)}
            >
              <div className={classes["internation-info"]}>
                <img src={country.image} alt="International Flag" />
                <p className={showCompetition ? classes.active : null}>
                  {country.name}
                </p>
              </div>
              <div className={classes["dropdown-number"]}>
                <span className={showCompetition ? classes.active : null}>
                  {country.competitionNumber}
                </span>
                {showCompetition ? upArrow : downArrow}
              </div>
            </button>
          ))} */}
          <button onClick={showCompetitionHandler}>
            <div className={classes["internation-info"]}>
              <img src={internationalIcon} alt="International Flag" />
              <p className={showCompetition ? classes.active : null}>
                International
              </p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span className={showCompetition ? classes.active : null}>5</span>
              {showCompetition ? upArrow : downArrow}
            </div>
          </button>
          {showCompetition ? internationalChamipons : null}
          <button>
            <div className={classes["internation-info"]}>
              <img src={england} alt="England Flag" />
              <p>England</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={spain} alt="Spain Icon" />
              <p>Spain</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>2</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={italy} alt="Italy Icon" />
              <p>Italy</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>2</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={germany} alt="Germany Icon" />
              <p>Germany</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>2</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={france} alt="France Icon" />
              <p>France</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={unitedStateIcon} alt="United States Icon" />
              <p>United States</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={austria} alt="Austria Icon" />
              <p>Austria</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={norway} alt="norway Icon" />
              <p>Norway</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={netherlands} alt="netherlands Icon" />
              <p>Netherlands</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={brazil} alt="brazil Icon" />
              <p>Brazil</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={peruIcon} alt="Peru Icon" />
              <p>Peru</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={greece} alt="greece Icon" />
              <p>Greece</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={turkey} alt="Turkey Icon" />
              <p>Turkey</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={belgium} alt="Belgium Icon" />
              <p>Belgium</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={argentina} alt="Argentina Icon" />
              <p>Argentina</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={switzerland} alt="Switzerland Icon" />
              <p>Switzerland</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={sweden} alt="Sweden Icon" />
              <p>Sweden</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={australia} alt="Australia Icon" />
              <p>Australia</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={chile} alt="Chile Icon" />
              <p>Chile</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={uruguay} alt="Uruguay Icon" />
              <p>Uruguay</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={denmark} alt="Denmark Icon" />
              <p>Denmark</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={japan} alt="Japan Icon" />
              <p>Japan</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={costaRica} alt="CostaRica Icon" />
              <p>Costa Rica</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={finland} alt="Finland Icon" />
              <p>Finland</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={scotland} alt="Scotland Icon" />
              <p>Scotland</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={portugal} alt="Portugal Icon" />
              <p>Portugal</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={colombia} alt="Colombia Icon" />
              <p>Colombia</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={china} alt="China Icon" />
              <p>China</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={mexicoIcon} alt="Mexico Icon" />
              <p>Mexico</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button>
            <div className={classes["internation-info"]}>
              <img src={ireland} alt="Ireland Icon" />
              <p>Ireland</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>

          <button>
            <div className={classes["internation-info"]}>
              <img src={iceland} alt="Iceland Icon" />
              <p>Iceland</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
        </div>
      </div>
    </div>
  );
};

export default CountryInplay;
