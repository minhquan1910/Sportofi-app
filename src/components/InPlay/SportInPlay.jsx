import React from "react";
import classes from "./SportInPlay.module.css";
import internationalWhiteIcon from "../../../assets/Flags/international-white.svg";
import { useDispatch, useSelector } from "react-redux";
import { cartActions } from "../../../store/slices/cart-slice";

const DUMMY_ODDS = [
  {
    id: "id1",
    nameCompetion: "J.Leguae Cup",
    image: internationalWhiteIcon,
    matchs: [
      {
        idMatch: "id_match1",
        firstCountry: "Cerezo Osaka",
        secondCountry: "Urawa",
        time: "Tody 17:00",
        matchTypes: [
          {
            idMatchType: "id_matchType11",
            odds: 2.78,
          },
          //   {
          //     idMatchType: "id_matchType12",
          //     odds: 3.1,
          //   },
          {
            idMatchType: "id_matchType13",
            odds: 2.44,
          },
        ],
      },
      // {
      //   idMatch: "id_match2",
      //   firstCountry: "Cerezo Osaka",
      //   secondCountry: "Urawa",
      //   time: "Tody 17:00",

      //   matchTypes: [
      //     {
      //       idMatchType: "id_matchType21",
      //       odds: 2.78,
      //     },
      //     {
      //       idMatchType: "id_matchType22",
      //       odds: 3.1,
      //     },
      //     {
      //       idMatchType: "id_matchType23",
      //       odds: 2.44,
      //     },
      //   ],
      // },
    ],
  },
];

const matchsData = DUMMY_ODDS[0].matchs[0];

const SportInPlay = () => {
  const dispatch = useDispatch();
  const isActiveOdd = useSelector((state) => state.cart.oddsActive);

  const addOddsHander = (event) => {
    dispatch(cartActions.isActiveOdds());

    const idOdd = event.currentTarget.id;
    if (idOdd === "id_matchType11") {
      const name = matchsData.firstCountry;
      const price = matchsData.matchTypes[0].odds;
      dispatch(cartActions.addOddHandler({ idOdd, name, price }));
    } else if (idOdd === "id_matchType12") {
      const name = "Draw";
      const price = matchsData.matchTypes[1].odds;
      dispatch(cartActions.addOddHandler({ idOdd, name, price }));
    } else {
      const name = matchsData.secondCountry;
      const price = matchsData.matchTypes[2].odds;
      dispatch(cartActions.addOddHandler({ idOdd, name, price }));
    }
  };

  const sportItems = DUMMY_ODDS.map((sportItem) => (
    <div className={classes["card-info"]} key={sportItem}>
      <div className={classes["card-header"]}>
        <div className={classes["match-headername"]}>
          <p>
            <img src={sportItem.image} alt="international icon" />
          </p>
          <p>{sportItem.nameCompetion}</p>
        </div>
        <div className={classes["match-type"]}>
          <ul>
            <li>1</li>
            {/* <li>X</li> */}
            <li>2</li>
          </ul>
        </div>
      </div>
      <div className={classes["card-content"]}>
        {sportItem.matchs.map((match) => (
          <div className={classes["data-list"]} key={match.idMatch}>
            <div className={classes["league-matchTeam"]}>
              <div className={classes["league-matchName"]}>
                <p>{match.firstCountry}</p>
                <p>{match.secondCountry}</p>
                <p>{match.time}</p>
              </div>
              <div className={classes["league-matchBlock"]}>
                <ul>
                  {match.matchTypes.map((matchtype) => (
                    <li key={matchtype.idMatchType}>
                      <button
                        onClick={addOddsHander}
                        id={matchtype.idMatchType}
                      >
                        {matchtype.odds}
                      </button>
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  ));

  return (
    <section className={classes.sportInfoMain}>
      {/* <div className={classes["header-info"]}> */}
      {/* <ul>
          <li>
            <a href="/">In-Play</a>
          </li>
          <li>
            <a href="/" className={classes.active}>
              Upcoming
            </a>
          </li>
        </ul> */}
      {/* </div> */}
      <div className={classes["main-info"]}>{sportItems}</div>
    </section>
  );
};

export default SportInPlay;
