import React from "react";
import classes from "./TournamentMatchType.module.css";
import TournamentMatchTypeItem from "../TournamentMatchTypeItem/TournamentMatchTypeItem";

const TournamentMatchType = ({ matchId }) => {
  return (
    <ul className={classes["odds-list"]}>
      <TournamentMatchTypeItem oddMatchId={matchId} />
    </ul>
  );
};

export default TournamentMatchType;
