import React, { useState } from "react";
import classes from "./HistoryContent.module.css";
import { Radio, Tabs } from "antd";
import "react-tabs/style/react-tabs.css";
import { useTranslation } from "react-i18next";

import RecentTrade from "../History/RecentTrade";
import TimeLine from "../BlockRight/TimeLine";
import MyBetsHistory from "./MyBetsHistory";

const HistoryContent = () => {
  const { t } = useTranslation();
  return (
    <div className={classes.historyMain}>
      <div>
        <Tabs centered={true}>
          <Tabs.TabPane tab={t("description.bethistory")} key="item-1">
            <MyBetsHistory />
          </Tabs.TabPane>
          <Tabs.TabPane tab={t("description.recentlyTrade")} key="item-2">
            <RecentTrade />
          </Tabs.TabPane>
        </Tabs>
      </div>
    </div>
  );
};

export default HistoryContent;
