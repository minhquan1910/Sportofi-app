import React from "react";
import "./History.css";

import SportMain from "../SportMain/SportMain";
import TimeLine from "../BlockRight/TimeLine";
import SportPiece from "../Sports/SportPiece";
import Country from "../Country/Country";
import MyBets from "../MyBets/MyBets";
import HistoryContent from "./HistoryContent";

const Sports = () => {
  return (
    <div className="sporting">
      <main className="mainMain">
        <SportPiece />
        <Country />
        <HistoryContent />
        {/* <SportMain /> */}
        {/* <TimeLine /> */}
      </main>
    </div>
  );
};

export default Sports;
