import React from "react";
import "./HistoryMain.css";

import SportPiece from "../Sports/SportPiece";
import Country from "../Country/Country";
import AllHistory from "./AllHistory";

const HistoryMain = () => {
  return (
    <div className="sporting">
      <main className="mainMain">
        <SportPiece />
        {/* <Country /> */}
        <AllHistory />
        {/* <HistoryContent /> */}
        {/* <SportMain /> */}
        {/* <TimeLine /> */}
      </main>
    </div>
  );
};

export default HistoryMain;
