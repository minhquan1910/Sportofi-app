import { Table } from "antd";
import React, { useEffect } from "react";
import { useState } from "react";

import { useDispatch, useSelector } from "react-redux";
import { fetchBetAllHistory } from "../../store/actions/betHistory-action";
import { isEmpty } from "lodash";
import classes from "./AllHistory.module.css";
import moment from "moment";

const AllHistory = () => {
  const formatTime = (unix) => {
    const date = moment(unix * 1000);
    const dateStr = `${date.format("YYYY-MM-DD").toString()}`;
    const hourStr = `${date.format("HH:mm")}`;
    return {
      dateStr,
      hourStr,
    };
  };
  const dispatch = useDispatch();
  const betAllHistory = useSelector(
    (state) => state.betHistory.betAllHistoryData
  );
  console.log(betAllHistory);
  const [historyData, setHistoryData] = useState([]);
  useEffect(() => {
    dispatch(fetchBetAllHistory());
  }, [dispatch]);
  useEffect(() => {
    if (!isEmpty(betAllHistory)) {
      const data = betAllHistory.map((historyItem, index) => {
        return {
          id: betAllHistory.length - index,
          betId: historyItem?.betId,
          betType: historyItem?.settleStatus?.name ?? "",
          odds: historyItem?.odd,
          betTime: formatTime(historyItem?.placedBetTime).dateStr ?? "unknown",
          startsin:
            formatTime(historyItem?.matchDetails?.fixture?.timestamp)
              ?.dateStr ?? "unknown",
          logoTeam: historyItem?.matchDetails?.teams?.home?.logo,
          homeTeam: historyItem?.matchDetails?.teams?.home?.name ?? "",
          awayTeam: historyItem?.matchDetails?.teams?.away?.name ?? "",
          amount: historyItem?.amount ?? 0,
          wallet: historyItem?.address ?? "unknown",
          key: historyItem?._id ?? "",
          description: "helooooooooooo",
        };
      });
      setHistoryData(data);
    }
  }, [betAllHistory]);
  const wrapperHeader = (value) => {
    return <b style={{ color: "#145AAA" }}>{value}</b>;
  };

  const columns = [
    {
      title: wrapperHeader("#"),
      dataIndex: "id",
      align: "center",
      width: 50,
      render: (text) => <b>{text}</b>,
    },
    {
      title: wrapperHeader("Bet Id"),
      dataIndex: "betId",
      align: "center",
    },
    {
      title: wrapperHeader("Bet Type"),
      dataIndex: "betType",
      align: "center",
      width: 170,
    },
    {
      title: wrapperHeader("Odds"),
      dataIndex: "odds",
      align: "center",
    },
    {
      title: wrapperHeader("Bet Time"),
      dataIndex: "betTime",
      align: "center",
      width: 100,
    },
    {
      title: wrapperHeader("Starts In"),
      dataIndex: "startsin",
      align: "center",
      width: 100,
    },
    {
      title: wrapperHeader("Home Team"),
      dataIndex: "homeTeam",
      align: "center",
      width: 150,
    },
    {
      title: wrapperHeader("Away Team"),
      dataIndex: "awayTeam",
      align: "center",
      width: 150,
    },
    {
      title: wrapperHeader("Amount"),
      dataIndex: "amount",
      align: "center",
    },
    {
      title: wrapperHeader("Wallet"),
      dataIndex: "wallet",
      align: "center",
    },
  ];

  return (
    <div>
      <h4 className={classes.title}>Recently Trade History</h4>
      <Table
        // expandable={{
        //   expandedRowRender: (record) => (
        //     <p style={{ margin: 0 }}>{record.description}</p>
        //   ),
        //   expandIcon: ({ expanded, onExpand, record }) =>
        //     expanded ? (
        //       <MinusCircleTwoTone onClick={(e) => onExpand(record, e)} />
        //     ) : (
        //       <PlusCircleTwoTone onClick={(e) => onExpand(record, e)} />
        //     ),
        // }}
        // pagination={{
        //   position: ["bottomCenter"],
        // }}
        rowClassName={classes.row}
        className={classes.tableMain}
        columns={columns}
        dataSource={historyData}
        size="middle"
      />
    </div>
  );
};

export default AllHistory;
