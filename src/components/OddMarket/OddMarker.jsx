import { isEmpty } from "lodash";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import constants from "../../constants";
import { cartActions } from "../../store/slices/cart-slice";
import { uiActions } from "../../store/slices/ui-slice";

import classes from "./OddMarket.module.css";

const OddMarker = ({ betType }) => {
  const dispatch = useDispatch();
  const params = useParams();
  const oddItemId = params.matchId;

  const handleClick = () => {
    // setBtnState((btnState) => !btnState);
    dispatch(uiActions.toggleOddsCart());
  };

  const addOddHandler = (index) => {
    // console.log("bookmarket", bookmaker);
    // dispatch(uiActions.toggleOddsCart());
    // const values = oddAllMatch?.bookmakers?.[0]?.bets.find()
    console.log("Index", index);
    console.log("betType", betType);
    const value = betType.values?.[index];
    console.log("values odd", value);
    let oddValue = "";
    if (!isEmpty(value)) {
      oddValue = value?.odd;
    } else {
      oddValue = "";
    }
    const side = {
      value: value?.value,
      id: index,
    };
    console.log("AddOddHandler OddMarker", {
      oddItemId,
      side,
      oddValue,
      betType,
    });
    dispatch(
      cartActions.addOddHandler({
        oddItemId,
        side,
        oddValue,
        betType,
      })
    );
  };

  return (
    <div className={classes.marketOddData}>
      <div className={classes.marketNameHeading}>
        <p className={classes.title}>
          <span>{betType.name}</span>
        </p>
        <div></div>
      </div>
      <div className={classes.marketDataCard}>
        <div className={classes.marketDataHeading}>
          <ul className={classes.marketListing}>
            <li className={classes.matchScoring}></li>
            <li></li>
          </ul>
        </div>
      </div>
      <div className={classes.mainMarketData}>
        {!isEmpty(betType.values) &&
          betType.values.map((item, index) => {
            return (
              <ul key={index}>
                <li>
                  <b>{item?.value}</b>
                </li>
                <li
                  className={classes.blueBox}
                  onClick={() => {
                    addOddHandler(index);
                    handleClick();
                  }}
                >
                  <b>{item?.odd}</b>
                </li>
              </ul>
            );
          })}
      </div>
      <div class={` ${classes.viewMarket}`}>
        <a href="/">View full market</a>
      </div>
    </div>
  );
};

export default OddMarker;