import React from "react";

import sportofiLogoOriginal from "../../assets/images/sportofi_logo_original.png";
import NavLinkMenu from "./Header/NavLinkMenu";

import classes from "./Header.module.css";
import SubHeaderComponentMain from "./Header/SubHeaderComponentMain";
import MenuItem from "./Header/MenuItem";

const Header = () => {
  return (
    <React.Fragment>
      <header className={`${classes.header}`}>
        <MenuItem />

        <a href="/">
          <img src={sportofiLogoOriginal} alt="Sportofi Logo" />
        </a>
        <NavLinkMenu />
      </header>
      <SubHeaderComponentMain />
    </React.Fragment>
  );
};

export default Header;
