import React from "react";
import "./Switch.css";
// import cx from "classnames";

const Switch = ({ isToggled, onClick }) => {
  return (
    <label className="switch">
      <input type="checkbox" checked={isToggled} onClick={onClick} />
      <span className="slider"></span>
    </label>
  );
};

export default Switch;
