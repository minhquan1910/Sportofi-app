import React, { useState } from "react";
import SubMenu from "../../SubMenu/SubMenu";
import classes from "./MenuItem.module.css";

const MenuItem = () => {
  const [show, setShow] = useState(false);
  const itemClick = () => {
    setShow((prevShow) => !prevShow);
  };

  return (
    // <form id="form">
    <div className={classes.menuBarMain}>
      <button
        className={classes.check}
        onClick={itemClick}
        style={{
          width: "40px",
          height: "30px",
          border: "none",
          background: "none",
        }}
      >
        {show ? (
          <i class={`fa-solid fa-xmark ${classes.menuBar}`}></i>
        ) : (
          <i class={`fa-solid fa-bars ${classes.menuBar}`}></i>
        )}
      </button>
      {show ? <SubMenu /> : null}
    </div>
    // </form>
  );
};

export default MenuItem;
