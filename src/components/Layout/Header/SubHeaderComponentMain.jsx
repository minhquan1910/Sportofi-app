import React from "react";
import { useDispatch, useSelector } from "react-redux";
import englandImage from "../../../assets/Flags/united-kingdom.png";
import itImage from "../../../assets/Flags/italy.png";
import zhImage from "../../../assets/Flags/china.png";
import koreaImage from "../../../assets/images/south-korea.png";
import japanImage from "../../../assets/images/japan.png";
import { useTranslation } from "react-i18next";
import classes from "./Subheader.module.css";
import downIconBold from "../../../assets/Flags/down-arrow-bold.svg";
import { uiActions } from "../../../store/slices/ui-slice";
import Wallet from "../../Wallet/Wallet";
import { useState } from "react";
const SubHeaderComponentMain = () => {
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();

  const showLanguageModalHandler = () => {
    dispatch(uiActions.toggleLanguageModal());
  };

  const showOddsModalHandler = () => {
    dispatch(uiActions.toggleOddsModal());
  };

  const showMultipleLanguage = useSelector(
    (state) => state.ui.multipleLanguageIsVisible
  );

  const showOdds = useSelector((state) => state.ui.oddsIsVisible);

  const langs = [
    {
      code: "en",
      img: englandImage,
      alt: "english",
      name: "EN",
    },
    {
      code: "cn",
      img: zhImage,
      alt: "china",
      name: "ZH",
    },
    {
      code: "jp",
      img: japanImage,
      alt: "japan",
      name: "JP",
    },
    {
      code: "kr",
      img: koreaImage,
      alt: "korea",
      name: "KO",
    },
  ];
  const [currentLang, setCurrentLang] = useState(langs[0]);

  const changeLanguage = (language) => {
    i18n.changeLanguage(language);
    const lang = langs.find((lang) => lang.code === language);
    setCurrentLang(lang);
  };
  const multiLanguageMenu = (
    <div className={classes["dropdown-menu"]}>
      {langs.map((lang, index) => (
        <button key={index} onClick={() => changeLanguage(lang.code)}>
          <img src={lang.img} alt={lang.alt} />
          <span>{lang.name}</span>
        </button>
      ))}
      {/* <button>
        <img src={itImage} alt="Italy" />
        <span>It</span>
      </button> */}
      {/* <button onClick={() => changeLanguage("cn")}>
        <img src={zhImage} alt="China" />
        <span>ZH</span>
      </button>
      <button>
        <img src={koreaImage} alt="Korean" />
        <span>ko</span>
      </button>
      <button onClick={() => changeLanguage("jp")}>
        <img src={japanImage} alt="Japan" />
        <span>ja</span>
      </button> */}
      {/* <button>
        <span>More...</span>
      </button> */}
    </div>
  );

  const oddsMenu = (
    <div className={classes["dropdown-menu-link"]}>
      <a href="/">{t("description.decimalOdds")}</a>
      <a href="/">American Odds</a>
    </div>
  );

  return (
    <React.Fragment>
      <div className={classes["header-main"]}>
        <div className={classes["main-left"]}>
          <nav>
            <div>
              <div className={classes.dropdown}>
                <button onClick={showLanguageModalHandler}>
                  <img src={currentLang.img} alt={currentLang.alt} />
                  <span>{currentLang.name}</span>
                  <img src={downIconBold} alt="" />
                </button>
                {showMultipleLanguage ? multiLanguageMenu : null}
              </div>
              <div id="google_translate_element"></div>
              <div className={classes.dropdown}>
                <button disabled onClick={showOddsModalHandler}>
                  <span className={classes.text}>
                    {t("description.decimalOdds")}
                  </span>
                  {/* <img src={downIconBold} alt="" /> */}
                </button>

                {/* {showOdds ? oddsMenu : null} */}
              </div>
              <a target={"_blank"} href="https://t.me/sportofi_global">
                {t("description.livehelp")}
              </a>
            </div>
          </nav>
        </div>
        <Wallet />
      </div>
    </React.Fragment>
  );
};

export default SubHeaderComponentMain;
