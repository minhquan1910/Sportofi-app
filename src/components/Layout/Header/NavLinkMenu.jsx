import React from "react";
import classes from "./NavLinkMenu.module.css";
import { useTranslation } from "react-i18next";
import { Link, NavLink } from "react-router-dom";

// import Link from "@material-ui/core/Link";
// import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";

const NavLinkMenu = () => {
  const { t } = useTranslation();
  return (
    <nav className={classes.navLink}>
      <ul className={classes.nav}>
        <li>
          <NavLink
            to="/"
            className={({ isActive }) =>
              isActive ? classes.active : undefined
            }
            end
          >
            {t("description.sports")}
          </NavLink>
        </li>
        {/* <li>
          <Link to="/inplay">{t("description.in-play")}</Link>
        </li> */}
        <li>
          <a className={classes["comming-soon"]}>
            {t("description.nft-esports")}
          </a>
        </li>
        {/* <li>
          <a  className={classes["comming-soon"]}>
            {t("description.fantasy-sports")}
          </a>
        </li> */}
        <li>
          <a className={classes["comming-soon"]}>
            {t("description.p2p-games")}
          </a>
        </li>
        <li>
          <a className={classes["comming-soon"]}>
            {t("description.tournaments")}
          </a>
        </li>
        <li>
          <NavLink
            to="/history"
            className={({ isActive }) =>
              isActive ? classes.active : undefined
            }
            end
          >
            {t("description.history")}
          </NavLink>
        </li>
        <li>
          <NavLink
            to="/faucet"
            className={({ isActive }) =>
              isActive ? classes.active : undefined
            }
            end
          >
            Faucet
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default NavLinkMenu;
