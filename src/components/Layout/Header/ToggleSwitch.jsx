import React from "react";
// import { Switch } from "antd";
// import Switch from "react-switch";
import Switch from "./Switch";
import classes from "./ToggleSwitch.module.css";
import { useDispatch } from "react-redux";
import { uiActions } from "../../../store/slices/ui-slice";

const ToggleSwitch = () => {
  const dispatch = useDispatch();

  const toggleSwitchHandler = () => {
    dispatch(uiActions.toggleSwitchButton());
  };

  return (
    <div className={classes.toggle}>
      <Switch onClick={toggleSwitchHandler} />
    </div>
  );
};

export default ToggleSwitch;
