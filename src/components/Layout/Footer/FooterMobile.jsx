import React, { useState } from "react";
import homeG from "../../../assets/Flags/active-home.svg";
import home from "../../../assets/Flags/home.svg";
import betHistoryG from "../../../assets/Flags/active_bet-history-g.svg";
import classes from "./FooterMobile.module.css";
import inPlay from "../../../assets/Flags/inplay.svg";
import inPlayG from "../../../assets/Flags/active_inplay-g.svg";
import livehelp from "../../../assets/Flags/help.svg";
import livehelpG from "../../../assets/Flags/active_help.svg";
import betHistory from "../../../assets/Flags/bet-history.svg";
import { Link, NavLink } from "react-router-dom";
import { useTranslation } from "react-i18next";

const FooterMobile = () => {
  const { t } = useTranslation();
  const [active, setActive] = useState({
    homeActive: true,
    inPlayActive: false,
    betActive: false,
    // withdrawalActive: false,
    liveHelpActive: false,
  });
  const itemHomeClick = () => {
    setActive({
      homeActive: true,
      betActive: false,
      inPlayActive: false,
      // withdrawalActive: false,
      liveHelpActive: false,
    });
  };

  const itemInPlayClick = () => {
    setActive({
      betActive: false,
      homeActive: false,
      inPlayActive: true,
      // withdrawalActive: false,
      liveHelpActive: false,
    });
  };

  const itemHistoryClick = () => {
    setActive({
      betActive: true,
      homeActive: false,
      inPlayActive: false,
      // withdrawalActive: false,
      liveHelpActive: false,
    });
  };
  const itemLiveClick = () => {
    setActive({
      betActive: false,
      homeActive: false,
      inPlayActive: false,
      // withdrawalActive: false,
      liveHelpActive: true,
    });
  };

  return (
    <footer className={classes["footer-mobile"]}>
      <div className={classes["footer-tabs"]}>
        <div className={classes.active}>
          <Link onClick={itemHomeClick} to={"/"}>
            <div className={classes["footer-tab"]}>
              {active.homeActive ? (
                <img src={homeG} alt="Home Icon" />
              ) : (
                <img src={home} alt="Home Icon" />
              )}
              <div className={classes["tab-name"]}>
                <span className={`${active.homeActive && classes.active}`}>
                  {t("description.home")}
                </span>
              </div>
            </div>
          </Link>
        </div>
        <div>
          <Link onClick={itemInPlayClick} to={"/live-sports"}>
            <div className={classes["footer-tab"]}>
              {active.inPlayActive ? (
                <img src={inPlayG} alt="Home Icon" />
              ) : (
                <img src={inPlay} alt="Home Icon" />
              )}
              <div className={classes["tab-name"]}>
                <span className={`${active.inPlayActive && classes.active}`}>
                  {t("description.inplay")}
                </span>
              </div>
            </div>
          </Link>
        </div>
        <div>
          <Link onClick={itemHistoryClick} to={"/history"}>
            <div className={classes["footer-tab"]}>
              {active.betActive ? (
                <img src={betHistoryG} alt="betHistory Icon" />
              ) : (
                <img src={betHistory} alt="betHistory Icon" />
              )}
              <div className={classes["tab-name"]}>
                <span className={`${active.betActive && classes.active}`}>
                  {t("description.bethistory")}
                </span>
              </div>
            </div>
          </Link>
        </div>
        <div>
          <a
            onClick={itemLiveClick}
            target={"_blank"}
            // href="/https://t.me/sportofi_global"
            href="https://t.me/sportofi_global"
          >
            <div className={classes["footer-tab"]}>
              {active.liveHelpActive ? (
                <img src={livehelpG} alt="livehelp Icon" />
              ) : (
                <img src={livehelp} alt="livehelp Icon" />
              )}
              <div className={classes["tab-name"]}>
                <span className={`${active.liveHelpActive && classes.active}`}>
                  {t("description.livehelp")}
                </span>
              </div>
            </div>
          </a>
        </div>
        {/* <div>
          <div className={classes["footer-tab"]}>
            <img src={help} alt="Help Icon" />
            <div className={classes["tab-name"]}>
              <span>{t("description.livehelp")}</span>
            </div>
          </div>
        </div> */}
      </div>
      <div className={classes["footer-glider"]}></div>
    </footer>
  );
};

export default FooterMobile;
