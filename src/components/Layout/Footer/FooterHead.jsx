import React from "react";
import classes from "./FooterHead.module.css";
import plusEightTeen from "../../../assets/Flags/plus18.svg";

const FooterHead = () => {
  return (
    <div className={classes["footer-head"]}>
      <div>
        <img src={plusEightTeen} alt="Plus 18" />
        <p>PLEASE GAMBLE RESPONSIBLY</p>
      </div>
    </div>
  );
};

export default FooterHead;
