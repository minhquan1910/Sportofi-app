import React from "react";
import classes from "./FooterBody.module.css";
import gcImage from "../../../assets/Flags/gc.png";
import bsImage from "../../../assets/Flags/bs.png";

const FooterBody = () => {
  return (
    <div className={classes["footer-body"]}>
      <div>
        <div>
          <img src={gcImage} alt="gc Logo" />
          <img src={bsImage} alt="bs Logo" />
        </div>
        <div>
          <p>
            2022 Sportofi Labs N.V. All rights reserved. Sportofi Labs N.V. is
            registered in accordance with Hong Kong law with company no. 159732
            whose registered office is Zuikertuintjeweg Z/N, Willemstad, Hong
            Kong.
          </p>
        </div>
      </div>
      <div>
        <a
          target={"blank"}
          href="https://docsend.com/view/h6rxjiey9txi68zf"
          className={classes.termPrivacy}
        >
          Terms & Conditions
        </a>
        {/* <div></div> */}
        {/* <a href="/responsible-gambling">Responsible Gambling Policy</a> */}
        <div></div>
        <a
          target={"blank"}
          href="https://docsend.com/view/q6ktqida7krb938p"
          className={classes.termPrivacy}
        >
          Privacy Policy
        </a>
        {/* <div></div> */}
        {/* <a href="/sports-betting-rules">Sports Exchange Rules</a> */}
      </div>
    </div>
  );
};

export default FooterBody;
