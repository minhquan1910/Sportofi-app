import React from "react";
import classes from "./FooterFoot.module.css";
import sportofiLogoWhiteText from "../../../assets/images/sportofi_logo_whitetext.png";
import discordIcon from "../../../assets/Flags/socialicon_w_discord.svg";
import telegramIcon from "../../../assets/Flags/socialicon_w_telegram.svg";
import twitterIcon from "../../../assets/Flags/socialicon_w_twitter.svg";
import linkedinIcon from "../../../assets/Flags/socialicon_w_linkedin.svg";

const FooterFoot = () => {
  return (
    <div className={classes["footer-foot"]}>
      <div>
        <a
          target={"blank"}
          href="https://docsend.com/view/h6rxjiey9txi68zf"
          className={classes.termPrivacy}
        >
          Terms & Conditions
        </a>

        <div className={classes["line"]}></div>
        <a
          target={"blank"}
          href="https://docsend.com/view/q6ktqida7krb938p"
          className={classes.termPrivacy}
        >
          Privacy Policy
        </a>
      </div>
      <div>
        <img
          className={classes.logo}
          src={sportofiLogoWhiteText}
          alt="BBS Icon"
        />
      </div>
      <div className={classes.iconSocial}>
        <a target={"blank"} href="https://discord.com/invite/QRuf74ZRV2">
          <img src={discordIcon} alt="Discord" />
        </a>
        <a href="https://t.me/sportofi_global">
          <img src={telegramIcon} alt="Telegram" />
        </a>
        <a target={"blank"} href="https://twitter.com/sportofi">
          <img src={twitterIcon} alt="Twitter" />
        </a>
        <a
          className={classes.icon}
          target={"blank"}
          href="https://www.linkedin.com/company/sportofi"
        >
          <img src={linkedinIcon} alt="Linkedin" />
        </a>
      </div>
      <div className={classes["medium-res"]}>
        <div className={classes["medium-resLogo"]}>
          <img src={sportofiLogoWhiteText} alt="BBS Icon" />
        </div>
        <div className={classes["medium-resRight"]}>
          <div className={classes["medium-resSocial"]}>
            <a target={"blank"} href="https://discord.com/invite/QRuf74ZRV2">
              <img src={discordIcon} alt="Discord" />
            </a>
            <a href="https://t.me/sportofi_global">
              <img src={telegramIcon} alt="Telegram" />
            </a>
            <a target={"blank"} href="https://twitter.com/sportofi">
              <img src={twitterIcon} alt="Twitter" />
            </a>
            <a
              className={classes.icon}
              target={"blank"}
              href="https://www.linkedin.com/company/sportofi"
            >
              <img src={linkedinIcon} alt="Linkedin" />
            </a>
          </div>
          <div className={classes["medium-resTer"]}>
            <a
              target={"blank"}
              href="https://docsend.com/view/h6rxjiey9txi68zf"
              className={classes.termPrivacy}
            >
              Terms & Conditions
            </a>

            <div className={classes["line"]}></div>
            <a
              target={"blank"}
              href="https://docsend.com/view/q6ktqida7krb938p"
              className={classes.termPrivacy}
            >
              Privacy Policy
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FooterFoot;
