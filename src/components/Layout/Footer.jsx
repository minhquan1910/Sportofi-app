import React from "react";
import classes from "./Footer.module.css";
// import FooterHead from "./Footer/FooterHead";
// import FooterBody from "./Footer/FooterBody";
import FooterFoot from "./Footer/FooterFoot";
import FooterMobile from "./Footer/FooterMobile";

const Footer = () => {
  return (
    <React.Fragment>
      <div className={classes.footer}>
        {/* <FooterHead />
        <FooterBody /> */}
        <FooterFoot />
      </div>
      <FooterMobile />
    </React.Fragment>
  );
};

export default Footer;
