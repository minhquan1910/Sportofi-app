import React from "react";
import classes from "./TimeLineBets.module.css";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import deleteBin from "../../assets/Flags/delete.svg";
import shapeThree from "../../assets/images/shape-3.png";
import { useDispatch, useSelector } from "react-redux";
import { cartActions } from "../../store/slices/cart-slice";
import { useState } from "react";
import { useEffect } from "react";
import constants from "../../constants";

import { Button } from "antd";
import { useNetwork, useAccount, useSwitchNetwork, useSigner } from "wagmi";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import { placeBetHandler } from "../Handlers";
import { fetchMatchData } from "../../store/actions/match-action";
import { fetchBetHistory } from "../../store/actions/betHistory-action";
import HistoryItem from "../HistoryItems";
import { fetchOpenBetHistory } from "../../store/actions/betHistory-action";
import { isEmpty } from "lodash";
import { uiActions } from "../../store/slices/ui-slice";
import { useTranslation } from "react-i18next";
const MyBets = (props) => {
  const { t } = useTranslation();
  const [loadingPlaceBet, setLoadingPlaceBet] = useState(false);
  const { switchNetwork } = useSwitchNetwork();
  const { chain } = useNetwork();
  const { isConnected, address } = useAccount();
  const [qualityInput, setQualityInput] = useState("1");
  const [disabledButton, setDisabledButton] = useState(true);
  const [nameOdd, setNameOdd] = useState("");
  const [tabIndex, setTabIndex] = useState(0);

  const [dataOdd, setDataOdd] = useState({});
  const oddData = useSelector((state) => state.cart.cartItem);
  const totalOdd = useSelector((state) => state.cart.totalOdd);
  const liability = useSelector((state) => state.cart.liability);
  const profit = useSelector((state) => state.cart.profit);
  const quality = useSelector((state) => state.cart.qualityStake);
  const matchDetails = useSelector((state) => state.match.matchItem);
  const tabCartIndex = useSelector((state) => state.ui.tabCartIsVisible);

  const dispatch = useDispatch();
  const betHistoryWallet = useSelector(
    (state) => state.betHistory.betHistoryItem
  );
  const openBetHistory = useSelector(
    (state) => state.betHistory.openBetHistoryData
  );
  const showConnectWallet = () => {
    dispatch(uiActions.toggleConnectWalletVisible());
  };
  useEffect(() => {
    dispatch(fetchOpenBetHistory(address));
  }, [dispatch, address]);
  useEffect(() => {
    dispatch(fetchBetHistory(address));
  }, [dispatch, address]);
  // console.log("BetHistory", betHistoryWallet);

  const { data: wagmiSigner } = useSigner({
    chainId: constants.CHAIN.bscChain.id,
  });

  const changeOddHandler = (e) => {
    console.log(e.target.value);
  };

  const changeQualityHandler = (e) => {
    dispatch(cartActions.addQualityStake(e.target.value));
    setQualityInput(e.target.value);
  };

  useEffect(() => {
    const data = oddData?.[0] ?? {};
    if (data?.odd && data?.odd !== 0) {
      setDisabledButton(false);
    } else {
      setDisabledButton(true);
    }
    dispatch(fetchMatchData(data?.id));
  }, [oddData, dispatch]);

  const submitHandler = async (event) => {
    event.preventDefault();

    // 1. check connected wallet
    if (!isConnected) {
      // failureModal("Error", "Please connect wallet first");
      showConnectWallet();
      return;
    }
    console.log("Submit");
    console.log("chain", chain);
    console.log("targetChain", constants.TARGET_NETWORK.chainId);
    // 2. check target network
    if (chain.id !== constants.TARGET_NETWORK.chainId) {
      switchNetwork(constants.TARGET_NETWORK.chainId);
      return;
    }
    const signer = await wagmiSigner;
    console.log("Signer ", signer);
    console.log("Data", dataOdd);
    const gameId = constants.game.SCORE.code;
    const matchId = dataOdd?.id;
    const odd = dataOdd?.odd;
    const settleStatus =
      dataOdd?.betType?.id ?? constants.DEFAULTS.BET_TYPE.code; // default only available for full match
    const side = dataOdd?.side?.id + 1;
    const amount = qualityInput;
    setLoadingPlaceBet(true);
    await placeBetHandler(
      gameId,
      matchId,
      odd,
      settleStatus,
      side,
      amount,
      address,
      signer
    );
    setLoadingPlaceBet(false);
  };

  useEffect(() => {
    if (!isEmpty(matchDetails) && !isEmpty(oddData)) {
      // console.log("OddData", oddData);
      // console.log("MatchDetail", matchDetails);
      const data = oddData?.[0] ?? {};
      setDataOdd(data);
      if (data?.betType?.id === constants.DEFAULTS.BET_TYPE.code) {
        if (data?.side?.id === 0) {
          setNameOdd(matchDetails?.teams?.home?.name);
        } else if (data.side?.id === 1) {
          setNameOdd(t("description.draw"));
        } else if (data.side?.id === 2) {
          setNameOdd(matchDetails?.teams?.away?.name);
        } else {
          setNameOdd("");
        }
      } else {
        console.log("data", data);
        setNameOdd(data?.side?.value);
      }
    }
  }, [matchDetails, oddData]);

  useEffect(() => {
    if (!isEmpty(tabCartIndex)) {
      setTabIndex(0);
    }
    console.log("tabIndex", tabIndex);
    console.log("tabCartIndex", tabCartIndex);
  }, [tabIndex,tabCartIndex]);

  return (
    <Tabs
      selectedIndex={tabIndex}
      onSelect={(index) => {
        console.log(index);
      }}
    >
      <div id="cart-bet" className={classes.navigation}>
        <TabList className={classes.navMain}>
          <Tab className={`${classes.nav}`}>
            <div>
              <button
                className={`${tabIndex === 0 ? classes.active : null}`}
                onClick={() => {
                  setTabIndex(0);
                  dispatch(uiActions.toggleTabCartVisible());
                }}
              >
                {t("description.betslip")}
              </button>
            </div>
          </Tab>
          <Tab className={classes.nav}>
            <div>
              <button
                className={`${tabIndex === 1 ? classes.active : null}`}
                onClick={() => {
                  setTabIndex(1);
                  dispatch(uiActions.resetTabCartIsVisible());
                }}
              >
                {t("description.openbets")}
              </button>
            </div>
          </Tab>
          <Tab className={classes.nav}>
            <div>
              <button
                className={` ${tabIndex === 2 ? classes.active : null}`}
                onClick={() => {
                  setTabIndex(2);
                  dispatch(uiActions.resetTabCartIsVisible());
                }}
              >
                {t("description.bethistory")}
              </button>
            </div>
          </Tab>
        </TabList>
        <TabPanel>
          <div className={classes["tab-content"]}>
            <div className={classes["heading-tab"]}>
              <div>
                <p>{t("description.selection")}</p>
                <span>
                  <img src={deleteBin} alt="Delete Bin" />
                </span>
              </div>
            </div>
            <form className={classes["body-tab"]} onSubmit={submitHandler}>
              <div className={classes["back-div"]}>
                <ul>
                  <li>
                    <p>{t("description.back")}</p>
                  </li>
                  <li>
                    <p>{t("description.odds")}</p>
                    <p>{t("description.stake")}</p>
                  </li>
                </ul>
              </div>
              <div className={classes["back-data"]}>
                <ul>
                  <li>
                    <div>
                      <p style={{ fontWeight: "bold", fontSize: "13px" }}>
                        {nameOdd}
                      </p>
                      <p>{dataOdd?.betType?.name}</p>
                    </div>
                  </li>
                  <li>
                    <div>
                      <input
                        type="number"
                        maxLength={10}
                        data-index={0}
                        step={0.01}
                        value={dataOdd?.odd}
                        onChange={changeOddHandler}
                      />
                    </div>
                    <div>
                      <input
                        type="number"
                        min={0}
                        max={50}
                        maxLength={10}
                        data-index={0}
                        step={1}
                        value={quality}
                        onChange={changeQualityHandler}
                      />
                    </div>
                  </li>
                </ul>
              </div>
              <div className={classes["payout-amt"]}>
                <ul>
                  <li></li>
                  <li>
                    <p>{t("description.payout")}</p>
                    <p>${totalOdd.toFixed(2)}</p>
                  </li>
                </ul>
              </div>
              <div className={classes["profit"]}>
                <p>
                  <span>{t("description.liability")}:</span>
                  <span>${liability}</span>
                </p>
                <p>
                  <span>{t("description.profit")} :</span>
                  <span>${profit.toFixed(2)}</span>
                </p>
              </div>
              <div className={classes["shape-img"]}>
                <img src={shapeThree} alt="Shape 3" />
              </div>
              <div className={classes["place-betsBtn"]}>
                {console.log()}
                <Button
                  loading={loadingPlaceBet}
                  type="primary"
                  style={{
                    backgroundColor: "#27aae1",
                  }}
                  onClick={submitHandler}
                  disabled={disabledButton ? true : false}
                >
                  {t("description.placebet")}
                </Button>
              </div>
            </form>
          </div>
        </TabPanel>

        <TabPanel>
          {!isConnected ? (
            <div className={classes.alert}>
              <span className={classes.desc}>
                {t("description.connectWalletToViewOpenBet")}
              </span>
            </div>
          ) : (
            openBetHistory.map((openBetHistory, index) => {
              return <HistoryItem key={index} betHistory={openBetHistory} />;
            })
          )}
        </TabPanel>
        <TabPanel>
          {!isConnected ? (
            <div className={classes.alert}>
              <span className={classes.desc}>
                {t("description.connectWalletToViewHistory")}
              </span>
            </div>
          ) : (
            betHistoryWallet.map((betHistory, index) => {
              return <HistoryItem key={index} betHistory={betHistory} />;
            })
          )}
        </TabPanel>
      </div>
    </Tabs>
  );
};

export default MyBets;