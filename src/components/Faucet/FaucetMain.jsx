import { ethers } from "ethers";
import constants from "../../constants";
import { useSigner, useAccount, useContractEvent } from "wagmi";
import { uiActions } from "../../store/slices/ui-slice";
import { useDispatch, useSelector } from "react-redux";
import { Button, Input, Table } from "antd";
import { useState, useEffect } from "react";
import { toString } from "lodash";
import { failureModal, successModal } from "../../helpers/modals";
import styles from "./styles.module.css";
import { fetchRanking } from "../../store/actions/ranks-action";
import { isEmpty } from "lodash";
const FaucetMain = () => {
  const dispatch = useDispatch();
  const rankingData = useSelector((state) => state.ranking.ranking);
  const countData = useSelector((state) => state.ranking.totalCount);
  console.log("rankingData", rankingData);
  console.log("countData", countData);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [ranking, setRanking] = useState([]);
  useEffect(() => {
    dispatch(fetchRanking(page, pageSize));
  }, [dispatch, page, pageSize]);
  useEffect(() => {
    if (!isEmpty(rankingData)) {
      const rankingDataSrc = rankingData.map((item, index) => {
        const rank = countData - ((page - 1) * pageSize + index);
        
        return {
          key: index,
          rank: "#" + rank,
          address: item?.address,
          balance: item?.balance,
        };
      });
      setRanking(rankingDataSrc);
    }
  }, [rankingData, countData, page, pageSize]);
  const columns = [
    {
      title: "Rank",
      dataIndex: "rank",
      key: "rank",
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Total Balance",
      dataIndex: "balance",
      key: "balance",
      defaultSortOrder: "descend",
      sorter: (a, b) => a.balance - b.balance,
    },
  ];
  const handlePageChange = (page, pageSize) => {
    console.log("page", page);
    console.log("pageSize", pageSize);
    setPage(page);
    setPageSize(pageSize);
  };
  const { isConnected } = useAccount();
  const [loading, setLoading] = useState(false);
  const { data: wagmiSigner } = useSigner({
    chainId: constants.CHAIN.bscChain.id,
  });
  useContractEvent({
    address: constants.FAUCET_ADDRESS,
    abi: constants.FAUCET_ABI,
    eventName: "Transfer",
    listener(node, label, owner) {
      console.log(node, label, owner);
    },
  });
  const sendFaucet = async () => {
    try {
      const faucet = new ethers.Contract(
        constants.FAUCET_ADDRESS,
        constants.FAUCET_ABI,
        wagmiSigner
      );
      console.log("Faucet ", faucet);
      console.log("Wagmi signer", wagmiSigner);
      if (isConnected) {
        setLoading(true);
        const address = document.getElementById("address").value;
        const tx = await faucet.mint(address);
        const transaction = await tx.wait();
        console.log("Give PMToken to ", transaction);
        setLoading(false);
        successModal("Success", "Give PMToken to " + address);
      } else {
        dispatch(uiActions.toggleConnectWalletVisible());
      }
    } catch (err) {
      setLoading(false);
      const error = { err };
      let reason = error.err.reason.substr(28);
      let reasonString = toString(reason);
      console.log("Faucet error", reasonString);
      if (reasonString.length !== 0) {
        if (reasonString.localeCompare("ALREADY_MINTED")) {
          failureModal("Error", "You have already received faucet");
        } else {
          failureModal("Error", "Faucet error");
        }
      }
    }
  };
  return (
    <div className={styles.wrapper}>
      <div className={styles.faucet}>
        <p className={styles.title}>
          SPORTOFI <br></br> FAUCET
        </p>
        <div className={styles.inputWrapper}>
          <span>Address: </span>
          <Input className={styles.input} type="text" id="address" />
        </div>

        <Button
          className={styles.button}
          loading={loading}
          onClick={sendFaucet}
        >
          Give me SPORTO
        </Button>
      </div>
      <div className={styles["table-info"]}>
        <Table
          className={styles["table-details"]}
          columns={columns}
          dataSource={ranking}
          pagination={{
            total: countData,
            pageSize,
            onChange: handlePageChange,
          }}
        />
      </div>
    </div>
  );
};

export default FaucetMain;
