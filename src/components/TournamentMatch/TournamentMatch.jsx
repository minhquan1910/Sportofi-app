import React from "react";
import { Link, useParams, useLocation } from "react-router-dom";
import TournamentMatchType from "../TournamentMatchType/TournamentMatchType";
import TournamentMatchTypeItem from "../TournamentMatchTypeItem/TournamentMatchTypeItem";
import classes from "./TournamentMatch.module.css";
import Moment from "react-moment";
import { Empty } from "antd";
import { isEmpty } from "lodash";

const TournamentMatch = ({ matchs }) => {
  const location = useLocation();
  const pathName = location.pathname;

  return (
    <React.Fragment>
      {!isEmpty(matchs) ? (
        matchs.map((match) => (
          <div className={classes["data-list"]} key={match?.fixture?.id}>
            <div className={classes["league-matchTeam"]}>
              <Link to={`${pathName}/match/${match?.fixture?.id}`}>
                <div className={classes["league-matchName"]}>
                  <div className={classes.leagueMatch}>
                    <img src={match?.teams?.home?.logo} alt="" />
                    <p>{match?.teams?.home?.name}</p>
                  </div>
                  <div className={classes.leagueMatch}>
                    <img src={match?.teams?.away?.logo} alt="" />
                    <p>{match?.teams?.away?.name}</p>
                  </div>
                  <p>
                    <Moment format="YYYY-MM-DD HH:mm">
                      {match?.fixture?.date}
                    </Moment>
                  </p>
                </div>
              </Link>
              <div className={classes["league-matchBlock"]}>
                <TournamentMatchType
                  key={match?.fixture?.id}
                  matchId={match?.fixture?.id}
                />
              </div>
            </div>
          </div>
        ))
      ) : (
        <Empty />
      )}
    </React.Fragment>
  );
};

export default TournamentMatch;
