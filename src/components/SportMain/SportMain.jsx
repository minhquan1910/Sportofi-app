import { isEmpty } from "lodash";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchMatchAllData } from "../../store/actions/match-action";
import Card from "../UI/Card";
import SportCompetition from "./SportCompetition/SportCompetition";
import SportInfo from "./SportInfo/SportInfo";

const SportMain = () => {
  const dispatch = useDispatch();
  // const [matchOfLeaguePL, setMatchOfLeaguePL] = useState([]);

  var today = new Date();
  var dd = String(today.getDate()).padStart(2, "0");
  var dayofWeek = String(today.getDate() + 7).padStart(2, "0");
  var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + "-" + mm + "-" + dd;
  var dateAfterSevenDays = yyyy + "-" + mm + "-" + dayofWeek;
  const matchOfLeaguePL = useSelector((state) => state.match.matchAll);

  useEffect(() => {
    dispatch(fetchMatchAllData(39, today, dateAfterSevenDays));
  }, [dispatch]);

  return (
    <React.Fragment>
      <Card>
        <SportCompetition />
        <SportInfo matchOfLeaguePLs={matchOfLeaguePL?.[1]} />
      </Card>
    </React.Fragment>
  );
};

export default SportMain;
