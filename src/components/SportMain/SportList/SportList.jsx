import { Empty } from "antd";
import { isEmpty } from "lodash";
import { useTranslation } from "react-i18next";
import SportMatch from "../SportInfo/SportMatch";
import styles from "./styles.module.css";
const SportList = ({ sports }) => {
  const { t } = useTranslation();
  return (
    <div
      className={styles["card-info"]}
      key={sports?.[0]?.fixture?.id ?? "unknown"}
    >
      <div className={styles["card-header"]}>
        <div className={styles["match-headername"]}>
          {!isEmpty(sports?.[0]?.league?.logo) && (
            <img src={sports?.[0].league?.logo} alt="international icon" />
          )}
          <p>{sports?.[0]?.league?.name ?? "Live Matches"}</p>
        </div>
        <div className={styles["match-type"]}>
          <ul>
            <li>1</li>
            <li>X</li>
            <li>2</li>
          </ul>
        </div>
      </div>
      <div className={styles["card-content"]}>
        {!isEmpty(sports) ? (
          sports?.map((sportItem, index) => (
            <SportMatch
              leagueId={sportItem?.league?.id}
              key={index}
              idCompetition={sportItem?.fixture?.id}
              matchs={sportItem}
            />
          ))
        ) : (
          <Empty description={t("description.nodata")} />
        )}
      </div>
    </div>
  );
};

export default SportList;
