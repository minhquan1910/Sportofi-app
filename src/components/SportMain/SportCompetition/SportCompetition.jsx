import React, { useEffect, useState } from "react";
import classes from "./SportCompetition.module.css";
import { OPTIONS as options } from "../../../services/api/options";
import SubCountryMobile from "../../Country/SubCountryMobile";
import SportCompetitionItem from "./SportCompetitionItem";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchLeaguesDataBySeason,
  fetchLeaguesOfWorldData,
} from "../../../store/actions/league-actions";
import { leagueActions } from "../../../store/slices/league-slice";
import { league } from "../../../constants/league";
import { useTranslation } from "react-i18next";

var year = new Date().getFullYear();

const SportCompetition = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [showActive, setShowActive] = useState({
    activePopular: true,
    activeCompetition: false,
  });
  const [showCountries, setShowCountries] = useState(false);

  const leagueDataBySeason = useSelector(
    (state) => state.league.leagueBySeason
  );

  const leagueWorldDataBySeason = useSelector(
    (state) => state.league.leagueOfWorldCurrent
  );

  useEffect(() => {
    dispatch(fetchLeaguesOfWorldData(year, "cup", "true"));
    dispatch(fetchLeaguesDataBySeason(year, "league", "true"));
  }, [dispatch]);

  const leaguesCommonDatas = leagueDataBySeason.concat(leagueWorldDataBySeason);

  const wordCup =
    leaguesCommonDatas &&
    leaguesCommonDatas.find(
      (leagueCommonData) =>
        leagueCommonData?.league?.name === league.world.worldCup
    );

  const championsLeague =
    leaguesCommonDatas &&
    leaguesCommonDatas.find(
      (leagueCommonData) =>
        leagueCommonData?.league?.name === league.world.championsLeague
    );

  const europaLeague =
    leaguesCommonDatas &&
    leaguesCommonDatas.find(
      (leagueCommonData) =>
        leagueCommonData?.league?.name === league.world.europaLeague
    );

  const premierLeague =
    leaguesCommonDatas &&
    leaguesCommonDatas.find(
      (leagueCommonData) =>
        leagueCommonData?.country?.name === "England" &&
        leagueCommonData?.league?.name === league.england.premierLeague
    );

  const laligaLeague =
    leaguesCommonDatas &&
    leaguesCommonDatas.find(
      (leagueCommonData) =>
        leagueCommonData?.league?.name === league.spain.laligaLeague
    );

  const serieALeague =
    leaguesCommonDatas &&
    leaguesCommonDatas.find(
      (leagueCommonData) =>
        leagueCommonData?.country?.name === "Italy" &&
        leagueCommonData?.league?.name === league.italy.serieALeague
    );

  const ligue1League =
    leaguesCommonDatas &&
    leaguesCommonDatas.find(
      (leagueCommonData) =>
        leagueCommonData?.country?.name === "France" &&
        leagueCommonData?.league?.name === league.france.ligue1League
    );

  const bundesligaLeague =
    leaguesCommonDatas &&
    leaguesCommonDatas.find(
      (leagueCommonData) =>
        leagueCommonData?.league?.name === league.germany.bundesligaLeague
    );

  const leaguesCommonData = [
    wordCup,
    championsLeague,
    europaLeague,
    premierLeague,
    laligaLeague,
    serieALeague,
    ligue1League,
    bundesligaLeague,
  ];

  return (
    <section className={classes.mainSection}>
      <h1 className={classes["main-header"]}>{t("description.football")}</h1>
      <div className={classes.panel}>
        <div className={classes.popular}>
          <div
            onClick={() => {
              setShowActive({ activePopular: true, activeCompetition: false });
              setShowCountries(false);
            }}
            className={showActive.activePopular ? classes.active : null}
          >
            <p>{t("description.popular")}</p>
          </div>
          <div
            onClick={() => {
              setShowActive({ activePopular: false, activeCompetition: true });
              setShowCountries(true);
            }}
            className={showActive.activeCompetition ? classes.active : null}
          >
            <p>{t("description.competitions")}</p>
          </div>
        </div>

        <h2>{t("description.topCompetitions")}</h2>
        <div className={classes["panel-info"]}>
          <ul>
            {leaguesCommonData.map((leagueCommon, index) => (
              <SportCompetitionItem
                key={index}
                idLeague={leagueCommon?.league?.id}
                image={leagueCommon?.league?.logo}
                nameLeague={leagueCommon?.league?.name}
              />
            ))}
          </ul>
          {showCountries && <SubCountryMobile />}
        </div>
      </div>
    </section>
  );
};

export default SportCompetition;
