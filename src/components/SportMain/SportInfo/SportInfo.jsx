import { Button } from "antd";
import { isEmpty } from "lodash";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchLiveMatchData } from "../../../store/actions/match-action";
import { fetchMatchAllData } from "../../../store/actions/match-action";
import { league } from "../../../constants/league";
import SportList from "../SportList";
import classes from "./SportInfo.module.css";
import { useTranslation } from "react-i18next";
const SportInfo = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [matchesLiveData, setLiveMatchesData] = useState([]);
  const [matchesUpcomingData, setMatchesUpcomingData] = useState([]);
  const [showMatchInLive, setShowMatchInLive] = useState(false);
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, "0");
  var dayofTwo = String(today.getDate() + 2).padStart(2, "0");
  var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
  var yyyy = today.getFullYear();
  today = yyyy + "-" + mm + "-" + dd;
  var dateAfterTwoDays = yyyy + "-" + mm + "-" + dayofTwo;
  const upComingMatches = useSelector((state) => state.match.matchAll);
  const liveMatches = useSelector((state) => state.match.matchLive);

  useEffect(() => {
    dispatch(fetchMatchAllData(39, today, dateAfterTwoDays));
    dispatch(fetchLiveMatchData());
  }, [dispatch]);

  useEffect(() => {
    if (!isEmpty(liveMatches)) {
      setLiveMatchesData(liveMatches[0]);
    }

    if (!isEmpty(upComingMatches)) {
      setMatchesUpcomingData(upComingMatches[0]);
    }
  }, [liveMatches, upComingMatches]);

  const matchesLive = matchesLiveData.filter(
    (matcheLiveData) =>
      matcheLiveData?.league?.name === league.world.worldCup ||
      matcheLiveData?.league?.name === league.world.championsLeague ||
      matcheLiveData?.league?.name === league.world.europaLeague ||
      matcheLiveData?.league?.name === league.world.friendliesCup ||
      (matcheLiveData?.league?.country === "England" &&
        (matcheLiveData?.league?.name === league.england.premierLeague ||
          matcheLiveData?.league?.name === league.england.fACup ||
          matcheLiveData?.league?.name === league.england.championship ||
          matcheLiveData?.league?.name === league.england.leagueOne ||
          matcheLiveData?.league?.name === league.england.leagueTwo ||
          matcheLiveData?.league?.name === league.england.eFLTrophy)) ||
      (matcheLiveData?.league?.country === "Spain" &&
        matcheLiveData?.league?.name === league.spain.laligaLeague) ||
      (matcheLiveData?.league?.country === "Italy" &&
        (matcheLiveData?.league?.name === league.italy.serieALeague ||
          matcheLiveData?.league?.name === league.italy.serieBLeague)) ||
      (matcheLiveData?.league?.country === "Germany" &&
        matcheLiveData?.league?.name === league.germany.bundesligaLeague) ||
      (matcheLiveData?.league?.country === "France" &&
        (matcheLiveData?.league?.name === league.france.ligue1League ||
          matcheLiveData?.league?.name === league.france.ligue2League))
  );

  useEffect(() => {
    if (!isEmpty(matchesLive)) {
      setShowMatchInLive(true);
    }
  }, [matchesLive]);

  const handleInPlayClicked = () => {
    // console.log("InPlay Clicked");
    // console.log("Live Matches", liveMatches);
    // setMatchesData(liveMatches?.[0] ?? []);
    setShowMatchInLive(true);
  };

  const handleUpcomingClicked = () => {
    //   console.log("Upcoming Clicked");
    //   console.log("Upcoming Matches", upComingMatches);
    setShowMatchInLive(false);
  };

  return (
    <section className={classes.sportInfoMain}>
      <div className={classes["header-info"]}>
        <ul>
          <li>
            <Button
              className={`${classes.navBtn} ${
                showMatchInLive ? classes.active : null
              }`}
              onClick={handleInPlayClicked}
            >
              {t("description.inplay")}
            </Button>
          </li>
          <li>
            <Button
              onClick={handleUpcomingClicked}
              className={`${classes.navBtn} ${
                showMatchInLive ? null : classes.active
              }`}
            >
              {t("description.upcoming")}
            </Button>
          </li>
        </ul>
      </div>
      <div className={classes["main-info"]}>
        <SportList
          sports={showMatchInLive ? matchesLive : matchesUpcomingData}
        />
      </div>

      {/* <RecentTrade /> */}
    </section>
  );
};

export default SportInfo;
