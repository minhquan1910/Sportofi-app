import React, { useEffect, useState } from "react";
import OddItem from "../../OddItem/OddItem";
import constants from "../../../constants";
import { OPTIONS as options } from "../../../services/api/options";

const SportMatchTypeItem = ({ oddMatchId }) => {
  const [odd, setOdd] = useState({});

  useEffect(() => {
    fetch(
      `https://api-football-v1.p.rapidapi.com/v3/odds?fixture=${oddMatchId}&bookmaker=${constants.DEFAULTS.BOOKMARKER.code}`,
      options
    )
      .then((response) => response?.json())
      .then(
        (response) =>
          response?.response?.length > 0 && setOdd(response?.response[0])
      )
      .catch((err) => console.error(err));
  }, [oddMatchId]);

  return <OddItem oddItemId={oddMatchId} odd={odd} />;
};

export default SportMatchTypeItem;
