import React from "react";
import SportMatchType from "./SportMatchType";
import classes from "./SportMatch.module.css";
import { Link } from "react-router-dom";
import Moment from "react-moment";
import { useTranslation } from "react-i18next";

const SportMatch = (props) => {
  const { t } = useTranslation();
  const calendarStrings = {
    lastDay: `[${t("description.yesterday")} at] LT`,
    sameDay: `[${t("description.today")} at] LT`,
    nextDay: `[${t("description.tomorrow")} at] LT`,
    lastWeek: "[last] dddd [at] LT",
    nextWeek: "dddd [at] LT",
    sameElse: "L",
  };

  return (
    <React.Fragment>
      <div className={classes["data-list"]} key={props.matchs?.fixture?.id}>
        <div className={classes["league-matchTeam"]}>
          <Link
            to={`/tournament/${props.leagueId}/match/${props.matchs?.fixture?.id}`}
          >
            <div className={classes["league-matchName"]}>
              <div className={classes.matchTeam}>
                <img src={props.matchs?.teams?.home?.logo} alt="" />

                <p>{props.matchs?.teams?.home?.name}</p>
              </div>
              <div className={classes.matchTeam}>
                <img src={props.matchs?.teams?.away?.logo} alt="" />
                <p>{props.matchs?.teams?.away?.name}</p>
              </div>

              <p>
                <Moment calendar={calendarStrings}>
                  {props.matchs?.fixture?.date}
                </Moment>
              </p>
            </div>
          </Link>
          <div className={classes["league-matchBlock"]}>
            <SportMatchType matchTypes={props.matchs?.fixture?.id} />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default SportMatch;
