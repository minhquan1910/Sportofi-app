import React from "react";
import SportMatchTypeItem from "./SportMatchTypeItem";
import classes from "./SportMatchType.module.css";

const SportMatchType = ({ matchTypes }) => {
  return (
    <ul className={classes["odds-list"]}>
      <SportMatchTypeItem oddMatchId={matchTypes} />
    </ul>
  );
};

export default SportMatchType;
