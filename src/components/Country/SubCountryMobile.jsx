import React, { useState, useEffect } from "react";
import internationalIcon from "../../assets/Flags/international-icon.svg";
import peruIcon from "../../assets/Flags/peru.png";
import england from "../../assets/Flags/england.png";
import spain from "../../assets/Flags/spain.png";
import germany from "../../assets/Flags/germany.png";
import france from "../../assets/Flags/france.png";
import italy from "../../assets/Flags/italy.png";
import unitedStateIcon from "../../assets/Flags/united-states.png";
import austria from "../../assets/Flags/austria.png";
import norway from "../../assets/Flags/norway.png";
import mexicoIcon from "../../assets/Flags/mexico.png";
import greece from "../../assets/Flags/greece.png";
import brazil from "../../assets/Flags/brazil.png";
import sweden from "../../assets/Flags/sweden.png";
import chile from "../../assets/Flags/chile.png";
import uruguay from "../../assets/Flags/uruguay.png";
import australia from "../../assets/Flags/australia.png";
import portugal from "../../assets/Flags/portugal.png";
import japan from "../../assets/Flags/japan.png";
import ireland from "../../assets/Flags/ireland.png";
import colombia from "../../assets/Flags/colombia.png";
import belgium from "../../assets/Flags/belgium.png";
import iceland from "../../assets/Flags/iceland.png";
import costaRica from "../../assets/Flags/costa-rica.png";
import scotland from "../../assets/Flags/scotland.png";
import china from "../../assets/Flags/china.png";
import finland from "../../assets/Flags/finland.png";
import denmark from "../../assets/Flags/denmark.png";
import switzerland from "../../assets/Flags/switzerland.png";
import argentina from "../../assets/Flags/argentina.png";
import turkey from "../../assets/Flags/turkey.png";
import netherlands from "../../assets/Flags/netherlands.png";
import classes from "./SubCountryMobile.module.css";
import upArrowIcon from "../../assets/Flags/up-arrow.svg";
import {
  fetchLeagueData,
  fetchLeagueDataByCountryName,
  fetchLeagueFranceData,
  fetchLeagueGermanyData,
  fetchLeagueItalyData,
  fetchLeagueSpainData,
} from "../../store/actions/league-actions";
import { useDispatch, useSelector } from "react-redux";
import League from "../League/League";
import { useTranslation } from "react-i18next";

const SubCountryMobile = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [showCompetition, setShowCompetition] = useState(false);
  const [showLeague, setShowLeague] = useState(false);
  const [showLeagueOfES, setShowLeagueOfES] = useState(false);
  const [showLeagueOfWorld, setShowLeagueOfWorld] = useState(false);
  const [showLeagueOfIT, setShowLeagueOfIT] = useState(false);
  const [showLeagueOfDE, setShowLeagueOfDE] = useState(false);
  const [showLeagueOfFR, setShowLeagueOfFR] = useState(false);

  const leagueOfWorlds = useSelector((state) => state.league.leagueOfWorld);
  const leagueOfFrances = useSelector(
    (state) => state.league.leagueFranceItems
  );
  const leagueOfSpains = useSelector((state) => state.league.leagueSpainItems);
  const leagueOfItalys = useSelector((state) => state.league.leagueItalyItems);
  const leagueOfGermanys = useSelector(
    (state) => state.league.leagueGermanyItems
  );
  const leagueOfEnglands = useSelector((state) => state.league.leagueItems);
  useEffect(() => {
    dispatch(fetchLeagueData("GB"));
    dispatch(fetchLeagueDataByCountryName("World"));
    dispatch(fetchLeagueSpainData("ES"));
    dispatch(fetchLeagueItalyData("IT"));
    dispatch(fetchLeagueGermanyData("DE"));
    dispatch(fetchLeagueFranceData("FR"));
  }, [dispatch]);

  const showLeagueOfWorldHandler = () => {
    setShowLeagueOfWorld((prevShowLeague) => !prevShowLeague);
    setShowLeague(false);
    setShowLeagueOfES(false);

    setShowLeagueOfES(false);
    // setShowLeagueOfIT(false);
    // setShowLeagueOfDE(false);
    // setShowLeagueOfFR(false);
  };

  const showLeagueHandler = () => {
    setShowLeague((prevShowLeague) => !prevShowLeague);
    setShowLeagueOfWorld(false);
    setShowLeagueOfES(false);
    setShowLeagueOfIT(false);
    setShowLeagueOfDE(false);
    setShowLeagueOfFR(false);
  };

  const showLeagueOfSpainHandler = () => {
    setShowLeagueOfES((prevShowLeague) => !prevShowLeague);
    setShowLeague(false);
    setShowLeagueOfWorld(false);
    setShowLeagueOfIT(false);
    setShowLeagueOfDE(false);
    setShowLeagueOfFR(false);
  };
  const showLeagueOfItalyHandlder = () => {
    setShowLeagueOfIT((prevShowLeague) => !prevShowLeague);
    setShowLeague(false);
    setShowLeagueOfWorld(false);
    setShowLeagueOfES(false);
    setShowLeagueOfDE(false);
    setShowLeagueOfFR(false);
  };
  const showLeagueOfGermanyHandler = () => {
    setShowLeagueOfIT(false);
    setShowLeague(false);
    setShowLeagueOfWorld(false);
    setShowLeagueOfES(false);
    setShowLeagueOfDE((prevShowLeague) => !prevShowLeague);
    setShowLeagueOfFR(false);
  };
  const showLeagueOfFranceHandler = () => {
    setShowLeagueOfIT(false);
    setShowLeague(false);
    setShowLeagueOfWorld(false);
    setShowLeagueOfES(false);
    setShowLeagueOfFR((prevShowLeague) => !prevShowLeague);
    setShowLeagueOfDE(false);
  };

  const showCompetitionHandler = () => {
    setShowCompetition((prevShowCompetition) => !prevShowCompetition);
  };

  const leaguesCommonOfWorld = leagueOfWorlds.filter(
    (leagueOfWorld) =>
      leagueOfWorld.country.name === "World" &&
      (leagueOfWorld.league.name === "World Cup" ||
        leagueOfWorld.league.name === "UEFA Europa League" ||
        leagueOfWorld.league.name === "UEFA Champions League" ||
        leagueOfWorld.league.name === "Friendlies Clubs")
  );

  const leagueOfWorldList = (
    <ul>
      {leaguesCommonOfWorld &&
        leaguesCommonOfWorld.map((leagueCommonOfWorld) => (
          <League
            key={leagueCommonOfWorld.league.id}
            leagueId={leagueCommonOfWorld.league.id}
            nameLeague={leagueCommonOfWorld.league.name}
          />
        ))}
    </ul>
  );
  const leaguesCommonOfSpain = leagueOfSpains.filter(
    (leagueSpainItems) =>
      leagueSpainItems.country.name === "Spain" &&
      leagueSpainItems.league.name === "La Liga"
  );
  const leagueOfSpainList = (
    <ul>
      {leaguesCommonOfSpain &&
        leaguesCommonOfSpain.map((leagueCommonOfSpain) => (
          <League
            key={leagueCommonOfSpain.league.id}
            leagueId={leagueCommonOfSpain.league.id}
            nameLeague={leagueCommonOfSpain.league.name}
          />
        ))}
    </ul>
  );

  const leaguesCommonOfItaly = leagueOfItalys.filter(
    (leagueOfItalys) =>
      leagueOfItalys.country.name === "Italy" &&
      (leagueOfItalys.league.name === "Serie A" ||
        leagueOfItalys.league.name === "Serie B")
  );
  const leagueOfItalyList = (
    <ul>
      {leaguesCommonOfItaly &&
        leaguesCommonOfItaly.map((leagueCommonOfItaly) => (
          <League
            key={leagueCommonOfItaly.league.id}
            leagueId={leagueCommonOfItaly.league.id}
            nameLeague={leagueCommonOfItaly.league.name}
          />
        ))}
    </ul>
  );

  const internationalChamipons = (
    <ul>
      <li>
        <a href="/">
          <span>FIFA World Cup 2022</span>
          <svg
            className="svg-inline--fa fa-chevron-right fa-w-10"
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="chevron-right"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 320 512"
            data-fa-i2svg=""
          >
            <path
              fill="currentColor"
              d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"
            ></path>
          </svg>
        </a>
      </li>
      <li>
        <a href="/">
          <span>UEFA Nations League</span>
          <svg
            className="svg-inline--fa fa-chevron-right fa-w-10"
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="chevron-right"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 320 512"
            data-fa-i2svg=""
          >
            <path
              fill="currentColor"
              d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"
            ></path>
          </svg>
        </a>
      </li>
      <li>
        <a href="/">
          <span>UEFA Europa League</span>
          <svg
            className="svg-inline--fa fa-chevron-right fa-w-10"
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="chevron-right"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 320 512"
            data-fa-i2svg=""
          >
            <path
              fill="currentColor"
              d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"
            ></path>
          </svg>
        </a>
      </li>
      <li>
        <a href="/">
          <span>UEFA Champions League</span>
          <svg
            className="svg-inline--fa fa-chevron-right fa-w-10"
            aria-hidden="true"
            focusable="false"
            data-prefix="fas"
            data-icon="chevron-right"
            role="img"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 320 512"
            data-fa-i2svg=""
          >
            <path
              fill="currentColor"
              d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"
            ></path>
          </svg>
        </a>
      </li>
    </ul>
  );

  const upArrow = (
    <img className={classes["up-arrow"]} src={upArrowIcon} alt="up row" />
  );

  const downArrow = (
    <svg
      className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
      aria-hidden="true"
      focusable="false"
      data-prefix="fa"
      data-icon="chevron-down"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 448 512"
      data-fa-i2svg=""
    >
      <path
        fill="currentColor"
        d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
      ></path>
    </svg>
  );

  const leaguesCommonOfEG = leagueOfEnglands.filter(
    (leagueOfEngland) =>
      leagueOfEngland.country.name === "England" &&
      (leagueOfEngland.league.name === "Premier League" ||
        leagueOfEngland.league.name === "Championship" ||
        leagueOfEngland.league.name === "League Two" ||
        leagueOfEngland.league.name === "FA Cup" ||
        leagueOfEngland.league.name === "League One" ||
        leagueOfEngland.league.name === "EFL Trophy")
  );

  const leaguesCommonOfGermany = leagueOfGermanys.filter(
    (leagueOfGermanys) =>
      leagueOfGermanys.country.name === "Germany" &&
      leagueOfGermanys.league.name === "Bundesliga"
  );
  const leagueOfGermanyList = (
    <ul>
      {leaguesCommonOfGermany &&
        leaguesCommonOfGermany.map((leagueCommonOfGermany) => (
          <League
            key={leagueCommonOfGermany.league.id}
            leagueId={leagueCommonOfGermany.league.id}
            nameLeague={leagueCommonOfGermany.league.name}
          />
        ))}
    </ul>
  );
  const leaguesCommonOfFrance = leagueOfFrances.filter(
    (leagueOfFrances) =>
      (leagueOfFrances.country.name === "France" &&
        leagueOfFrances.league.name === "Ligue 1") ||
      leagueOfFrances.league.name === "Ligue 2"
  );
  const leagueOfFranceList = (
    <ul>
      {leaguesCommonOfFrance &&
        leaguesCommonOfFrance.map((leagueCommonOfFrance) => (
          <League
            key={leagueCommonOfFrance.league.id}
            leagueId={leagueCommonOfFrance.league.id}
            nameLeague={leagueCommonOfFrance.league.name}
          />
        ))}
    </ul>
  );
  const leagueOfEnglandList = (
    <ul>
      {leaguesCommonOfEG &&
        leaguesCommonOfEG.map((leagueCommonOfEG) => (
          <League
            key={leagueCommonOfEG.league.id}
            leagueId={leagueCommonOfEG.league.id}
            nameLeague={leagueCommonOfEG.league.name}
          />
        ))}
    </ul>
  );
  return (
    <div className={classes.country}>
      <h1>{t("description.chooseCountry")}</h1>
      <div className={classes.accordion}>
        <div className={classes["accordian-heading"]}>
          <button onClick={showLeagueOfWorldHandler}>
            <div className={classes["internation-info"]}>
              <img
                src={internationalIcon}
                alt={t("description.international")}
              />
              <p>{t("description.international")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span className={showCompetition ? classes.active : null}>
                {leaguesCommonOfWorld.length}
              </span>
              {showLeagueOfWorld ? upArrow : downArrow}
            </div>
          </button>
          {showLeagueOfWorld ? leagueOfWorldList : null}
          <button onClick={showLeagueHandler}>
            <div className={classes["internation-info"]}>
              <img src={england} alt={t("description.england")} />
              <p>{t("description.england")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span className={showCompetition ? classes.active : null}>
                {leaguesCommonOfEG.length}
              </span>
              {showLeague ? upArrow : downArrow}
            </div>
          </button>
          {showLeague ? leagueOfEnglandList : null}
          <button onClick={showLeagueOfSpainHandler}>
            <div className={classes["internation-info"]}>
              <img src={spain} alt={t("description.spain")} />
              <p>{t("description.spain")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span className={showCompetition ? classes.active : null}>
                {leaguesCommonOfSpain.length}
              </span>
              {showLeagueOfES ? upArrow : downArrow}
            </div>
          </button>
          {showLeagueOfES ? leagueOfSpainList : null}
          <button onClick={showLeagueOfItalyHandlder}>
            <div className={classes["internation-info"]}>
              <img src={italy} alt={t("description.italy")} />
              <p>{t("description.italy")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span className={showCompetition ? classes.active : null}>
                {leaguesCommonOfItaly.length}
              </span>
              {showLeagueOfIT ? upArrow : downArrow}
            </div>
          </button>
          {showLeagueOfIT ? leagueOfItalyList : null}
          <button onClick={showLeagueOfGermanyHandler}>
            <div className={classes["internation-info"]}>
              <img src={germany} alt={t("description.germany")} />
              <p>{t("description.germany")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span className={showCompetition ? classes.active : null}>
                {leaguesCommonOfGermany.length}
              </span>
              {showLeagueOfDE ? upArrow : downArrow}
            </div>
          </button>
          {showLeagueOfDE ? leagueOfGermanyList : null}
          <button onClick={showLeagueOfFranceHandler}>
            <div className={classes["internation-info"]}>
              <img src={france} alt={t("description.france")} />
              <p>{t("description.france")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span className={showCompetition ? classes.active : null}>
                {leaguesCommonOfFrance.length}
              </span>
              {showLeagueOfFR ? upArrow : downArrow}
            </div>
          </button>
          {showLeagueOfFR ? leagueOfFranceList : null}
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={unitedStateIcon} alt={t("description.united")} />
              <p>{t("description.united")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={austria} alt={t("description.austria")} />
              <p>{t("description.austria")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={norway} alt={t("description.norway")} />
              <p>{t("description.norway")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={netherlands} alt={t("description.netherlands")} />
              <p>{t("description.netherlands")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={brazil} alt={t("description.brazil")} />
              <p>{t("description.brazil")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={peruIcon} alt={t("description.peru")} />
              <p>{t("description.peru")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={greece} alt={t("description.greece")} />
              <p>{t("description.greece")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={turkey} alt={t("description.turkey")} />
              <p>{t("description.turkey")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={belgium} alt={t("description.belgium")} />
              <p>{t("description.belgium")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={argentina} alt={t("description.argentina")} />
              <p>{t("description.argentina")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={switzerland} alt={t("description.switzerland")} />
              <p>{t("description.switzerland")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={sweden} alt={t("description.sweden")} />
              <p>{t("description.sweden")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={australia} alt={t("description.australia")} />
              <p>{t("description.australia")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={chile} alt={t("description.chile")} />
              <p>{t("description.chile")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={uruguay} alt={t("description.uruguay")} />
              <p>{t("description.uruguay")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={denmark} alt={t("description.denmark")} />
              <p>{t("description.denmark")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={japan} alt={t("description.japan")} />
              <p>{t("description.japan")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={costaRica} alt={t("description.costaRica")} />
              <p>{t("description.costaRica")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={finland} alt={t("description.finland")} />
              <p>{t("description.finland")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={scotland} alt={t("description.scotland")} />
              <p>{t("description.scotland")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={portugal} alt={t("description.portugal")} />
              <p>{t("description.portugal")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={colombia} alt={t("description.colombia")} />
              <p>{t("description.colombia")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={china} alt={t("description.china")} />
              <p>{t("description.china")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={mexicoIcon} alt={t("description.mexico")} />
              <p>{t("description.mexico")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={ireland} alt={t("description.ireland")} />
              <p>{t("description.ireland")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>

          <button disabled className={classes["invisible-country"]}>
            <div className={classes["internation-info"]}>
              <img src={iceland} alt={t("description.iceland")} />
              <p>{t("description.iceland")}</p>
            </div>
            <div className={classes["dropdown-number"]}>
              <span>1</span>
              <svg
                className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
                aria-hidden="true"
                focusable="false"
                data-prefix="fa"
                data-icon="chevron-down"
                role="img"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 448 512"
                data-fa-i2svg=""
              >
                <path
                  fill="currentColor"
                  d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                ></path>
              </svg>
            </div>
          </button>
        </div>
      </div>
    </div>
  );
};

export default SubCountryMobile;
