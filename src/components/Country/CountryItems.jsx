import React, { useEffect, useState } from "react";
import classes from "./CountryItems.module.css";
import upArrowIcon from "../../assets/Flags/up-arrow.svg";
import Country from "./Country";
import COUNTRY_DATA from "../../datas/Country";
import { data } from "autoprefixer";
import { useDispatch, useSelector } from "react-redux";
import { fetchCountryData } from "../../store/actions/country-action";

const CountryItems = (props) => {
  const dispatch = useDispatch();

  const [showCompetition, setShowCompetition] = useState(false);
  const [showLeague, setShowLeague] = useState(false);
  const countryAll = useSelector((state) => state.fixture.countryItem);

  useEffect(() => {
    dispatch(fetchCountryData);
  }, [dispatch]);

  const upArrow = (
    <img className={classes["up-arrow"]} src={upArrowIcon} alt="up row" />
  );

  const downArrow = (
    <svg
      className="svg-inline--fa fa-chevron-down fa-w-14 down-arrow-icon"
      aria-hidden="true"
      focusable="false"
      data-prefix="fa"
      data-icon="chevron-down"
      role="img"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 448 512"
      data-fa-i2svg=""
    >
      <path
        fill="currentColor"
        d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
      ></path>
    </svg>
  );
  const showLeagueHandler = () => {
    setShowLeague((prevShowLeague) => !prevShowLeague);
  };
  const showCompetitionHandler = () => {
    setShowCompetition((prevShowCompetition) => !prevShowCompetition);
  };

  return (
    <>
      <button onClick={showCompetitionHandler}>
        <div className={classes["internation-info"]}>
          {/* <img src={internationalIcon} alt="International Flag" /> */}
          <img src={props.image} alt="" />
          <p className={showCompetition ? classes.active : null}>
            {props.name}
          </p>
        </div>
        <div className={classes["dropdown-number"]}>
          <span className={showCompetition ? classes.active : null}>5</span>
          {showCompetition ? upArrow : downArrow}
        </div>
      </button>
    </>
  );
};

export default CountryItems;
