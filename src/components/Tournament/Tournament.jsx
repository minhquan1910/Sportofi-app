import React, { useEffect, useState } from "react";
import TimeLine from "../BlockRight/TimeLine";

import Country from "../Country/Country";
import SportPiece from "../Sports/SportPiece";
import Card from "../UI/Card";
import classes from "./Tournament.module.css";
import { useParams } from "react-router-dom";

import TournamentInfo from "../TournamentInfo/TournamentInfo";
import { useDispatch, useSelector } from "react-redux";
import { fetchFixtureUpcomingData, fetchFixtureLive } from "../../store/actions/fixture-action";
import { Button } from "antd";
import { isEmpty } from "lodash";
import { useTranslation } from "react-i18next";

const Tournament = () => {
  const { t } = useTranslation();
  const params = useParams();
  const dispatch = useDispatch();
  const idTournament = params.tournamentId;
  const fixtureUpComming = useSelector((state) => state.fixture.fixtureItem);
  const fixtureLive = useSelector((state) => state.fixture.fixtureLive);
  const [fixturesListData, setFixturesListData] = useState([]);
  const [showFixturedInLive, setShowFixturedInLive] = useState(false);

  useEffect(() => {
    dispatch(fetchFixtureLive(idTournament));
    dispatch(fetchFixtureUpcomingData(idTournament, 2022, "2023-03-28"));
  }, [dispatch, idTournament]);
  useEffect(() => {
    console.log("ShowFixturedInLive", showFixturedInLive);
    if(!showFixturedInLive){
      setFixturesListData(!isEmpty(fixtureUpComming) ? fixtureUpComming : []);
    }
    else{
      setFixturesListData(!isEmpty(fixtureLive) ? fixtureLive : []);
    }
  }, [fixtureUpComming, fixtureLive, showFixturedInLive])
  const handleInPlayClicked = () => {
    // console.log("InPlay Clicked");
    // console.log("Live Matches", liveMatches);
    // setMatchesData(liveMatches?.[0] ?? []);
    setShowFixturedInLive(true);
  };

  const handleUpcomingClicked = () => {
    //   console.log("Upcoming Clicked");
    //   console.log("Upcoming Matches", upComingMatches);
    setShowFixturedInLive(false);
  };

  return (
    <div className={classes.sporting}>
      <main className={classes.mainMain}>
        <SportPiece />
        <Country />
        <Card>
          <section className={classes.sportInfoMain}>
            <div className={classes["header-main"]}>
              {!isEmpty(fixturesListData[0]?.league?.logo) && (
                <img
                  src={fixturesListData[0]?.league?.logo}
                  alt={fixturesListData[0]?.league?.logo}
                />
              )}
              <h1>{fixturesListData[0]?.league?.name}</h1>
            </div>
            <div className={classes["header-info"]}>
              <ul>
                <li>
                  <Button onClick={handleInPlayClicked}
                    className={`${classes.navBtn} ${showFixturedInLive ? classes.active : null
                      }`}
                  >{t("description.inplay")}</Button>
                </li>
                <li>
                  <Button onClick={handleUpcomingClicked}
                    className={`${classes.navBtn} ${showFixturedInLive ? null : classes.active
                      }`}>
                    {t("description.upcoming")}
                  </Button>
                </li>
              </ul>
            </div>
            <div className={classes["main-info"]}>
              <TournamentInfo
                idTournament={idTournament}
                matchsTournament={fixturesListData}
              />
            </div>
          </section>
        </Card>
        <TimeLine />
      </main>
    </div>
  );
};

export default Tournament;
