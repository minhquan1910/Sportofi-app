import classes from "./ConnectedWallet.module.css";
import { isEmpty } from "lodash";
import { useEffect, useState, useCallback } from "react";
import clsx from "clsx";
import {
  useAccount,
  useDisconnect,
  useBalance
} from "wagmi";
import { getSysConnectors } from "../connectors";
import constants from "../../../constants";
import { useNetwork, useSwitchNetwork } from "wagmi";
import referralIcon from "../../../assets/images/refferal.svg";
import Card from "antd/lib/card/Card";
import { Col, Row, Tooltip, Tree, Typography, Grid } from "antd";
import { useTranslation } from "react-i18next";
import { CopyIcon } from "../../Icons/ExploreIcon";
import { useDispatch, useSelector } from "react-redux";
import { fetchAccount, saveAccount } from "../../../store/actions/account-action";
const { TreeNode } = Tree;
const { useBreakpoint } = Grid;
const ReferralSystem = () => {
  const { xs } = useBreakpoint();
  const dispatch = useDispatch();
  const { address, isConnected } = useAccount();
  const [linkReferral, setLinkReferral] = useState("");
  const [totalSystem, setTotalSystem] = useState(0);
  const [commission, setCommission] = useState(0);
  const [children, setChildren] = useState([]);
   
  const accountData = useSelector((state) => state.account.currentAccount);
  useEffect(() => {
    dispatch(fetchAccount(address));
  }, [dispatch, address])
  useEffect(() => {
    if (!isEmpty(accountData)) {
      console.log("accountData", accountData);
      const code = accountData.code;
      setTotalSystem(accountData.totalSystem);
      setCommission(accountData.commission);
      setChildren(accountData.children);
      setLinkReferral(`${window.location.origin}/invite/${code}`);
    } else {
      setTotalSystem(0);
      setCommission(0);
      setChildren([]);
      setLinkReferral("");
    }
  }, [address, isConnected, accountData]);

  const compactAddress = (address) => {
    const addressCompact = address.substring(0, 4) +
      "..." +
      address.substring(
        address.length - 4,
        address.length
      );
    return addressCompact;
  }
  const renderValue = (number) => {
    if (number > 0) {
      return xs ? number.toFixed(7) : number.toFixed(10);
    } else {
      return number;
    }
  };
  const handleCopy = (text) => {
    navigator.clipboard.writeText(text);
  };
  const renderNodes = (children) => {
    return children.map((child, index) => (
      <TreeNode
        selectable={false}
        title={
          <div
            className={clsx(classes.box, classes.nodeBox)}
          >
            <Tooltip
              title="Copied"
              trigger="click"
              placement="left"
              className={classes["node-tree"]}
            >
              <div className={classes.nodeLeft} onClick={() => handleCopy(child?.address)}>
                <Typography.Text
                  strong
                  style={{ color: "#fff" }}
                >
                  {compactAddress(child?.address)}
                </Typography.Text>
              </div>
            </Tooltip>
            <div className={classes.nodeRight}>
              Commission: {renderValue(child.commission)} BUSD
            </div>
          </div>
        }
      >
        {child.children && renderNodes(child.children)}
      </TreeNode>
    ))
  }
  return (

    <Card className={classes.card}>
      <Row gutter={[16, 16]}>
        <div className={classes.referralTitle}>Referral System</div>
        <Col span={24}>
          <div className={clsx(classes.addressBox, classes.box)} style={{ marginBottom: "5px" }}>
            <Typography.Text
              strong

            >
              <span className={classes["address-text"]}>
                {linkReferral}
              </span>
            </Typography.Text>
            <Tooltip title="Copied" trigger="click" placement="top">
              <span className={classes.iconCopy}>
                <CopyIcon
                  onClick={() => handleCopy(linkReferral)}
                  style={{
                    color: "#fff",
                    fontSize: 12,
                    marginLeft: "10px",
                  }}
                />
              </span>
            </Tooltip>
          </div>
          <div className={clsx(classes.addressBox, classes.box)}>
            <Typography.Text
              strong>
              <span className={classes["address-text"]}>
                {address}
              </span>
            </Typography.Text>
            <Tooltip title="Copied" trigger="click" placement="top">
              <span className={classes.iconCopy}>
                <CopyIcon
                  onClick={() => handleCopy(address)}
                  style={{
                    color: "#fff",
                    fontSize: 12,
                    marginLeft: "10px",
                  }}
                />
              </span>
            </Tooltip>
          </div>
        </Col>
        <Col span={24}>
          {/* <Spin> */}
          <div className={clsx(classes.infoTotalBox, classes.box)}>
            <Row gutter={4} style={{ width: "100%" }}>
              <Col span={12}>
                <span>Total System:</span>
              </Col>
              <Col span={12}>
                <span>{totalSystem} BUSD</span>
              </Col>
            </Row>
          </div>
          {/* </Spin> */}
        </Col>
        <Col span={24}>
          <div
            className={clsx(classes.infoCommissionlBox, classes.box)}
          >
            <Row gutter={4} style={{ width: "100%" }}>
              <Col span={12}>
                <span>Your Commission:</span>
              </Col>
              <Col span={12}>
                <span>{commission} BUSD</span>
              </Col>
            </Row>
          </div>
        </Col>
        <Col className={`${classes.col4}`} span={24}>
          <div className={classes.tree}>
            <Tree
              showLine={{ showLeafIcon: false }}
              defaultExpandAll={true}
              switcherIcon={null}
              className={classes.referralNode}
            >
              {
                children && renderNodes(children)
              }
            </Tree>
          </div>
        </Col>
      </Row>
    </Card>
  )
}
const ConnectedWallet = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { disconnect } = useDisconnect();
  const { address, connector, isConnected } = useAccount();
  const { switchNetwork } = useSwitchNetwork();
  const { chain } = useNetwork();
  const [showReferral, setShowReferral] = useState(false);
  const { data } = useBalance({
    addressOrName: address,
    chainId: constants.CHAIN.bscChain.id,
    token: constants.GTOKEN_ADDRESS,
    watch: true,
  });
  const [walletIcon, setWalletIcon] = useState("");
  const [showFeature, setShowFeature] = useState(false);
  const [offset, setOffset] = useState(0);
  const saveNewAccount = () => {
    console.log("saveNewAccount", address);
    const isInvite = window.location.href.includes("invite");
    const referralCode = window.location.href.split("/invite/")[1];
    console.log("isInvite", isInvite);
    console.log("referralCode", referralCode);
    if(referralCode && isInvite){
      console.log("Has code", referralCode);
      dispatch(saveAccount(address, referralCode));
    }
    else{
      console.log("No code");
      dispatch(saveAccount(address));
    }
  };
  useEffect(() => {
    if (address && isConnected) {
      console.log("address", address);
      saveNewAccount();
    }
  }, [address,isConnected]);
  useEffect(() => {
    const onScroll = () => setOffset(window.pageYOffset);
    // clean up code
    window.removeEventListener("scroll", onScroll);
    window.addEventListener("scroll", onScroll, { passive: true });
    return () => window.removeEventListener("scroll", onScroll);
  }, []);

  useEffect(() => {
    // console.log("connector", connector);
    if (connector) {
      if (chain.id !== constants.TARGET_NETWORK.chainId) {
        switchNetwork(constants.TARGET_NETWORK.chainId);
      }
      const sysConnectors = getSysConnectors(connector.id);
      if (sysConnectors) {
        // console.log("sysConnectors", sysConnectors);
        setWalletIcon(sysConnectors?.icon ?? "");
      }
    }
  }, [address, connector, chain, switchNetwork]);

  const showFeatureHandler = () => {
    setShowFeature((prevShow) => !prevShow);
  };

  const showReferralSystemHandler = () => {
    setShowReferral((prevShowReferral) => !prevShowReferral);
  };


  const amountSectionWithoutScroll = (
    <div className={classes["account-section-without-scroll"]}>
      <div className={classes["amount-inner-bg"]}>
        <div className={classes["wallet-icon"]}>
          <img
            src="https://beta.betswap.gg/images/metamask.svg"
            alt="meta icon"
          />
        </div>
        <div className={classes["font-18"]}>
          {t("description.balance")}
          <div className={classes["user-address"]}>
            {address}
          </div>
        </div>
        <div className={classes["mob-user-balance"]}>
          <div className={classes["mob-user-amount"]}>${data?.formatted}</div>
          <div className={classes["currency-type"]}>SPORTO</div>
        </div>
      </div>
      <div
        className={`${classes["bridge-swap-crypto-section"]} ${showFeature ? classes["bridge-swap-crypto-section-hidden"] : null
          }`}
      >
        <ReferralSystem />
      </div>
      <div className={classes["header-plus-icon"]} onClick={showFeatureHandler}>
        {showFeature ? (
          <img
            src="https://beta.betswap.gg/images/icons/plus.svg"
            className={classes["close-icon"]}
            alt="close"
          />
        ) : (
          <img
            className={classes["refferal-icon"]}
            src={referralIcon}
            alt="plus"
          />
        )}
      </div>
    </div>
  );

  const amountSectionScroll = (
    <div className={classes["account-section-with-scroll"]}>
      <div className={classes["amount-section"]}>
        <div className={classes["balance-section"]}>
          <div className={classes["font-18-scroll"]}>
            <span>{t("description.balance")}</span>
            <div className={classes["mob-user-balance-scroll"]}>
              <span className={classes["mob-user-balance-scroll-amount"]}>
                ${data?.formatted}
              </span>
              {/* <span className={classes["currency-type"]}>{data?.symbol}</span> */}
              <span className={classes["currency-type"]}>SPORTO</span>
            </div>
          </div>
          <div className={classes["meta-icon-scroll"]}>
            <div className={classes["wallet-icon-box"]}>
              <img
                src="https://beta.betswap.gg/images/metamask.svg"
                alt="meta icon"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );

  return (
    <>
      <div className={classes["sub-header-right-section"]}>
        <div className={classes["user-balance-logout"]}>
          <div className={classes["sub-header-balance"]}>
            <div>
              <p className={classes["sub-header-balance-text"]}>
                <span className={classes.text}>{t("description.balance")}</span>
                <span className={classes.address}>
                  {address}
                </span>
              </p>
              <div className={classes["meta-icon"]}>
                <img src={walletIcon} alt="meta icon" />
              </div>
            </div>
            <div className={classes["user-balance"]}>
              {data?.formatted}
              <span>${"SPORTO"}</span>
            </div>
          </div>

          <div className={classes["user-profile"]}>
            <button onClick={disconnect} title="Logout">
              <img
                src="https://beta.betswap.gg/images/icons/logout.svg"
                alt="logout"
              />
            </button>
            <button onClick={showReferralSystemHandler} title="Referral">
              <img src={referralIcon} alt="Referral" />
            </button>
          </div>
        </div>
        {showReferral && (
          <div className={classes["referral-sec"]}>
            <ReferralSystem />
          </div>
        )}
      </div>
      {offset > 0 ? amountSectionScroll : amountSectionWithoutScroll}
    </>
  );
};

export default ConnectedWallet;
