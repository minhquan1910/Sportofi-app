import React, { useEffect, useCallback } from "react";
import Modal from "../UI/Modal";

import { CloseCircleOutlined } from "@ant-design/icons";
import { uiActions } from "../../store/slices/ui-slice";
import { useDispatch, useSelector } from "react-redux";
import { Grid } from "antd";
import { sysConnectors, sysConnectorsMobile } from "./connectors";
import { useConnect } from "wagmi";
import { failureModal } from "../../helpers/modals";
import styles from "./styles.module.css";
import { useTranslation } from "react-i18next";
import Metamask from "../../assets/wallets/metamask.png";
import { MetaMaskConnector } from "wagmi/connectors/metaMask";
const { useBreakpoint } = Grid;
const WalletModal = () => {
  const {ethereum} = window;
  const { t } = useTranslation();
  const { connect, connectors, error, isLoading, pendingConnector, data } = useConnect();
  const { md } = useBreakpoint();
  let systemConnectors = md ? sysConnectors : sysConnectorsMobile;
  const dispatch = useDispatch();
  useEffect(() => {
    systemConnectors.forEach((connector) => {
      const wagConnector = connectors.find((c) => c.id === connector.code);
      if (wagConnector) {
        connector.connector = wagConnector;
      }
    });
  }, [md, systemConnectors, connectors]);
  const closeConnectWallet = useCallback(() => {
    dispatch(uiActions.toggleConnectWalletVisible());
  }, [dispatch]);
  useEffect(() => {
    if (isLoading || error) {
      console.log("loading");
      closeConnectWallet();
    }
    if (pendingConnector) {
      console.log("pending", pendingConnector);
    }
    if (error) {
      console.error("Connect Wallet", error.message);
      failureModal(error.name, error.message);
    }
  }, [isLoading, error, pendingConnector, closeConnectWallet]);
  return (
    <Modal onClose={closeConnectWallet}>
      <div className={styles["modal-content"]}>
        <CloseCircleOutlined
          style={{
            fontSize: "30px",
            float: "right",
            paddingRight: "20px",
            paddingTop: "10px",
          }}
          onClick={closeConnectWallet}
        />
        <div>
          <p style={{ paddingLeft: "25px" }}>
            {t("description.connectWallet")}
          </p>
          {md?
            <div className={styles.gridMain}>
                {systemConnectors.map((connector) => {
                  return (
                    <div
                      className={styles.flexMain}
                      key={connector.code}
                      onClick={() => {
                        console.log("Connector Clicked", connector);
                        if (connector.connector) {
                          console.log("Connector", {connector})
                          connect({ connector: connector.connector })
                        } else {
                          closeConnectWallet();
                          failureModal("Error", "This wallet is not supported yet");
                        }
                      }}
                    >
                      <img src={connector.icon} alt={connector.title} />
                      <span>{connector.title ?? connector.name}</span>
                    </div>
                  );
                })}
            </div> : 
            <div onClick={()=>{
              if(ethereum && ethereum.isMetaMask){
                const metaMask = connectors.find((c) => c.id === "metaMask")
                connect({connector : metaMask})
              }else{
                window.location.href = `https://metamask.app.link/dapp/${window.location.host}`
              }
            }}>
              <img src={Metamask}/>
            </div>
          }
        </div>
      </div>
    </Modal>
  );
};

export default WalletModal;
