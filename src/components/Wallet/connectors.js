import Metamask from "../../assets/wallets/metamask.png";
import Coin98 from "../../assets/wallets/coin98.png";
import WalletConnect from "../../assets/wallets/wallet-connect.svg";
import MathWallet from "../../assets/wallets/math-wallet.svg";
import TokenPocket from "../../assets/wallets/token-pocket.png";
import SafePal from "../../assets/wallets/safepal.png";
import TrustWallet from "../../assets/wallets/trust-wallet.png";
import coinbase from "../../assets/wallets/coinbase.svg";
import rainbow from "../../assets/wallets/rainbow.svg";
export const sysConnectors = [
  {
    code: "metaMask",
    title: "Metamask",
    icon: Metamask,
  },
  {
    code: "walletConnect",
    title: "Wallet Connect",
    icon: WalletConnect,
  },
  {
    code: "coinbaseWallet",
    title: "Coinbase Wallet",
    icon: coinbase,
  },
  {
    code: "trustWallet",
    title: "Trust Wallet",
    icon: TrustWallet,
  },
  {
    code: "mathWallet",
    title: "MathWallet",
    icon: MathWallet,
  },
  {
    code: "tokenPocket",
    title: "TokenPocket",
    icon: TokenPocket,
  },
  {
    code: "safePal",
    title: "SafePal",
    icon: SafePal,
  },
  {
    code: "coin98",
    title: "Coin98",
    icon: Coin98,
  },
];

export const sysConnectorsMobile = [
  {
    code: "metaMask",
    title: "Metamask",
    icon: Metamask,
  },
  {
    code: "walletConnect",
    title: "Wallet Connect",
    icon: WalletConnect,
  },
];
export const getSysConnectors = (code, isMobile = false) => {
  const connectors = isMobile ? sysConnectorsMobile : sysConnectors;
  return connectors.find((connector) => connector.code === code);
}