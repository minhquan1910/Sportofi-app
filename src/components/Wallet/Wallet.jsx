import { useDispatch } from "react-redux";
import { useAccount, useConnect } from "wagmi";
import walletIcon from "../../assets/images/wallet-icon.svg";
import styles from "./styles.module.css";
import ConnectedWallet from "./ConnectedWallet";
import { uiActions } from "../../store/slices/ui-slice";
import { useTranslation } from "react-i18next";
function Wallet() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const showConnectWallet = () => {
    dispatch(uiActions.toggleConnectWalletVisible());
  };
  const { connector, isConnected } = useAccount();
  const { isLoading, pendingConnector } = useConnect();
  return (
    <>
      {isConnected ? (
        <ConnectedWallet />
      ) : (
        <div className={styles.connectWrapper}>
          <button onClick={() => showConnectWallet()}>
            <img src={walletIcon} alt="Wallet icon" />
            {isLoading && connector.id === pendingConnector.id ? (
              <span>Connecting...</span>
            ) : (
              <p>{t("description.connectYourWallet")}</p>
            )}
          </button>
        </div>
      )}
    </>
  );
}

export default Wallet;
