import Icon from "@ant-design/icons";
import { ReactComponent as Copy } from "./svgs/copy.svg";
export const CopyIcon = (props) => {
  return <Icon component={Copy} {...props} />;
};
