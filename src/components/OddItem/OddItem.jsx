import React, { useState } from "react";
import classes from "./OddItem.module.css";
import classNames from "classnames";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { isEmpty } from "lodash";
import { cartActions } from "../../store/slices/cart-slice";
import constants from "../../constants";
import { uiActions } from "../../store/slices/ui-slice";
import LockIcon from "@material-ui/icons/Lock";
const OddItem = ({ oddItemId, odd }) => {
  const [btnState, setBtnState] = useState(false);
  const [disabledButton, setDisabledButton] = useState(true);
  const dispatch = useDispatch();
  // console.log("oddId", oddItemId);
  // console.log("odd", odd);

  const [home, setHome] = useState("");
  const [draw, setDraw] = useState("");
  const [away, setAway] = useState("");
  const handleClick = () => {
    setBtnState((btnState) => !btnState);
    dispatch(uiActions.toggleOddsCart());
    dispatch(uiActions.toggleTabCartVisible());
  };
  const myClassNames = classNames("button", {
    active: btnState,
  });
  useEffect(() => {
    const bookmarker = odd?.bookmakers?.[0];
    const bets = bookmarker?.bets ?? [];
    const bet =
      bets.find((bet) => bet?.id === constants.DEFAULTS.BET_TYPE.code) ?? {};
    const value = bet?.values ?? [];
    // console.log("value", value);
    if (!isEmpty(value)) {
      setDisabledButton(false);
      setHome(value?.[0]?.odd || <LockIcon />);
      setDraw(value?.[1]?.odd || <LockIcon />);
      setAway(value?.[2]?.odd || <LockIcon />);
    } else {
      setDisabledButton(true);
      setHome(<LockIcon />);
      setDraw(<LockIcon />);
      setAway(<LockIcon />);
    }
  }, [odd, oddItemId]);

  const addOddHandler = (index) => {
    // dispatch(uiActions.toggleOddsCart());
    const bookmarker = odd?.bookmakers?.[0] ?? [];
    const bets = bookmarker?.bets ?? [];
    const bet =
      bets.find((bet) => bet?.id === constants.DEFAULTS.BET_TYPE.code) ?? {};
    const betType = !isEmpty(bet)
      ? {
          id: bet?.id,
          name: bet?.name,
        }
      : {
          id: constants.DEFAULTS.BET_TYPE.code,
          name: constants.DEFAULTS.BET_TYPE.name,
      };
    const value = bet?.values?.[index] ?? [];
    let oddValue = "";

    if (!isEmpty(value)) {
      oddValue = value?.odd;
    } else {
      oddValue = "";
    }
    const side = {
      value: value?.value,
      id: index,
    };

    dispatch(cartActions.addOddHandler({ oddItemId, side, oddValue, betType }));
  };

  return (
    <>
      <li className={`${classes["odd-item"]} `}>
        <button
          className={myClassNames ? "btn" : null}
          onClick={() => {
            addOddHandler(0);
            handleClick();
          }}
          disabled={disabledButton}
        >
          {home}
        </button>
      </li>
      <li className={`${classes["odd-item"]} `}>
        <button
          className={myClassNames ? "btn" : null}
          onClick={() => {
            addOddHandler(1);
            handleClick();
          }}
          disabled={disabledButton}
        >
          {draw}
        </button>
      </li>
      <li className={`${classes["odd-item"]} `}>
        <button
          className={myClassNames ? "btn" : null}
          onClick={() => {
            addOddHandler(2);
            handleClick();
          }}
          disabled={disabledButton}
        >
          {away}
        </button>
      </li>
    </>
  );
};

export default OddItem;