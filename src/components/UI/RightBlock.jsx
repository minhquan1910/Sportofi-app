import React from "react";
import classes from "./RightBlock.module.css";

const RightBlock = (props) => {
  return <div className={classes["right-main"]}>{props.children}</div>;
};

export default RightBlock;
