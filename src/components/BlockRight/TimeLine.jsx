import React from "react";
import classes from "./TimeLine.module.css";

import RightBlock from "../UI/RightBlock";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import MyBets from "../MyBets/MyBets";
import RecentTrade from "../RecentTradeHistory";
const TimeLine = (props) => {
  return (
    <RightBlock>
      <section className={classes.right}>
        <MyBets />
        <RecentTrade />
      </section>
    </RightBlock>
  );
};

export default TimeLine;
