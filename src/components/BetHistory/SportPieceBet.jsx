import React from "react";
import ballIcon from "../../assets/Flags/ball-icon.svg";
import iceHockey from "../../assets/Flags/35232-g.svg";
import basketBall from "../../assets/Flags/48242-g.svg";
import tennis from "../../assets/Flags/54094-g.svg";
import americaFootball from "../../assets/Flags/131506-g.svg";
import baseBall from "../../assets/Flags/154914-g.svg";
import boxing from "../../assets/Flags/154919-g.svg";
import darts from "../../assets/Flags/154923-g.svg";
import snooker from "../../assets/Flags/262622-g.svg";
import RugbyLeague from "../../assets/Flags/274792-g.svg";
import AustralianRules from "../../assets/Flags/389537-g.svg";
import Cricket from "../../assets/Flags/452674-g.svg";
import EGames from "../../assets/Flags/687890-g.svg";
import MMA from "../../assets/Flags/154919001-g.svg";
import classes from "./SportPieceBet.module.css";
// import golIcon from "../../assets/Flags/gol-black.svg";
// import golIconWhite from "../../assets/Flags/gol-white.svg";
// import volleyBallIcon from "../../assets/Flags/volleyball-black.svg";
// import volleyBallWhiteIcon from "../../assets/Flags/volleyball-white.svg";

const SportPieceBet = () => {
  // const [showSportPiece, setShowSportPiece] = useState(false);

  const showSportPieceHandler = () => {
    // setShowSportPiece((prevShowSportPiece) => !prevShowSportPiece);
  };

  return (
    <div className={classes.sportPiece}>
      <ul>
        <li className={classes.active}>
          <button onClick={showSportPieceHandler}>
            <a href="/">
              <img src={ballIcon} alt="Ball Icon" />
            </a>
          </button>
        </li>
        <li>
          <button>
            {/* {showSportPiece ? (
              <img src={golIcon} alt="Ball Icon" />
            ) : (
              )} */}
            <a href="/">
              <img src={iceHockey} alt="iceHockey" />
            </a>
          </button>
        </li>
        <li>
          <button>
            <a href="/">
              <img src={basketBall} alt="basketBall" />
            </a>
          </button>
        </li>
        <li>
          <button>
            <a href="/">
              <img src={tennis} alt="tennis" />
            </a>
          </button>
        </li>
        <li>
          <button>
            <a href="/">
              <img src={americaFootball} alt="america Football" />
            </a>
          </button>
        </li>
        <li>
          <button>
            <a href="/">
              <img src={baseBall} alt="baseBall" />
            </a>
          </button>
        </li>

        <li>
          <button>
            <a href="/">
              <img src={boxing} alt="boxing" />
            </a>
          </button>
        </li>
        <li>
          <button>
            <a href="/">
              <img src={darts} alt="darts" />
            </a>
          </button>
        </li>
        <li>
          <button>
            <a href="/">
              <img src={snooker} alt="snooker" />
            </a>
          </button>
        </li>
        <li>
          <button>
            <a href="/">
              <img src={RugbyLeague} alt="Rugby League" />
            </a>
          </button>
        </li>
        <li>
          <button>
            <a href="/">
              <img src={AustralianRules} alt="Australian Rules" />
            </a>
          </button>
        </li>
        <li>
          <button>
            <a href="/">
              <img src={Cricket} alt="Cricket" />
            </a>
          </button>
        </li>
        <li>
          <button>
            <a href="/">
              <img src={EGames} alt="E-Games" />
            </a>
          </button>
        </li>
        <li>
          <button>
            <a href="/">
              <img src={MMA} alt="MMA" />
            </a>
          </button>
        </li>
      </ul>
    </div>
  );
};

export default SportPieceBet;
