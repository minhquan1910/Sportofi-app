import React from "react";
import classes from "./TimeLine.module.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import MyBets from "../MyBets/MyBets";
const BetTimeLineHistory = (props) => {
  return (
    <div className={classes.mybets}>
      <MyBets />
    </div>
  );
};

export default BetTimeLineHistory;
