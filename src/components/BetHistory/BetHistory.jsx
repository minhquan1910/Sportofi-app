import { Card, DatePicker, Space } from "antd";
import React, { useEffect, useState } from "react";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import TimeLine from "../BlockRight/TimeLine";
import Country from "../Country/Country";
import SportPiece from "../Sports/SportPiece";
import classes from "./BetHistory.module.css";
import calendar from "../../assets/images/calendar.svg";
import moment from "moment";
import ProfileAvatar from "./ProfileAvatar";
import BetTimeLineHistory from "./BetTimeLineHistory";
import SportPieceBet from "./SportPieceBet";
import MyBets from "../MyBets/MyBets";
import BetMobile from "../BetMobile/BetMobile";
const BetHistory = () => {
  const dateFormat = "YYYY/MM/DD";
  const weekFormat = "MM/DD";
  const monthFormat = "YYYY/MM";
  const dateFormatList = ["DD/MM/YYYY", "DD/MM/YY"];
  const customFormat = (value) => `custom format: ${value.format(dateFormat)}`;
  const customWeekStartEndFormat = (value) =>
    `${moment(value).startOf("week").format(weekFormat)} ~ ${moment(value)
      .endOf("week")
      .format(weekFormat)}`;

  const [offset, setOffset] = useState(0);

  useEffect(() => {
    const onScroll = () => setOffset(window.pageYOffset);
    // clean up code
    window.removeEventListener("scroll", onScroll);
    window.addEventListener("scroll", onScroll, { passive: true });
    return () => window.removeEventListener("scroll", onScroll);
  }, []);

  return (
    <>
      <div className={classes.sporting}>
        <main className={classes.mainMain}>
          <SportPieceBet />
          <Country />
          <Card
            className={`${classes.main} ${
              offset === 0 ? classes["marginHeader"] : null
            }`}
          >
            {/* <div className={classes.sectionMain}>
              <div className={classes.profileWrapper}>
                <div className={classes.profileContainer}>
                  <div className={classes.profile}>
                   
                    <ProfileAvatar />

                    <div className={classes.profileTitle}>My Bet History</div>
                    <div className={classes.profileAdd}>
                      <span className={classes.profileSub} title="Copy address">
                        <span className="pr-2">0x09f84...d700f9</span>
                        <i class="far fa-copy"></i>
                      </span>
                    </div>
                  </div>
                  <div className={classes.profileInfoData}>
                    <div className={classes.profileInfo}>
                      <div className={classes.profileValue}>0</div>
                      <div className={classes.profileLabel}>Total Bets</div>
                    </div>
                    <div className={classes.profileInfo}>
                      <div className={classes.profileValue}>$0</div>
                      <div className={classes.profileLabel}>Total Winnings</div>
                    </div>
                    <div className={classes.profileInfo}>
                      <div className={classes.profileValue}>$0</div>
                      <div className={classes.profileLabel}>Biggest Win</div>
                    </div>
                  </div>
                </div>
              </div>
              <div className={classes.containerTabs}>
                <Tabs>
                  <TabList className={classes.mainHeader}>
                    <Tab>All</Tab>
                    <Tab>Settled</Tab>
                    <Tab>Unsettled</Tab>
                    <Space direction="vertical" size={12}>
                      <DatePicker
                        style={{ borderRadius: "10px" }}
                        defaultValue={moment("2015/01/01", dateFormat)}
                        format={dateFormat}
                      />
                    </Space>
                  </TabList>

                  <TabPanel>
                    <div className={classes.history}>
                      <div className={classes.historyInfo}>
                        <div className={classes.historyDes}>
                          No history to display for this filter
                        </div>
                      </div>
                    </div>
                  </TabPanel>
                  <TabPanel>
                    <h2>Any content 2</h2>
                  </TabPanel>
                </Tabs>
              </div>
            </div> */}
            <MyBets />
          </Card>
          <BetMobile />
        </main>
      </div>
    </>
  );
};

export default BetHistory;
