import classes from "./styles.module.css";
import Slider from "react-slick";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { fetchBetAllHistory } from "../../store/actions/betHistory-action";
import HistoryItem from "../HistoryItems";
import { useTranslation } from "react-i18next";

function SampleNextArrow(props) {
  const { className, style } = props;
  return <div className={className} style={{ ...style, display: "none" }} />;
}

function SamplePrevArrow(props) {
  const { className, style } = props;
  return <div className={className} style={{ ...style, display: "none" }} />;
}
const RecentTradeHistory = () => {
  const { t } = useTranslation();

  const dispatch = useDispatch();
  const betAllHistory = useSelector(
    (state) => state.betHistory.betAllHistoryData
  );

  useEffect(() => {
    dispatch(fetchBetAllHistory(5));
  }, [dispatch]);

  var settings = {
    dots: true,
    dotWidth: "30px",
    infinite: true,
    speed: 500,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    dotsClass: `slick-dots ${classes.dots}`,

    appendDots: (dots) => (
      <div
        style={{
          borderRadius: "10px",
          padding: "10px",
        }}
      >
        <ul style={{ margin: "0px" }}>{dots} </ul>
      </div>
    ),

    // dotsClass: `slick-dots ${classes.dots}`,
  };
  return (
    <>
      <div className={classes.headerTraded}>
        <span className={classes.recentlyTraded}>
          {t("description.recentlyTrade")}
        </span>
        {/* <span className={classes.tradeIcon} /> */}
      </div>

      <div className={`${classes.tradeHistory} ${classes.tradeGap}`}>
        <div className={classes.collapseOne}>
          <div class={classes.slick_slider}>
            <Slider className={classes.sliderMain} {...settings}>
              {betAllHistory.map((betAllHistoryItem) => (
                <HistoryItem
                  key={betAllHistoryItem._id}
                  betHistory={betAllHistoryItem}
                  isRecentTrade={true}
                />
              ))}
            </Slider>
          </div>
        </div>
      </div>
    </>
  );
};
export default RecentTradeHistory;
