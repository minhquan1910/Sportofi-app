import React, { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchOddData } from "../../store/actions/odd-action";
import OddItem from "../OddItem/OddItem";

const options = {
  method: "GET",
  headers: {
    "X-RapidAPI-Key": "177c193834msh3f7504588eae5c3p1ac8d5jsn6e8450dc2cb0",
    "X-RapidAPI-Host": "api-football-v1.p.rapidapi.com",
  },
};

const TournamentMatchTypeItem = ({ oddMatchId }) => {
  const [odd, setOdd] = useState({});

  useEffect(() => {
    fetch(
      `https://api-football-v1.p.rapidapi.com/v3/odds?fixture=${oddMatchId}&bookmaker=1`,
      options
    )
      .then((response) => response.json())
      .then((response) =>
        response?.response?.length > 0
          ? setOdd(response?.response[0])
          : setOdd(undefined)
      )
      .catch((err) => console.error(err));
  }, [oddMatchId]);

  return <OddItem oddItemId={oddMatchId} odd={odd} />;
};

export default TournamentMatchTypeItem;
