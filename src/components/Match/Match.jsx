import React, { useEffect, useState } from "react";
import classes from "./Match.module.css";
import MiddleMatch from "./MiddleMatch";
import TimeLine from "../BlockRight/TimeLine";
import SportPiece from "../Sports/SportPiece";
import Country from "../Country/Country";
import Card from "../UI/Card";
import TableMatching from "../TableMatching/TableMatching";
import {
  fetchLiveMatchData,
  fetchLiveMatchDetailData,
  fetchMatchData,
} from "../../store/actions/match-action";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchOddMatchBookmakerData,
  fetchOddMatchInPlayData,
} from "../../store/actions/odd-action";
import LoadingSpinner from "../UI/LoadingSpinner";
import MatchContent from "./MatchContent";

import { isEmpty } from "lodash";

const Match = () => {
  const params = useParams();
  const matchId = params.matchId;
  const dispatch = useDispatch();
  const [showMatchLive, setShowMatchLive] = useState(false);
  const [showMatch, setShowMatch] = useState({});
  const [showOddMatchBookmaker, setShowOddMatchBookmaker] = useState({});
  const [showOddMatchInPlay, setShowOddMatchInPlay] = useState([]);

  const match = useSelector((state) => state.match.matchItem);
  const oddBookMakers = useSelector((state) => state.odd.oddMatchBookmaker);
  const oddMatchInPlayData = useSelector((state) => state.odd.oddMatchInPlay);

  useEffect(() => {
    if (!isEmpty(match)) {
      setShowMatch(match);
    }
    if (!isEmpty(oddBookMakers)) {
      setShowOddMatchBookmaker(oddBookMakers);
    }
    if (!isEmpty(oddMatchInPlayData)) {
      setShowOddMatchInPlay(oddMatchInPlayData);
    }
    if (
      showMatch?.fixture?.status?.short === "1H" ||
      showMatch?.fixture?.status?.short === "HT" ||
      showMatch?.fixture?.status?.short === "2H" ||
      showMatch?.fixture?.status?.short === "ET" ||
      showMatch?.fixture?.status?.short === "P"
    ) {
      setShowMatchLive(true);
    }
  }, [
    match,
    oddBookMakers,
    showMatch?.fixture?.status?.short,
    oddMatchInPlayData,
  ]);

  useEffect(() => {
    dispatch(fetchMatchData(matchId));
    dispatch(fetchOddMatchBookmakerData(matchId));
    dispatch(fetchOddMatchInPlayData(matchId));
  }, [dispatch, matchId]);

  return (
    <div className={classes.sporting}>
      <main className={classes.mainMain}>
        <SportPiece />
        <Country />
        <Card>
          <section className={classes.sportInfoMain}>
            {showMatchLive ? (
              <div className={classes.banner}>
                <div className={classes.main}>
                  <img
                    className={classes.banerMain}
                    src={"https://beta.betswap.gg/images/sport-banner.jpg"}
                    alt="banner"
                  />
                  <div className={classes.leagueBannerMain}>
                    <img
                      className={classes.iconbaner}
                      src={showMatch?.league?.logo}
                      alt="League Banner"
                    />
                  </div>
                  <div className={classes.matchIcon}>
                    <ul>
                      <li>
                        <img src={showMatch?.teams?.home?.logo} alt="" />
                        <span>{showMatch?.teams?.home?.name}</span>
                      </li>
                      <li>
                        <p>VS</p>
                      </li>
                      <li>
                        <img src={showMatch?.teams?.away?.logo} alt="" />
                        <span>{showMatch?.teams?.away?.name}</span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            ) : (
              <TableMatching
                key={matchId}
                imageLeague={showMatch?.league?.logo}
                nameLeague={showMatch?.league?.name}
                date={showMatch?.fixture?.date}
                status={showMatch?.fixture?.status?.long}
                imageHome={showMatch?.teams?.home?.logo}
                imageAway={showMatch?.teams?.away?.logo}
                home={showMatch?.teams?.home?.name}
                away={showMatch?.teams?.away?.name}
              />
            )}
          </section>
          <div className={classes["main-info"]}>
            <MiddleMatch
              match={showMatch}
              oddMatchBookmakers={showMatchLive ? null : showOddMatchBookmaker}
            />
          </div>

          <MatchContent
            fixtureMatch={showMatch}
            fixtureId={showMatch?.fixture?.id}
          />
        </Card>
        <TimeLine />
      </main>
    </div>
  );
};

export default React.memo(Match);
