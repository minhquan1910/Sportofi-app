import React, { useEffect, useState } from "react";
import classes from "./MatchItem.module.css";

import classNames from "classnames";
import { useDispatch, useSelector } from "react-redux";

import { isEmpty } from "lodash";
import { cartActions } from "../../store/slices/cart-slice";
import constants from "../../constants";
import { uiActions } from "../../store/slices/ui-slice";
import LockIcon from "@material-ui/icons/Lock";
import { fetchFixtureDetails } from "../../store/actions/fixture-action";
import { useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";

const MatchItem = ({ oddItemId, odd, betType }) => {
  const { t } = useTranslation();
  const [btnState, setBtnState] = useState(false);
  const [disabledButton, setDisabledButton] = useState(true);
  const dispatch = useDispatch();
  const params = useParams();
  const matchId = params.matchId;

  const fixtureDetail = useSelector((state) => state.fixture.fixtureDetail);

  const [home, setHome] = useState("");
  const [draw, setDraw] = useState("");
  const [away, setAway] = useState("");
  const handleClick = () => {
    setBtnState((btnState) => !btnState);
    dispatch(uiActions.toggleOddsCart());
  };
  const myClassNames = classNames("button", {
    active: btnState,
  });
  useEffect(() => {
    dispatch(fetchFixtureDetails(matchId));
    const value = odd?.bookmakers?.[0]?.bets?.[0]?.values;
    if (!isEmpty(value)) {
      setDisabledButton(false);
      setHome(value?.[0]?.odd || <LockIcon />);
      setDraw(value?.[1]?.odd || <LockIcon />);
      setAway(value?.[2]?.odd || <LockIcon />);
    } else {
      setDisabledButton(true);
      setHome(<LockIcon />);
      setDraw(<LockIcon />);
      setAway(<LockIcon />);
    }
  }, [odd, oddItemId, dispatch, matchId]);

  const addOddHandler = (index) => {
    // dispatch(uiActions.toggleOddsCart());
    const bookmarker = odd?.bookmakers?.[0] ?? [];
    const bets = bookmarker?.bets ?? [];
    const bet =
      bets.find((bet) => bet?.id === constants.DEFAULTS.BET_TYPE.code) ?? {};
    const betType = !isEmpty(bet)
      ? {
          id: bet?.id,
          name: bet?.name,
        }
      : {
          id: constants.DEFAULTS.BET_TYPE.code,
          name: constants.DEFAULTS.BET_TYPE.name,
      } ;
    const value = bet?.values?.[index] ?? [];
    let oddValue = "";
    console.log("values", value[index]);
    if (!isEmpty(value)) {
      oddValue = value?.odd;
    } else {
      oddValue = "";
    }
    const side = {
      value: value?.value,
      id: index,
    };

    dispatch(cartActions.addOddHandler({ oddItemId, side, oddValue, betType }));
  };

  return (
    <React.Fragment>
      <tr className={classes.ccordiontoggleodd}>
        <td className={classes.leagueimgtdwidth}>
          <div className={classes.matchName}>
            <div className={classes.matchiconname}>
              <p>
                <span className={classes.mobellipsistext}>
                  {fixtureDetail[0]?.teams?.home?.name}
                </span>
              </p>
            </div>
          </div>
        </td>
        <td>
          <div className={classes.blueboxMain}>
            <div
              className={`${classes.bluebox} ${classes.leftbox}`}
              onClick={() => {
                addOddHandler(0);
                handleClick();
              }}
            >
              <p>
                <span>{home}</span>
              </p>
            </div>
          </div>
        </td>
        <td></td>
      </tr>
      <tr className={classes.ccordiontoggleodd}>
        <td className={classes.leagueimgtdwidth}>
          <div className={classes.matchName}>
            <div className={classes.matchiconname}>
              <p>
                <span className={classes.mobellipsistext}>
                  {t("description.draw")}
                </span>
              </p>
            </div>
          </div>
        </td>
        <td>
          <div className={classes.blueboxMain}>
            <div
              className={`${classes.bluebox} ${classes.leftbox}`}
              onClick={() => {
                addOddHandler(1);
                handleClick();
              }}
            >
              <p>
                <span>{draw}</span>
              </p>
            </div>
          </div>
        </td>
        <td></td>
      </tr>
      <tr className={classes.ccordiontoggleodd}>
        <td className={classes.leagueimgtdwidth}>
          <div className={classes.matchName}>
            <div className={classes.matchiconname}>
              <p>
                <span className={classes.mobellipsistext}>
                  {fixtureDetail[0]?.teams?.away?.name}
                </span>
              </p>
            </div>
          </div>
        </td>
        <td>
          <div className={classes.blueboxMain}>
            <div
              className={`${classes.bluebox} ${classes.leftbox}`}
              onClick={() => {
                addOddHandler(2);
                handleClick();
              }}
            >
              <p>
                <span>{away}</span>
              </p>
            </div>
          </div>
        </td>
        <td></td>
      </tr>
    </React.Fragment>
  );
};

export default React.memo(MatchItem);