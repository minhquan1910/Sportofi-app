import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import classes from "./MatchContent.module.css";
import { fetchOddMatchBookmakerData } from "../../store/actions/odd-action";
import OddMarker from "../OddMarket/OddMarker";

import { isEmpty } from "lodash";

const MatchContent = ({ fixtureMatch, fixtureId }) => {
  const dispatch = useDispatch();
  const [bets, setBets] = useState([]);
  const oddMatches = useSelector((state) => state.odd.oddMatchBookmaker);
  useEffect(() => {
    dispatch(fetchOddMatchBookmakerData(fixtureId));
  }, [dispatch, fixtureId]);
  useEffect(() => {
    if (!isEmpty(oddMatches)) {
      const bookmarker = oddMatches?.bookmakers?.[0] ?? [];
      const betsData = bookmarker?.bets ?? [];
      setBets(betsData);
    }
  }, [oddMatches]);

  return (
    <>
      {!isEmpty(bets) ? (
        <div className={classes.tabContent}>
          <div
            className={`${classes.tabPane} ${classes.fade} ${classes.active}`}
          >
            <div className={classes.marketOdd}>
              {bets.map((bet, index) => (
                <OddMarker key={index} betType={bet} />
              ))}
            </div>
          </div>
        </div>
      ) : (
        <p className="text-center">No Data</p>
      )}
    </>
  );
};

export default MatchContent;