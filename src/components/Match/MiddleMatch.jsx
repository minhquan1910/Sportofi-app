import React from "react";
import classes from "./MiddleMatch.module.css";
import MatchType from "./MatchType";
import { useTranslation } from "react-i18next";

const MiddleMatch = ({ match, oddMatchBookmakers }) => {
  const { t } = useTranslation();
  return (
    <React.Fragment>
      <MatchType oddsBookmakerMatch={oddMatchBookmakers} />
      <div className={`${classes.othermarketsection} ${"py-1"}`}>
        <p>{t("description.otherMarket")}</p>
      </div>
      <div className={classes.navTitle}>
        <div className={classes.navTitleSub}>
          <ul className={classes.nav}>
            <li className={classes.navItem}>
              <a className={`${classes.navLink} ${classes.active}`} href="/">
                {t("description.markets")}
              </a>
            </li>
          </ul>
        </div>
      </div>
    </React.Fragment>
  );
};

export default MiddleMatch;
