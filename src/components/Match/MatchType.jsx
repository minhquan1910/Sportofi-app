import React from "react";
import classes from "./MatchType.module.css";
import MatchItem from "./MatchItem";
import { useTranslation } from "react-i18next";

const MatchType = ({ oddsBookmakerMatch }) => {
  const { t } = useTranslation();
  return (
    <div className={classes["card-content"]}>
      <div>
        <table className={classes.table}>
          <thead>
            <tr className={classes.trMain}>
              <th>{t("description.matchWinner")}</th>
              <th>
                <div className={classes.backlaydata}>
                  <div className={classes.leftdata}>
                    <p>
                      <span> </span>
                    </p>
                  </div>
                </div>
              </th>
              {/* <th>
                <button className={classes.refreshbtn}>
                  <span>Refresh</span>
                </button>
              </th> */}
            </tr>
          </thead>

          <tbody className={classes.tb}>
            <MatchItem
              oddItemId={oddsBookmakerMatch?.fixture?.id}
              odd={oddsBookmakerMatch}
              betType={oddsBookmakerMatch?.bookmakers?.[0]?.bets?.[0]}
            />
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default MatchType;
