import React, { useState } from "react";
import classes from "./SubMenu.module.css";
import home from "../../assets/Flags/active-home.svg";
import nft from "../../assets/Flags/nft.svg";
import star from "../../assets/Flags/star.svg";
import diamand from "../../assets/Flags/diamand.svg";
import trophy from "../../assets/Flags/trophy.svg";
import { useDispatch, useSelector } from "react-redux";
import englandImage from "../../assets/Flags/united-kingdom.png";
import itImage from "../../assets/Flags/italy.png";
import zhImage from "../../assets/Flags/china.png";
import koreaImage from "../../assets/images/south-korea.png";
import japanImage from "../../assets/images/japan.png";
import { useTranslation } from "react-i18next";
// import classes from "./Subheader.module.css";
import downIconBold from "../../assets/Flags/down-arrow-bold.svg";
import { uiActions } from "../../store/slices/ui-slice";
import { useAccount, useConnect, useDisconnect } from "wagmi";
import FaucetMain from "../Faucet/FaucetMain";
import { NavLink } from "react-router-dom";

const SubMenu = () => {
  const dispatch = useDispatch();
  const { t, i18n } = useTranslation();
  const { connector, isConnected } = useAccount();
  const { disconnect } = useDisconnect();
  const showLanguageModalHandler = () => {
    dispatch(uiActions.toggleLanguageModal());
  };

  const showOddsModalHandler = () => {
    dispatch(uiActions.toggleOddsModal());
  };

  const showMultipleLanguage = useSelector(
    (state) => state.ui.multipleLanguageIsVisible
  );

  const showOdds = useSelector((state) => state.ui.oddsIsVisible);

  const langs = [
    {
      code: "en",
      img: englandImage,
      alt: "english",
      name: "English",
    },
    {
      code: "cn",
      img: zhImage,
      alt: "china",
      name: "China",
    },
    {
      code: "jp",
      img: japanImage,
      alt: "japan",
      name: "Japan",
    },
    {
      code: "kr",
      img: koreaImage,
      alt: "korea",
      name: "Korea",
    },
  ];

  const [currentLang, setCurrentLang] = useState(langs[0]);

  const changeLanguage = (language) => {
    i18n.changeLanguage(language);
    const lang = langs.find((lang) => lang.code === language);
    setCurrentLang(lang);
  };

  const multiLanguageMenu = (
    <div className={classes["dropdown-menu"]}>
      {langs.map((lang) => (
        <button onClick={() => changeLanguage(lang.code)}>
          <img src={lang.img} alt={lang.alt} />
          <span>{lang.name}</span>
        </button>
      ))}
      {/* <button onClick={clickHandler}>
        <img src={itImage} alt="Italy" />
        <span>It</span>
      </button> */}
      {/* <button onClick={() => changeLanguage("cn")}>
        <img src={zhImage} alt="China" />
        <span>ZH</span>
      </button> */}
      {/* <button>
        <img src={koreaImage} alt="Korean" />
        <span>ko</span>
      </button> */}
      {/* <button onClick={() => changeLanguage("jp")}>
        <img src={japanImage} alt="Japan" />
        <span>ja</span>
      </button>
      <button>
        <span>More...</span>
      </button> */}
    </div>
  );

  const oddsMenu = (
    <div className={classes["dropdown-menu-link"]}>
      <a href="/">{t("description.decimalOdds")}</a>
      <a href="/">American Odds</a>
    </div>
  );
  return (
    <div>
      <div className={classes.subMenu}>
        <div className={classes.nav}>
          <ul>
            <li className={classes.mainTitle}>
              <img src={home} alt="" />
              <span>{t("description.sportExchange")}</span>
            </li>
            <li className={classes.secondTitle}>
              <img src={nft} alt="" />
              <span>{t("description.nft-esports")}</span>
              <button className={classes.btnComing}>
                {t("description.comingSoon")}
              </button>
            </li>
            {/* <li className={classes.secondTitle}>
              <img src={star} alt="" />
              <span className={classes.fantasy}>Fantasy Sports</span>
              <button className={classes.btnComing}>Coming Soon</button>
            </li> */}
            <li className={classes.secondTitle}>
              <img src={diamand} alt="" />
              <span>{t("description.p2p-games")}</span>
              <button className={classes.btnComing}>
                {t("description.comingSoon")}
              </button>
            </li>
            <li className={classes.secondTitle}>
              <img src={trophy} alt="" />
              <span>{t("description.tournaments")}</span>
              <button className={classes.btnComing}>
                {t("description.comingSoon")}
              </button>
            </li>
            <li>
              <NavLink to="/faucet" className={classes.faucet}>
                Faucet
              </NavLink>
            </li>
          </ul>
        </div>
        <hr className="bg-white m-2" />
        <div className={classes.secondMenu}>
          <a href="/">{t("description.Terms&Conditions")}</a>
          <a href="/">{t("description.ResponsibleGambling")}</a>
          <a href="/">{t("description.SportsBettingRules")}</a>
        </div>
        <hr className="bg-white m-2" />
        <div className={classes["header-main"]}>
          <div className={classes["main-left"]}>
            <nav>
              <div>
                <div className={classes.dropdown}>
                  <button onClick={showLanguageModalHandler}>
                    <img
                      className={classes.langue}
                      src={currentLang.img}
                      alt=""
                    />
                    <span className={classes.langueName}>
                      {currentLang.name}
                    </span>
                    <img
                      className={classes.logoCountry}
                      src={downIconBold}
                      alt=""
                    />
                  </button>
                  {showMultipleLanguage ? multiLanguageMenu : null}
                  {/* <div
                  className={classes.backdrop}
                  onClick={showLanguageModalHandler}
                >
                  <div className={classes["dropdown-menu"]}>
                    <button>
                      <img src={englandImage} alt="" />
                      <span>EN</span>
                    </button>
                    <button>
                      <img src={englandImage} alt="" />
                      <span>EN</span>
                    </button>
                    <button>
                      <img src={englandImage} alt="" />
                      <span>EN</span>
                    </button>
                    <button>
                      <img src={englandImage} alt="" />
                      <span>EN</span>
                    </button>
                    <button>
                      <img src={englandImage} alt="" />
                      <span>EN</span>
                    </button>
                  </div>
                </div> */}
                </div>

                <div className={classes.dropdown}>
                  <button onClick={showOddsModalHandler}>
                    <span className={classes.text}>Decimal Odds</span>
                    {/* <img
                      className={classes.logoOdd}
                      src={downIconBold}
                      alt=""
                    /> */}
                  </button>

                  {/* {showOdds ? oddsMenu : null} */}
                </div>

                {/* <a href="/">Live help</a> */}
              </div>
            </nav>
          </div>
          {/* <div className={classes.mainRight}></div> */}
        </div>
        {isConnected && (
          <div className={classes["header-main"]}>
            <button className={classes["user-logout"]} onClick={disconnect}>
              <img
                className={classes.logoutIcon}
                src="https://beta.betswap.gg/images/icons/logout.svg"
                alt="logout"
              />
              <span>{t("description.logout")}</span>
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default SubMenu;
