import React from "react";
import classes from "./TournamentInfo.module.css";
import TournamentMatch from "../TournamentMatch/TournamentMatch";
import { useTranslation } from "react-i18next";

const TournamentInfo = ({ matchsTournament }) => {
  const { t } = useTranslation();
  return (
    <div className={classes["card-info"]}>
      <div className={classes["card-header"]}>
        <div className={classes["match-headername"]}>
          <p>
            <img src={matchsTournament[0]?.league?.logo} alt="" />
          </p>
          <p>{matchsTournament[0]?.league?.name}</p>
        </div>
        <div className={classes["match-type"]}>
          <ul>
            <li>1</li>
            <li>X</li>
            <li>2</li>
          </ul>
        </div>
      </div>
      <div className={classes["card-content"]}>
        <TournamentMatch matchs={matchsTournament} />
      </div>
    </div>
  );
};

export default TournamentInfo;
