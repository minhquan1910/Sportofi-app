import React from "react";
import classes from "./BetMobile.module.css";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import deleteBin from "../../assets/Flags/delete.svg";
import shapeThree from "../../assets/images/shape-3.png";
import { useDispatch, useSelector } from "react-redux";
import { cartActions } from "../../store/slices/cart-slice";
import { useState } from "react";
import { useEffect } from "react";
import constants from "../../constants";
import { failureModal, successModal } from "../../helpers/modals";
import { Button, Card } from "antd";
import { useNetwork, useAccount, useSwitchNetwork, useSigner } from "wagmi";
import { Tab, TabList, TabPanel, Tabs } from "react-tabs";
import { placeBetHandler } from "../Handlers";
import { fetchMatchData } from "../../store/actions/match-action";
import { fetchBetHistory } from "../../store/actions/betHistory-action";
import HistoryItem from "../HistoryItems";
import { fetchOpenBetHistory } from "../../store/actions/betHistory-action";
const BetMobile = (props) => {
  const [loadingPlaceBet, setLoadingPlaceBet] = useState(false);
  const { switchNetwork } = useSwitchNetwork();
  const { chain } = useNetwork();
  const { isConnected, address } = useAccount();
  const [qualityInput, setQualityInput] = useState("1");
  const [disabledButton, setDisabledButton] = useState(true);
  const dataOdd = useSelector((state) => state.cart.cartItem);
  const totalOdd = useSelector((state) => state.cart.totalOdd);
  const liability = useSelector((state) => state.cart.liability);
  const profit = useSelector((state) => state.cart.profit);
  const quality = useSelector((state) => state.cart.qualityStake);
  const matchDetail = useSelector((state) => state.match.matchItem);
  const dispatch = useDispatch();
  const betHistoryWallet = useSelector(
    (state) => state.betHistory.betHistoryItem
  );
  const openBetHistory = useSelector(
    (state) => state.betHistory.openBetHistoryData
  );

  useEffect(() => {
    dispatch(fetchOpenBetHistory(address));
  }, [dispatch]);
  // console.log("BetHistory", betHistoryWallet);
  const { data: wagmiSigner } = useSigner({
    chainId: constants.CHAIN.bscChain.id,
  });

  const changeOddHandler = (e) => {
    console.log(e.target.value);
  };

  const changeQualityHandler = (e) => {
    dispatch(cartActions.addQualityStake(e.target.value));
    setQualityInput(e.target.value);
  };

  useEffect(() => {
    dispatch(fetchBetHistory(address));
  }, [dispatch]);

  useEffect(() => {
    console.log(dataOdd[0]);
    if (dataOdd[0]?.odd && dataOdd[0]?.odd !== 0) {
      setDisabledButton(false);
    } else {
      setDisabledButton(true);
    }
    dispatch(fetchMatchData(dataOdd[0]?.id));
  }, [dataOdd[0]?.odd, dispatch]);

  const submitHandler = async (event) => {
    event.preventDefault();
    // 1. check connected wallet
    if (!isConnected) {
      failureModal("Error", "Please connect wallet first");
      return;
    }
    console.log("Submit");
    console.log("chain", chain);
    console.log("targetChain", constants.TARGET_NETWORK.chainId);
    // 2. check target network
    if (chain.id !== constants.TARGET_NETWORK.chainId) {
      switchNetwork(constants.TARGET_NETWORK.chainId);
      return;
    }
    const signer = await wagmiSigner;
    const gameId = constants.game.SCORE.code;
    const matchId = dataOdd[0].id;
    const odd = dataOdd[0].odd;
    const settleStatus = constants.settleStatus.FULL_MATCH; // default only available for full match
    const side = dataOdd[0].side?.code;
    const amount = qualityInput;
    setLoadingPlaceBet(true);
    await placeBetHandler(
      gameId,
      matchId,
      odd,
      settleStatus,
      side,
      amount,
      address,
      signer
    );
    setLoadingPlaceBet(false);
  };

  const [tabIndex, setTabIndex] = useState(0);

  let nameOdd = "";

  if (dataOdd?.[0]?.side?.code === 1) {
    nameOdd = matchDetail?.teams?.home?.name;
  } else if (dataOdd?.[0]?.side?.code === 2) {
    nameOdd = "Draw";
  } else if (dataOdd?.[0]?.side?.code === 3) {
    nameOdd = matchDetail?.teams?.away?.name;
  }
  const [offset, setOffset] = useState(0);

  useEffect(() => {
    const onScroll = () => setOffset(window.pageYOffset);
    // clean up code
    window.removeEventListener("scroll", onScroll);
    window.addEventListener("scroll", onScroll, { passive: true });
    return () => window.removeEventListener("scroll", onScroll);
  }, []);

  return (
    <Tabs
      className={offset === 0 ? classes["marginHeader"] : null}
      defaultIndex={0}
    >
      <div className={classes.navigation}>
        <TabList className={classes.navMain}>
          <Tab className={`${classes.nav}`}>
            <div>
              <button
                className={`${tabIndex === 0 ? classes.active : null}`}
                onClick={() => {
                  setTabIndex(0);
                }}
              >
                Bet Slip
              </button>
            </div>
          </Tab>
          <Tab className={classes.nav}>
            <div>
              <button
                className={`${tabIndex === 1 ? classes.active : null}`}
                onClick={() => {
                  setTabIndex(1);
                }}
              >
                Open Bets
              </button>
            </div>
          </Tab>
          <Tab className={classes.nav}>
            <div>
              <button
                className={`${tabIndex === 2 ? classes.active : null}`}
                onClick={() => {
                  setTabIndex(2);
                }}
              >
                Bet History
              </button>
            </div>
          </Tab>
        </TabList>
        <TabPanel>
          <div className={classes["tab-content"]}>
            <div className={classes["heading-tab"]}>
              <div>
                <p>Selection</p>
                <span>
                  <img src={deleteBin} alt="Delete Bin" />
                </span>
              </div>
            </div>
            <form className={classes["body-tab"]} onSubmit={submitHandler}>
              <div className={classes["back-div"]}>
                <ul>
                  <li>
                    <p>Bet For</p>
                  </li>
                  <li>
                    <p>Odds</p>
                    <p>Stake</p>
                  </li>
                </ul>
              </div>
              <div className={classes["back-data"]}>
                <ul>
                  <li>
                    <div>
                      <p style={{ fontWeight: "bold", fontSize: "13px" }}>
                        {/* {dataOdd[0].side.code === 1 ? ( matchDetail?.teams?.home?.name ?? "" ) :  } */}

                        {nameOdd}
                      </p>
                      <p>Market 1x2</p>
                    </div>
                  </li>
                  <li>
                    <div>
                      <input
                        type="number"
                        maxLength={10}
                        data-index={0}
                        step={0.01}
                        value={dataOdd[0]?.odd}
                        onChange={changeOddHandler}
                      />
                    </div>
                    <div>
                      <input
                        type="number"
                        min={0}
                        maxLength={10}
                        data-index={0}
                        step={1}
                        value={quality}
                        onChange={changeQualityHandler}
                      />
                    </div>
                  </li>
                </ul>
              </div>
              <div className={classes["payout-amt"]}>
                <ul>
                  <li></li>
                  <li>
                    <p>Payout</p>
                    <p>${totalOdd.toFixed(2)}</p>
                  </li>
                </ul>
              </div>
              <div className={classes["profit"]}>
                <p>
                  <span>Liability:</span>
                  <span>${liability}</span>
                </p>
                <p>
                  <span>Profit :</span>
                  <span>${profit.toFixed(2)}</span>
                </p>
              </div>
              <div className={classes["shape-img"]}>
                <img src={shapeThree} alt="Shape 3" />
              </div>
              <div className={classes["place-betsBtn"]}>
                {console.log()}
                <Button
                  loading={loadingPlaceBet}
                  type="primary"
                  style={{
                    backgroundColor: "#27aae1",
                  }}
                  onClick={submitHandler}
                  disabled={disabledButton ? true : false}
                >
                  Place Bet
                </Button>
              </div>
            </form>
          </div>{" "}
        </TabPanel>
        <TabPanel>
          {!isConnected ? (
            <div className={classes.alert}>
              <span className={classes.desc}>
                Connect Wallet to view your Open Bets
              </span>
            </div>
          ) : (
            openBetHistory.map((openBetHistory, index) => {
              return <HistoryItem key={index} betHistory={openBetHistory} />;
            })
          )}
        </TabPanel>
        <TabPanel>
          {!isConnected ? (
            <div className={classes.alert}>
              <span className={classes.desc}>
                Connect Wallet to view your History Bets
              </span>
            </div>
          ) : (
            betHistoryWallet.map((betHistory, index) => {
              return <HistoryItem key={index} betHistory={betHistory} />;
            })
          )}
        </TabPanel>
      </div>
    </Tabs>
  );
};

export default BetMobile;
