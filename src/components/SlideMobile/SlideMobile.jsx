import React, { useEffect, useState } from "react";
import classes from "./SlideMobile.module.css";
import trash from "../../assets/Flags/delete.svg";
import { useDispatch, useSelector } from "react-redux";
import { cartActions } from "../../store/slices/cart-slice";
import { useAccount, useSwitchNetwork, useNetwork, useSigner, useConnect } from "wagmi";
import { MetaMaskConnector } from "wagmi/connectors/metaMask";
import constants from "../../constants";
import { placeBetHandler } from "../Handlers";
import { failureModal } from "../../helpers/modals";
import { fetchMatchData } from "../../store/actions/match-action";
import { isEmpty } from "lodash";
import { lightGreen } from "@material-ui/core/colors";
import { Button } from "antd";

const SlideMobile = () => {
  const [loadingPlaceBet, setLoadingPlaceBet] = useState(false);
  const { data: wagmiSigner } = useSigner({
    chainId: constants.CHAIN.bscChain.id,
  });
  const [qualityInput, setQualityInput] = useState("1");

  const { switchNetwork } = useSwitchNetwork();
  const { chain } = useNetwork();
  const { isConnected, address } = useAccount();
  const [minimizeCart, setMinimizeCart] = useState(false);
  const [nameOdd, setNameOdd] = useState("");
  const [matchDetail, setMatchDetail] = useState(null);
  const {connect} = useConnect()

  const dataOdd = useSelector((state) => state.cart.cartItem);
  const liability = useSelector((state) => state.cart.liability);
  const profit = useSelector((state) => state.cart.profit);
  const showCart = useSelector((state) => state.ui.cartIsVisible);
  const quality = useSelector((state) => state.cart.qualityStake);
  const matchDetails = useSelector((state) => state.match.matchItem);
  const chains = "97"

  const dispatch = useDispatch();
  const minimizeCartHandler = () => {
    setMinimizeCart((prevMinimizeCart) => !prevMinimizeCart);
  };

  useEffect(() => {
    if (!isEmpty(matchDetails)) setMatchDetail(matchDetails);
  }, [matchDetails]);

  const changeQualityHandler = (e) => {
    dispatch(cartActions.addQualityStake(e.target.value));
    setQualityInput(e.target.value);
  };
  const handleOnClickPlaceBet = async () => {
    if (!isConnected) {
      failureModal("Error", "Please connect wallet first");
      return;
    }
    console.log("Submit");
    console.log("chain", chain);
    console.log("targetChain", constants.TARGET_NETWORK.chainId);
    // 2. check target network
    if (chain.id !== constants.TARGET_NETWORK.chainId) {
      switchNetwork(constants.TARGET_NETWORK.chainId);
      return;
    }
    const signer = await wagmiSigner;
    const gameId = constants.game.SCORE.code;
    const matchId = dataOdd[0].id;
    const odd = dataOdd[0].odd;
    const settleStatus = constants.settleStatus.FULL_MATCH; // default only available for full match
    const side = dataOdd[0].side?.id + 1;
    const amount = qualityInput;
    setLoadingPlaceBet(true);
    await placeBetHandler(
      gameId,
      matchId,
      odd,
      settleStatus,
      side,
      amount,
      address,
      signer
    );
    setLoadingPlaceBet(false);
  };

  useEffect(() => {
    dispatch(fetchMatchData(dataOdd[0]?.id));
  }, [dataOdd[0]?.odd, dispatch]);

  useEffect(() => {
    if (dataOdd?.[0]?.side?.code === 1) {
      setNameOdd(matchDetail?.teams?.home?.name);
    } else if (dataOdd?.[0]?.side?.code === 2) {
      setNameOdd("Draw");
    } else if (dataOdd?.[0]?.side?.code === 3) {
      setNameOdd(matchDetail?.teams?.away?.name);
    } else {
      setNameOdd("");
    }
  }, [dataOdd[0]?.odd]);

  return (
    <React.Fragment>
      {showCart && (
        <div
          className={classes.modal}
          style={minimizeCart ? { transform: "translateY(120px)" } : {}}
        >
          <div className={classes.subModal}>
            <div className={classes.header} onClick={minimizeCartHandler}>
              <p>
                <span>Bet Slip </span>
                {showCart ? (
                  <svg
                    class={`${classes.arrow} "svg-inline--fa fa-chevron-down fa-w-14"`}
                    aria-hidden="true"
                    focusable="false"
                    data-prefix="fas"
                    data-icon="chevron-down"
                    role="img"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 448 512"
                    data-fa-i2svg=""
                  >
                    <path
                      fill="currentColor"
                      d="M207.029 381.476L12.686 187.132c-9.373-9.373-9.373-24.569 0-33.941l22.667-22.667c9.357-9.357 24.522-9.375 33.901-.04L224 284.505l154.745-154.021c9.379-9.335 24.544-9.317 33.901.04l22.667 22.667c9.373 9.373 9.373 24.569 0 33.941L240.971 381.476c-9.373 9.372-24.569 9.372-33.942 0z"
                    ></path>
                  </svg>
                ) : (
                  <svg
                    class={`${classes.arrow} 'svg-inline--fa fa-chevron-up fa-w-14'`}
                    aria-hidden="true"
                    focusable="false"
                    data-prefix="fas"
                    data-icon="chevron-up"
                    role="img"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 448 512"
                    data-fa-i2svg=""
                  >
                    <path
                      fill="currentColor"
                      d="M240.971 130.524l194.343 194.343c9.373 9.373 9.373 24.569 0 33.941l-22.667 22.667c-9.357 9.357-24.522 9.375-33.901.04L224 227.495 69.255 381.516c-9.379 9.335-24.544 9.317-33.901-.04l-22.667-22.667c-9.373-9.373-9.373-24.569 0-33.941L207.03 130.525c9.372-9.373 24.568-9.373 33.941-.001z"
                    ></path>
                  </svg>
                )}
              </p>
            </div>
          </div>
          <div className={classes.backBet}>
            <ul style={{ background: "rgb(166, 216, 255)" }}>
              <li>
                <p>Bet For</p>
              </li>
              <li>Odds</li>
              <li>Stake</li>
            </ul>
          </div>
          <div className={classes.PAS}>
            <ul>
              <li>
                <p className={classes.teamTitle}>
                  {nameOdd ? nameOdd : dataOdd?.[0].side.value}
                </p>
                <p className={classes.teamName}>1x2</p>
                <p>
                  {matchDetail?.teams?.home?.name} vs{" "}
                  {matchDetail?.teams?.away?.name}
                </p>
              </li>
              <li>
                <p>
                  <input type="number" defaultValue={dataOdd[0]?.odd} name="" id="" />
                </p>
              </li>
              <li>
                <p className={classes.payout}>
                  <input
                    type="number"
                    min={0}
                    maxLength={10}
                    data-index={0}
                    step={1}
                    value={quality}
                    onChange={changeQualityHandler}
                  />
                  <span className={classes.trashCan}>
                    <img src={trash} alt="" />
                  </span>
                </p>
                <p className={classes.payout}>
                  <span className={classes.text}>Payout</span>
                </p>
              </li>
            </ul>
          </div>
          <div className={classes.btnSection}>
            <div className={classes.profit}>
              <div className={classes.left}>
                <span className={classes.liabilityText}>Liability :</span>
                <span className={classes.liabilityAmount}>${liability}</span>
              </div>
              <div style={{ marginTop: "4px" }} className={classes.left}>
                <span className={classes.liabilityText}>Profit :</span>
                <span
                  className={`${classes.liabilityAmount} ${classes.profitAmount}`}
                >
                  ${profit.toFixed(2)}
                </span>
              </div>
            </div>
            <div className={classes.placeBet}>
              <Button loading={loadingPlaceBet} onClick={handleOnClickPlaceBet}>Place Bet</Button>
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

export default SlideMobile;