import React, { useEffect, useState } from "react";
import classes from "./TableMatching.module.css";
import logoLeft from "../../assets/Flags/11.svg";
import logoMid from "../../assets/Flags/13.svg";
import logoRight from "../../assets/Flags/20.svg";
import Moment from "react-moment";
import { useCountdown } from "../../hooks/use-countdown";
import moment from "moment/moment";

const TableMatching = ({
  imageLeague,
  nameLeague,
  date,
  status,
  imageHome,
  imageAway,
  home,
  away,
}) => {
  const [days, hours, minutes, seconds] = useCountdown(
    moment(date).unix() * 1000
  );

  const tableMatch = (
    <div className={classes.main}>
      <div className={classes.title}>
        <img src={imageLeague} className={classes.logo}></img>
        <span>{nameLeague}</span>
        <div className={classes.time}>
          <Moment format="YYYY/MM/DD hh:mm">{date}</Moment>
        </div>
      </div>
      <div className={classes["status-bar"]}>
        <div className={classes["event-status"]}>{status}</div>
        <div className={classes["event-time"]}>Starts in</div>
      </div>

      <div className={classes.header}>
        <div className={classes.subHeader}>
          <div className={classes.headerLeft}>
            <img src={imageHome} alt="logo home" />
            <span>{home}</span>
          </div>
          {/* <div className={classes.headerMid}>
        <span className={classes.subHeaderMid}>Finished</span>
        <div className={classes.score}>
          <span className={classes.scoreNumber}>1 : 2</span>
          <div className={classes.subScore}>
            <span>1</span>
            <span>0 : 0</span>
            <span className={classes.dot}>⋮</span>
          </div>
        </div>
      </div> */}
          <div className={classes.headerRight}>
            <img src={imageAway} alt="logo away" />
            <span>{away}</span>
          </div>
        </div>
        <div className={classes.timer}>
          <div className={classes.countdown}>
            <div className={classes.countdownValue}>{days}</div>
            <div className={classes.countdownDes}>d</div>
          </div>
          <div className={classes.countdown}>
            <div className={classes.countdownValue}>{hours}</div>
            <div className={classes.countdownDes}>h</div>
          </div>
          <div className={classes.countdown}>
            <div className={classes.countdownValue}>{minutes}</div>
            <div className={classes.countdownDes}>m</div>
          </div>
          <div className={classes.countdown}>
            <div className={classes.countdownValue}>{seconds}</div>
            <div className={classes.countdownDes}>s</div>
          </div>
        </div>
      </div>
      <div className={classes.detail}>
        <div className={classes.detailLeft}>
          <span>-</span>
          <img src={logoLeft} alt="" />
          <span className={classes.border}>-</span>
        </div>
        <div className={classes.detailMid}>
          <span>-</span>
          <img src={logoMid} alt="" />
          <span className={classes.border}>-</span>
        </div>
        <div className={classes.detailRight}>
          <span>-</span>
          <img src={logoRight} alt="" />
          <span>-</span>
        </div>
      </div>
      <div className={classes.menuSlider}>
        <a href="" className={classes.menuItem}>
          <div className={classes.text}>live</div>
        </a>
        <a href="" className={classes.menuItem}>
          <div className={classes.text}>Lineups</div>
        </a>
        <a href="" className={classes.menuItem}>
          <div className={classes.text}>h2h</div>
        </a>
        <a href="" className={classes.menuItem}>
          <div className={classes.text}>incidents</div>
        </a>
        <a href="" className={classes.menuItem}>
          <div className={classes.text}>stats</div>
        </a>
        <a href="" className={classes.menuItem}>
          <div className={classes.text}>standings</div>
        </a>
      </div>
    </div>
  );

  return <React.Fragment>{tableMatch}</React.Fragment>;
};

export default React.memo(TableMatching);
