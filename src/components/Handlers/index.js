
import { ethers } from "ethers";
import { floor, toString } from "lodash";
import constants from "../../constants";
import { failureModal, successModal } from "../../helpers/modals";
import Web3 from "web3";
const placeBetHandler = async (
  
  _gameId,
  _matchId,
  _odd,
  _settleStatus,
  _side,
  _amount,
  _address,
  signer
) => {
  try {
    console.log("Input Data", {
      _gameId,
      _matchId,
      _odd,
      _settleStatus,
      _side,
      _amount,
      signer
    });

    const gameId = ethers.BigNumber.from(_gameId);
    const matchId = ethers.BigNumber.from(_matchId);
    const odd = ethers.BigNumber.from(floor(_odd * constants.PERCENTAGE_FRACTION));
    const settleStatus = ethers.BigNumber.from(_settleStatus);
    const side = ethers.BigNumber.from(_side);
    const amount = ethers.utils.parseEther(toString(_amount));
    const web3 = new Web3(`${constants.NODE_REAL_IO.HTTPS}${process.env.REACT_APP_NODE_REAL_KEY}`);
    console.log("web3", web3);
    // const provider = new ethers.providers.Web3Provider(web3.currentProvider);
    // const provider = new ethers.providers.Web3Provider(window.ethereum)
    // const signer = provider.getSigner();
    const paymentToken = constants.GTOKEN_ADDRESS.toLowerCase();
    console.log("Payment Token", paymentToken);
    const signerAddress = await signer.getAddress();
    const signerDomain = {
      name: "PaymentToken",
      version: "1",
      chainId: 97,
      verifyingContract: paymentToken,
    };

    const houseAddress = constants.BET_TO_WIN_ADDRESS.toLowerCase();
    const HouseDomain = {
      name: "Bet2Win",
      version: "1",
      chainId: "97",
      verifyingContract: houseAddress,
    };

    let house = new ethers.Contract(
      houseAddress,
      constants.BET_TO_WIN_ABI,
      signer
    );
    console.log("House Contract", house);
    let tokenContract = new ethers.Contract(
      paymentToken,
      constants.GTOKEN_ABI,
      signer
    );
    console.log("Token Contract", tokenContract);
    const types = {
      // EIP712Domain: [
      //   { name: "name", type: "string" },
      //   { name: "version", type: "string" },
      //   { name: "chainId", type: "uint256" },
      //   { name: "verifyingContract", type: "address" },
      // ],
      Permit: [
        { name: "user", type: "address" },
        { name: "betId", type: "uint256" },
        { name: "amount", type: "uint256" },
        { name: "paymentToken", type: "address" },
        { name: "deadline", type: "uint256" },
        { name: "nonce", type: "uint256" },
      ],
    };

    const signerTypes = {
      EIP712Domain: [
        { name: "name", type: "string" },
        { name: "version", type: "string" },
        { name: "chainId", type: "uint256" },
        { name: "verifyingContract", type: "address" },
      ],
      Permit: [
        { name: "owner", type: "address" },
        { name: "spender", type: "address" },
        { name: "value", type: "uint256" },
        { name: "nonce", type: "uint256" },
        { name: "deadline", type: "uint256" },
      ],
    };

    let betId = await house.betIdOf(gameId, matchId, odd, settleStatus, side);
    console.log("Bet Id", betId);
    // let signerNonce = await house.nonces(signerAddress)
    let houseNonce = await house.nonces(signerAddress);
    console.log("House Nonce", toString(houseNonce));
    let tokenNonce = await tokenContract.nonces(signerAddress);
    console.log("Token Nonce", toString(tokenNonce));
    houseNonce = houseNonce.toString();
    tokenNonce = tokenNonce.toString();
    const houseSigner = new ethers.Wallet(
      process.env.REACT_APP_PRIVATE_KEY
    );

    const deadline = "1765687599";
    const signerMessage = {
      owner: signerAddress.toString(),
      spender: houseAddress.toString(),
      value: amount.toString(),
      nonce: tokenNonce,
      deadline: deadline,
    };

    const houseMessage = {
      user: signerAddress,
      betId: betId.toString(),
      amount: amount.toString(),
      paymentToken: paymentToken,
      nonce: houseNonce,
      deadline: deadline,
    };
    console.log("House Message", houseMessage);

    const houseTypedData = {
      types,
      primaryType: "Permit",
      domain: HouseDomain,
      message: houseMessage,
    };
    console.log("House Typed Data", houseTypedData);

    const signerTypedData = {
      signerTypes,
      primaryType: "Permit",
      signerDomain,
      signerMessage,
    };
    console.log("House signer", houseSigner);
    console.log("Signer Typed Data", signerTypedData);
    //User signer
    const signature = await signer._signTypedData(
      signerDomain,
      { Permit: signerTypedData.signerTypes.Permit },
      signerMessage
    );
    //Croupier sign || House sign
    const houseSign = await houseSigner._signTypedData(
      houseTypedData.domain,
      houseTypedData.types,
      houseTypedData.message
    );
    // const houseSign = signTypedData({privateKey, data: typedData, version: SignTypedDataVersion.V4, message:message})
    console.log("House signature ", houseSign);
    console.log("User signature ", signature);
    const { v, r, s } = await ethers.utils.splitSignature(signature);
    console.log("V", v);
    console.log("R", r);
    console.log("S", s);
    //Place bet
    console.log("Place Bet", amount.toString());
    let bet = await house.placeBet(
      betId.toString(),
      amount.toString(),
      deadline,
      deadline,
      v,
      r,
      s,
      paymentToken,
      houseSign
    );
    console.log("Bet", bet);
    let placeBet = await bet.wait();
    console.log(placeBet.events[0]);
    successModal("Success", "Bet Placed Successfully");
  } catch (err) {
    const error = { err };
    const reason = error.err.reason.toString()
    if (reason.length !== 0) {
      if(reason === "execution reverted: BET2WIN: ALREADY_PLACED_BET"){
        failureModal("Error", "Already place bet")
      }else if(reason === "execution reverted: BET2WIN: AMOUNT_OUT_OF_BOUNDS"){
        failureModal("Error", "Stake is too small or too big")
      }
      else{
        failureModal("Error", reason)
      }
    }
  }
  console.groupEnd();
}

export {
  placeBetHandler
}