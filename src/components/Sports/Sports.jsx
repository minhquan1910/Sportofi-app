import React, { useEffect, useCallback } from "react";
import "./Sports.css";
import SportPiece from "./SportPiece";
import Country from "../Country/Country";
import SportMain from "../SportMain/SportMain";
import TimeLine from "../BlockRight/TimeLine";
import { useParams } from "react-router-dom";
import { useAccount } from "wagmi";
import { useDispatch } from "react-redux";
import { uiActions } from "../../store/slices/ui-slice";
const Sports = () => {
  const { isConnected, address } = useAccount()
  const params = useParams();
  const dispatch = useDispatch();

  const toggleConnectWalletModal = useCallback(() => {
    dispatch(uiActions.toggleConnectWalletVisible());
  }, [dispatch]);
  useEffect(() => {
    const code = params?.referralCode;
    if(!isConnected && code) {
      const isInvite = window.location.href.includes("invite");
      if(isInvite){
        toggleConnectWalletModal();
      } 
    }
  }, [isConnected, params.referralCode, toggleConnectWalletModal])
  return (
    // <div className="sporting">
    <main className="mainMain">
      <SportPiece />
      <Country />
      <SportMain />
      <TimeLine />
    </main>
    // </div>
  );
};

export default Sports;
