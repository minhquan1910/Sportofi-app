import constants from "../constants"
import { concat } from "lodash";
// @params
// rawBets: [{id, name, values: [{value, odd}] }]
// return betTypes: [{id, market, name, types: [Number], values: [{value, odd}] }]
const mapBetTypes = (rawBets) => {
    const bets = constants.bets;
    let defaultBetTypes = [];
    Object.keys(bets).forEach((key) => {
        const betLists = bets[key];
        defaultBetTypes = concat(defaultBetTypes, ...betLists);
    })
    const result = defaultBetTypes.map((defaultBet) => {
        const betType = rawBets.find((item) => item.id === defaultBet.id);
        if (betType) {
            return {
                ...defaultBet,
                ...betType,
            };
        }
    }).filter((item) => item);
    return result;
}
export default mapBetTypes;