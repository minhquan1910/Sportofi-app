import { includes, isNumber, toNumber, words } from "lodash";
import constants from "../constants";
// @params
// betTypes: {id, market, name, types: [Number], values: [{value, odd}] }
// idxSide: index of side in values array of betTypes
// return side :{id, value, type}
const mapSide = (betTypes, idxSide) => {
    let side = betTypes?.values?.[idxSide] ?? {};
    side = {
        ...side,
        id: idxSide,
    }
    Object.keys(constants.BET_TYPE).forEach((key) => {
        const type = constants?.BET_TYPE?.[key];
        const isMatchBetType = includes(betTypes?.types ?? [], type);
        if (isMatchBetType) {
            const wordsBetValue = words(side?.value?.toString()?.toLowerCase() ?? "");
            switch (type?.typeApply) {
                case constants.TYPE_APPLY_TO.NUMBERS:
                    if (wordsBetValue.every((word) => isNumber(toNumber(word)))) {
                        side={
                            ...side,
                            type,
                        };
                        return side
                    };
                    break;
                case constants.TYPE_APPLY_TO.WORDS:
                    const wordsApplies = words(type?.applies ?? "");
                    const isMatch = wordsBetValue.some((word) => includes(wordsApplies, word));
                    if (isMatch) {
                        side = {
                            ...side,
                            type,
                        };
                        return side;
                    }
                    break;
                default:
                    break;
            }
        }
    });
    return side;
}
export default mapSide;