import constants from "../constants";

const checkWalletInstalled = () => {
    console.log(window)
    const { ethereum } = window;
    return Boolean(ethereum && ethereum.isMetaMask);
}
const addAndSwitchNetwork = async () => {
    await window.ethereum.request({
            method: 'wallet_addEthereumChain',
            params: [constants.TARGET_NETWORK],
    });
}
const connectWallet = async () => {
    const { ethereum } = window;
    if (!checkWalletInstalled) {
        alert('Get MetaMask!');
        return;
    }
    const currentChainId = await ethereum.request({ method: 'eth_chainId' });
    console.log("currentChainId", currentChainId);
    if (currentChainId !== constants.TARGET_NETWORK.chainId) {
        await addAndSwitchNetwork();
    }
    const accounts = await ethereum.request({ method: 'eth_requestAccounts' });
    return accounts[0];

}
export { checkWalletInstalled, connectWallet, addAndSwitchNetwork };