import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import "./index.css";
import App from "./App";
import store from "./store";
import {
  WagmiConfig,
  createClient,
  configureChains,
  defaultChains,
} from "wagmi";
import { infuraProvider } from "wagmi/providers/infura";
import { publicProvider } from "wagmi/providers/public";
import { InjectedConnector } from "wagmi/connectors/injected";
import { MetaMaskConnector } from "wagmi/connectors/metaMask";
import { CoinbaseWalletConnector } from "wagmi/connectors/coinbaseWallet";
import { WalletConnectConnector } from "wagmi/connectors/walletConnect";
import { BrowserRouter } from "react-router-dom";
import "./services/i18next";
import constants from "./constants";
// import { RainbowKitProvider, getDefaultWallets, connectorsForWallets } from "@rainbow-me/rainbowkit";

window.Buffer = window.Buffer || require("buffer").Buffer;

const { chains, provider, webSocketProvider } = configureChains(
  [constants.CHAIN.bscChain],
  [
    infuraProvider({
      apiKey: process.env.REACT_APP_INFURA_API_KEY,
      priority: 0,
    }),
    publicProvider({
      priority: 1,
    }),
  ]
);

// const {connectors} = getDefaultWallets({
//   appName: "Test",
//   chains
// })
// const connectors = connectorsForWallets([{
//   groupName: "Recommend",
//   wallets: [
//     metaMaskWallet({chains}),
//     coinbaseWallet({chains}),
//     trustWallet({chains})
//   ]
// }])
// Set up client
const client = createClient({
  autoConnect: true,
  connectors: [
    new MetaMaskConnector({ chains }),
    new CoinbaseWalletConnector({
      chains,
      options: {
        appName: "wagmi",
      },
    }),
    new WalletConnectConnector({
      bridge: "https://bridge.keyringpro.com",
      chains,
      options: {
        qrcode: true,
      },
    }),
    new InjectedConnector({
      chains,
      options: {
        name: "Injected",
        shimDisconnect: true,
      },
    }),
  ],
  provider,
  webSocketProvider,
});

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <WagmiConfig client={client}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
    </WagmiConfig>
  </Provider>
);
