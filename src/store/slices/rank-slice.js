
import { createSlice } from "@reduxjs/toolkit";

const rankingSlice = createSlice({
  name: "ranking",
  initialState: {
    ranking: [],
    totalCount: 0,
  },
  reducers: {
    replaceRanking(state, action) {
      state.ranking = action.payload.ranking;
      state.totalCount = action.payload.totalCount;
    },
  },
});

export const rankActions = rankingSlice.actions;

export default rankingSlice.reducer;