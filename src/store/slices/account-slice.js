import { createSlice } from "@reduxjs/toolkit";

const accountSlice = createSlice({
  name: "account",
  initialState: {
    currentAccount: {},
  },
  reducers: {
    setCurrentAccount(state, action) {
      state.currentAccount = action.payload.currentAccount;
    },
  },
});

export const accountActions = accountSlice.actions;

export default accountSlice.reducer;
