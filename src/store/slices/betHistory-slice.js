import { createSlice } from "@reduxjs/toolkit";

const betHistorySlice = createSlice({
  name: "account",
  initialState: {
    betHistoryItem: [],
    betAllHistoryData: [],
    openBetHistoryData: [],
    ranks: [],
  },
  reducers: {
    replaceBetHistory(state, action) {
      state.betHistoryItem = action.payload.betHistory;
    },

    replaceBetAllHistory(state, action) {
      state.betAllHistoryData = action.payload.betAllHistory;
    },
    replaceOpenBetHistory(state, action) {
      state.openBetHistoryData = action.payload.openBetHistory;
    },
    replaceRanks(state, action) {
      state.ranks = action.payload.ranks;
    },
  },
});

export const betHistoryActions = betHistorySlice.actions;

export default betHistorySlice.reducer;
