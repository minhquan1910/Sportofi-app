import { createSlice } from "@reduxjs/toolkit";
import { isEmpty } from "lodash";

const matchSlice = createSlice({
  name: "match",
  initialState: {
    matchItem: [],
    oddItem: [],
    matchAll: [],
    betHistory: [],
    matchLive: [],
    matchLiveDetail: [],
  },
  reducers: {
    replaceMatch(state, action) {
      state.matchItem = action.payload.matchItem;
    },
    replaceOdd(state, action) {
      state.oddItem = action.payload.oddItem;
    },
    addMatchAll(state, action) {
      state.matchAll.push(action.payload.matchAll);
    },
    addMatchLive(state, action) {
      state.matchLive.push(action.payload.matchLive);
    },

    replaceMatchLive(state, action) {
      state.matchLiveDetail = action.payload.matchLiveDetail;
    },
    showBetHistory(state, action) {
      state.betHistory = action.payload.betHistory;
    },
  },
});

export const matchActions = matchSlice.actions;

export default matchSlice.reducer;
