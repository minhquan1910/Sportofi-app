import { createSlice } from "@reduxjs/toolkit";

const uiSlice = createSlice({
  name: "ui",
  initialState: {
    multipleLanguageIsVisible: false,
    oddsIsVisible: false,
    cartIsVisible: false,
    connectWalletIsVisible: false,
    tabCartIsVisible: null,
  },
  reducers: {
    toggleLanguageModal(state) {
      state.multipleLanguageIsVisible = !state.multipleLanguageIsVisible;
    },
    toggleOddsModal(state) {
      state.oddsIsVisible = !state.oddsIsVisible;
    },

    toggleOddsCart(state) {
      state.cartIsVisible = true;
    },

    toggleConnectWalletVisible(state) {
      state.connectWalletIsVisible = !state.connectWalletIsVisible;
    },
    toggleTabCartVisible(state) {
      state.tabCartIsVisible = 0;
    },
    resetTabCartIsVisible(state) {
      state.tabCartIsVisible = null;
    },
  },
});

export const uiActions = uiSlice.actions;

export default uiSlice.reducer;
