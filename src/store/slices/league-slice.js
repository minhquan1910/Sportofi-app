import { createSlice } from "@reduxjs/toolkit";
import { act } from "react-dom/test-utils";

const leagueSlice = createSlice({
  name: "league",
  initialState: {
    leagueItems: [],
    leagueOfWorldCurrent: [],
    leagueOfWorld: [],
    leagueBySeason: [],
    leagueSpainItems: [],
    leagueItalyItems: [],
    leagueGermanyItems: [],
    leagueFranceItems: [],
  },
  reducers: {
    addLeague(state, action) {
      state.leagueItems = action.payload.leagueItem;
    },
    addLeagueSpain(state, action) {
      state.leagueSpainItems = action.payload.leagueSpainItem;
    },
    addLeagueItaly(state, action) {
      state.leagueItalyItems = action.payload.leagueItalyItem;
    },
    addLeagueGermany(state, action) {
      state.leagueGermanyItems = action.payload.leagueGermanyItem;
    },
    addLeagueFrance(state, action) {
      state.leagueFranceItems = action.payload.leagueFranceItem;
    },
    addLeagueOfWorld(state, action) {
      state.leagueOfWorldCurrent = action.payload.leagueOfWorldCurrentItem;
    },
    addWorldLeague(state, action) {
      state.leagueOfWorld = action.payload.leagueOfWorldItem;
    },
    addLeagueDataBySeason(state, action) {
      state.leagueBySeason = action.payload.leagueBySeason;
    },

    resetLeagueCommons(state) {
      state.leaguesCommon = [];
    },
  },
});

export const leagueActions = leagueSlice.actions;

export default leagueSlice.reducer;
