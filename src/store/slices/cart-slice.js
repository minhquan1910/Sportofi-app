import { createSlice } from "@reduxjs/toolkit";
import { isEmpty } from "lodash";

const cartSlice = createSlice({
  name: "cart",
  initialState: {
    cartItem: [],
    qualityStake: 0,
    oddsActive: false,
    totalOdd: 0,
    liability: 0,
    profit: 0,
  },
  reducers: {
    isActiveOdds(state) {
      state.oddsActive = !state.oddsActive;
    },

    replaceCart(state, action) {
      state.cartItem = action.payload.cartItem;
    },

    addOddHandler(state, actions) {
      const newCart = actions.payload;

      if (state.cartItem.length !== 0) {
        state.cartItem = [];
      }
      state.cartItem.push({
        id: newCart.oddItemId,
        side: newCart.side,
        odd: +newCart.oddValue,
        betType: newCart.betType,
      });
      state.qualityStake = isEmpty(newCart.oddValue) ? 0 : 1;
      state.liability = isEmpty(newCart.oddValue) ? 0 : 1.0;
      state.totalOdd = state.cartItem[0].odd;
      state.profit = state.totalOdd - state.liability;
    },

    addQualityStake(state, actions) {
      state.qualityStake = actions.payload;
      state.liability = state.qualityStake;
      state.totalOdd = state.qualityStake * state.cartItem[0].odd;
      state.profit = state.totalOdd - state.liability;
    },
  },
});

export const cartActions = cartSlice.actions;

export default cartSlice.reducer;
