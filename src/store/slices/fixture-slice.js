import { createSlice } from "@reduxjs/toolkit";

const fixtureSlice = createSlice({
  name: "fixture",
  initialState: {
    fixtureItem: [],
    fixtureDetail: [],
    fixtureLive: [],
  },
  reducers: {
    fixtureUpcoming(state, action) {
      state.fixtureItem = action.payload.fixtureItem;
    },
    replacefixtureDetail(state, action) {
      state.fixtureDetail = action.payload.fixtureDetailItem;
    },
    fixtureLive(state, action) {
      state.fixtureLive = action.payload.fixtureLive;
    },
  },
});

export const fixtureActions = fixtureSlice.actions;

export default fixtureSlice.reducer;
