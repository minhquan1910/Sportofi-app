import { createSlice } from "@reduxjs/toolkit";
import { act } from "react-dom/test-utils";

const countrySlice = createSlice({
  name: "country",
  initialState: {
    countryItems: [],
  },
  reducers: {
    addCountry(state, action) {
      state.countryItems = action.payload.countryItem;
    },
  },
});

export const countryActions = countrySlice.actions;

export default countrySlice.reducer;
