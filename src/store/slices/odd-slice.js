import { createSlice } from "@reduxjs/toolkit";

const oddSlice = createSlice({
  name: "odd",
  initialState: {
    oddItems: [],
    oddMatchBookmaker: [],
    oddMatchInPlay: [],
  },
  reducers: {
    oddFixture(state, action) {
      state.oddItems.push(action.payload.oddItem);
    },

    oddMatchBookmarkerReplace(state, action) {
      state.oddMatchBookmaker = action.payload.oddMatchBookmakerItem;
    },

    oddMatchInplayReplace(state, action) {
      state.oddMatchInPlay = action.payload.oddMatchInPlayItem;
    },
  },
});

export const oddActions = oddSlice.actions;

export default oddSlice.reducer;
