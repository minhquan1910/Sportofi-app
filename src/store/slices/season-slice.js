import { createSlice } from "@reduxjs/toolkit";

const seasonSlice = createSlice({
  name: "season",
  initialState: {
    seasonItem: [],
  },
  reducers: {
    replaceSeason(state, action) {
      state.seasonItem = action.payload.seasonItem;
    },
  },
});

export const seasonActions = seasonSlice.actions;

export default seasonSlice.reducer;
