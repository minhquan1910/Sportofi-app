import { configureStore } from "@reduxjs/toolkit";
import uiReducer from "./slices/ui-slice";
import cartReducer from "./slices/cart-slice";
import leagueReducer from "./slices/league-slice";
import seasonReducer from "./slices/season-slice";
import matchReducer from "./slices/match-slice";
import oddReducer from "./slices/odd-slice";
import fixtureReducer from "./slices/fixture-slice";
import betHistoryReducer from "./slices/betHistory-slice";
import countryReducer from "./slices/country-slice";
import accountReducer from "./slices/account-slice";
import rankReducer from "./slices/rank-slice";
const store = configureStore({
  reducer: {
    ui: uiReducer,
    cart: cartReducer,
    league: leagueReducer,
    season: seasonReducer,
    match: matchReducer,
    odd: oddReducer,
    fixture: fixtureReducer,
    betHistory: betHistoryReducer,
    country: countryReducer,
    account: accountReducer,
    ranking: rankReducer,
  },
});

export default store;
