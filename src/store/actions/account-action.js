
import { accountActions } from "../slices/account-slice";
import constants from "../../constants";
const url = `${constants.apiCloudConfigs.DOMAIN}${constants.apiCloudConfigs.SUB_DOMAIN}`;
export const fetchAccount = (addressWallet) => {
    return async (dispatch) => {
        const fetchData = async (address) => {
            const response = await fetch(`${url}${constants.apiCloudConfigs.endpoints.GET_ACCOUNT}`, {
                method: "POST",
                body: JSON.stringify({
                    address: address
                }),
            });
            if (!response.ok) {
                throw new Error("Could not fetch account data");
            }
            const data = await response.json();
            return data;
        };

        try {
            const accountData = await fetchData(addressWallet);
            dispatch(
                accountActions.setCurrentAccount({
                    currentAccount: accountData?.data ?? {},
                })
            );
        } catch (error) {
            console.error(error.message);
        }
    };
};

export const saveAccount = (addressWallet, referralCode) => {
    return async (dispatch) => {
        const saveData = async (address, referralCode) => {
            const response = await fetch(`${url}${constants.apiCloudConfigs.endpoints.SAVE_ACCOUNT}`, {
                method: "POST",
                body: JSON.stringify({
                    address: address,
                    referralCode: referralCode ?? undefined,
                }),
            });
            if (!response.ok) {
                throw new Error("Could not save account data");
            }
            const data = await response.json();
            return data;
        };

        try {
            await saveData(addressWallet,referralCode);
        } catch (error) {
            console.error(error.message);
        }
    };
};