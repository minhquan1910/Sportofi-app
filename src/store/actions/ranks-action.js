import { rankActions } from "../slices/rank-slice";
import constants from "../../constants";
const url = `${constants.apiCloudConfigs.DOMAIN}${constants.apiCloudConfigs.SUB_DOMAIN}${constants.apiCloudConfigs.endpoints.GET_RANKS}`;

export const fetchRanking = (page, pageSize) => {
  
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(`${url}`, {
        method: "POST",

        body: JSON.stringify({
          page,
          pageSize,
        }),
      });
      if (!response.ok) {
        throw new Error("Could not fetch ranking data!");
      }
      const data = await response.json();
      return data;
    };

    try {
      const rankingRes = await fetchData();
      console.log("rankingRes", rankingRes);

      dispatch(
        rankActions.replaceRanking({
            ranking: rankingRes.data?.rankings || [],
            totalCount: rankingRes.data?.totalCount || 0
        })
      );
    } catch (error) {
      console.log(error.message);
    }
  };
}