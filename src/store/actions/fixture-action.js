import { fixtureActions } from "../slices/fixture-slice";
import { OPTIONS } from "../../services/api/options";

// const options = {
//   method: "GET",
//   headers: {
//     "X-RapidAPI-Key": "177c193834msh3f7504588eae5c3p1ac8d5jsn6e8450dc2cb0",
//     "X-RapidAPI-Host": "api-football-v1.p.rapidapi.com",
//   },
// };

const url = "https://api-football-v1.p.rapidapi.com/v3/fixtures?";

var dateObj = new Date();
var month = dateObj.getUTCMonth() + 1;
var day = ("0" + dateObj.getUTCDate()).slice(-2);
var year = dateObj.getUTCFullYear();

var dateFrom = year + "-" + month + "-" + day;

export const fetchFixtureUpcomingData = (leagueId, season, dateTo) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `${url}league=${leagueId}&season=${season}&from=${dateFrom}&to=${dateTo}`,
        OPTIONS
      );

      if (!response.ok) {
        throw new Error("Could not fetch fixture upcomming data");
      }

      const data = await response.json();
      return data;
    };

    try {
      const fixtureUpcomingData = await fetchData();

      dispatch(
        fixtureActions.fixtureUpcoming({
          fixtureItem: fixtureUpcomingData.response || [],
        })
      );
    } catch (error) {
      console.error(error);
    }
  };
};
export const fetchFixtureLive = (leagueId) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `${url}live=all&league=${leagueId}`,
        OPTIONS
      );

      if (!response.ok) {
        throw new Error("Could not fetch fixture live data");
      }

      const data = await response.json();
      return data;
    };

    try {
      const fixtureLiveData = await fetchData();

      dispatch(
        fixtureActions.fixtureLive({
          fixtureLive: fixtureLiveData.response || [],
        })
      );
    } catch (error) {
      console.error(error);
    }
  };
};

export const fetchFixtureDetails = (fixtureId) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(`${url}id=${fixtureId}`, OPTIONS);

      if (!response.ok) {
        throw new Error("Could not fetch fixture by id");
      }

      const data = await response.json();
      return data;
    };

    try {
      const fixtureDetaiData = await fetchData();

      dispatch(
        fixtureActions.replacefixtureDetail({
          fixtureDetailItem: fixtureDetaiData.response || [],
        })
      );
    } catch (error) {
      console.log(error);
    }
  };
};
