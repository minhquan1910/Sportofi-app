// import { OPTIONS } from "../../services/api/options";

import { betHistoryActions } from "../slices/betHistory-slice";
import constants from "../../constants";
const url = `${constants.apiCloudConfigs.DOMAIN}${constants.apiCloudConfigs.SUB_DOMAIN}${constants.apiCloudConfigs.endpoints.GET_HISTORY}`;
const options = {
  sort: {
    placedBetTime: -1,
    createdAt: -1,
  },
};

export const fetchBetHistory = (addressWallet) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(`${url}`, {
        method: "POST",

        body: JSON.stringify({
          address: addressWallet,
          options: options,
        }),
      });
      if (!response.ok) {
        throw new Error("Could not fetch bet history data!");
      }
      const data = await response.json();
      return data;
    };

    try {
      const matchBetHistory = await fetchData();

      dispatch(
        betHistoryActions.replaceBetHistory({
          betHistory: matchBetHistory.data || [],
        })
      );
    } catch (error) {
      console.log(error.message);
    }
  };
};

export const fetchBetAllHistory = (limit = undefined) => {
  return async (dispatch) => {
    const optionObj = limit
      ? {
          ...options,
          limit: limit,
        }
      : options;

    const fetchData = async () => {
      const response = await fetch(`${url}`, {
        method: "POST",
        body: JSON.stringify({
          options: optionObj,
        }),
      });
      if (!response.ok) {
        throw new Error("Could not fetch bet all history data!");
      }
      const data = await response.json();
      return data;
    };

    try {
      const betAllHistory = await fetchData();

      dispatch(
        betHistoryActions.replaceBetAllHistory({
          betAllHistory: betAllHistory.data || [],
        })
      );
    } catch (error) {
      console.log(error.message);
    }
  };
};

export const fetchOpenBetHistory = (addressWallet) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(`${url}`, {
        method: "POST",

        body: JSON.stringify({
          address: addressWallet,
          isSolved: false,
          options: options,
        }),
      });
      if (!response.ok) {
        throw new Error("Could not fetch bet history data!");
      }
      const data = await response.json();
      return data;
    };

    try {
      const matchBetHistory = await fetchData();

      dispatch(
        betHistoryActions.replaceOpenBetHistory({
          openBetHistory: matchBetHistory.data || [],
        })
      );
    } catch (error) {
      console.log(error.message);
    }
  };
};
