import { leagueActions } from "../slices/league-slice";
import { OPTIONS } from "../../services/api/options";

const url = "https://api-football-v1.p.rapidapi.com/v3/leagues?";

const year = new Date().getFullYear();

export const fetchLeagueData = (countryCode) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `${url}code=${countryCode}&season=${year}`,
        OPTIONS
      );

      if (!response.ok) {
        throw new Error("Could not fetch league data!");
      }

      const data = await response.json();

      return data;
    };

    try {
      const leagueData = await fetchData();

      dispatch(
        leagueActions.addLeague({
          leagueItem: leagueData.response,
        })
      );
    } catch (error) {
      console.log(error);
    }
  };
};
export const fetchLeagueSpainData = (countryCode) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `${url}code=${countryCode}&season=${year}`,
        OPTIONS
      );

      if (!response.ok) {
        throw new Error("Could not fetch league data!");
      }

      const data = await response.json();

      return data;
    };

    try {
      const leagueSpainData = await fetchData();

      dispatch(
        leagueActions.addLeagueSpain({
          leagueSpainItem: leagueSpainData.response,
        })
      );
    } catch (error) {
      console.log(error);
    }
  };
};

export const fetchLeagueItalyData = (countryCode) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `${url}code=${countryCode}&season=${year}`,
        OPTIONS
      );

      if (!response.ok) {
        throw new Error("Could not fetch league data!");
      }

      const data = await response.json();

      return data;
    };

    try {
      const leagueItalyData = await fetchData();

      dispatch(
        leagueActions.addLeagueItaly({
          leagueItalyItem: leagueItalyData.response,
        })
      );
    } catch (error) {
      console.log(error);
    }
  };
};

export const fetchLeagueGermanyData = (countryCode) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `${url}code=${countryCode}&season=${year}`,
        OPTIONS
      );

      if (!response.ok) {
        throw new Error("Could not fetch league data!");
      }

      const data = await response.json();

      return data;
    };

    try {
      const leagueGermanyData = await fetchData();

      dispatch(
        leagueActions.addLeagueGermany({
          leagueGermanyItem: leagueGermanyData.response,
        })
      );
    } catch (error) {
      console.log(error);
    }
  };
};

export const fetchLeagueFranceData = (countryCode) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `${url}code=${countryCode}&season=${year}`,
        OPTIONS
      );

      if (!response.ok) {
        throw new Error("Could not fetch league data!");
      }

      const data = await response.json();

      return data;
    };

    try {
      const leagueFranceData = await fetchData();

      dispatch(
        leagueActions.addLeagueFrance({
          leagueFranceItem: leagueFranceData.response,
        })
      );
    } catch (error) {
      console.log(error);
    }
  };
};

export const fetchLeagueDataByCountryName = (countryName) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `${url}country=${countryName}&season=${year}`,
        OPTIONS
      );

      if (!response.ok) {
        throw new Error("Could not fetch league data by country name!");
      }

      const data = await response.json();

      return data;
    };

    try {
      const leagueDataByCountryName = await fetchData();

      dispatch(
        leagueActions.addWorldLeague({
          leagueOfWorldItem: leagueDataByCountryName.response,
        })
      );
    } catch (error) {
      console.log(error);
    }
  };
};

export const fetchLeaguesOfWorldData = (season, type, current) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `${url}season=${season}&type=${type}&current=${current}`,
        OPTIONS
      );

      if (!response.ok) {
        throw new Error("Could not fetch league of world data by league type!");
      }

      const data = await response.json();

      return data;
    };

    try {
      const leagueOfWorldDataByLeagueType = await fetchData();

      dispatch(
        leagueActions.addLeagueOfWorld({
          leagueOfWorldCurrentItem:
            leagueOfWorldDataByLeagueType.response || [],
        })
      );
    } catch (error) {
      console.log(error);
    }
  };
};

export const fetchLeaguesDataBySeason = (season, type, current) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `${url}season=${season}&type=${type}&current=${current}`,
        OPTIONS
      );

      if (!response.ok) {
        throw new Error("Could not fetch league data by season!");
      }
      const data = await response.json();

      return data;
    };

    try {
      const leagueDataBySeason = await fetchData();
      dispatch(
        leagueActions.addLeagueDataBySeason({
          leagueBySeason: leagueDataBySeason.response || [],
        })
      );
    } catch (error) {
      console.log(error);
    }
  };
};
