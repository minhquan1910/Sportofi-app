import { countryActions } from "../slices/league-slice";

const url = "https://api-football-v1.p.rapidapi.com/v3/countries";
export const fetchCountryData = (countryCode) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch({ url });

      if (!response.ok) {
        throw new Error("Could not fetch league data!");
      }

      const data = await response.json();

      return data;
    };

    try {
      const countryData = await fetchData();

      dispatch(
        countryActions.addCountry({
          countryItem: countryData.response,
        })
      );
    } catch (error) {
      console.log(error);
    }
  };
};
