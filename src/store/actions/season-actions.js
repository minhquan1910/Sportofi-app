import { seasonActions } from "../slices/season-slice";
import { OPTIONS } from "../../services/api/options";
// const url =
//   "https://app.sportdataapi.com/api/v1/soccer/seasons?apikey=4e594540-4a20-11ed-8312-99c50d9a0eb4";

const url = "";

export const fetchSeasonData = (leagueId) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(`${url}&league_id=${leagueId}`);
      if (!response.ok) {
        throw new Error("Could not fetch seasons data!");
      }
      const data = await response.json();
      return data;
    };

    try {
      const seasonData = await fetchData();
      dispatch(
        seasonActions.replaceSeason({
          seasonItem: seasonData.data || [],
        })
      );
    } catch (error) {
      console.log(error.message);
    }
  };
};
