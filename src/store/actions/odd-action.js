import { oddActions } from "../slices/odd-slice";
import { OPTIONS } from "../../services/api/options";
import constants from "../../constants";

export const fetchOddData = (matchId) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `https://api-football-v1.p.rapidapi.com/v3/odds?fixture=${matchId}&bookmaker=${constants.DEFAULTS.BOOKMARKER.code}`,
        OPTIONS
      );
      if (!response.ok) {
        throw new Error("Could not fetch odds data!");
      }
      const data = await response.json();
      return data;
    };

    try {
      const oddData = await fetchData();

      dispatch(
        oddActions.oddFixture({
          oddItem: oddData.response[0] || {
            fixture: {
              id: "undefine",
            },
          },
        })
      );
    } catch (error) {
      console.log(error.message);
    }
  };
};

export const fetchOddMatchBookmakerData = (matchId) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `https://api-football-v1.p.rapidapi.com/v3/odds?fixture=${matchId}&bookmaker=1`,
        OPTIONS
      );
      if (!response.ok) {
        throw new Error("Could not fetch odds data!");
      }
      const data = await response.json();
      return data;
    };

    try {
      const oddMatchBookmaker = await fetchData();

      dispatch(
        oddActions.oddMatchBookmarkerReplace({
          oddMatchBookmakerItem: oddMatchBookmaker.response[0] || {},
        })
      );
    } catch (error) {
      console.log(error.message);
    }
  };
};

export const fetchOddMatchInPlayData = (matchId) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `https://api-football-v1.p.rapidapi.com/v3/odds/live?fixture=${matchId}`,
        OPTIONS
      );
      if (!response.ok) {
        throw new Error("Could not fetch odds match in play data!");
      }
      const data = await response.json();
      return data;
    };

    try {
      const oddMatchInPlay = await fetchData();

      dispatch(
        oddActions.oddMatchInplayReplace({
          oddMatchInPlayItem: oddMatchInPlay.response || [],
        })
      );
    } catch (error) {
      console.log(error.message);
    }
  };
};
