import { isEmpty } from "lodash";
import moment from "moment";
import { OPTIONS } from "../../services/api/options";
import { matchActions } from "../slices/match-slice";

const url = "https://api-football-v1.p.rapidapi.com/v3/fixtures?";
const CURRENT_YEAR = moment().year();
export const fetchMatchData = (matchId) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(`${url}&id=${matchId}`, OPTIONS);
      if (!response.ok) {
        throw new Error("Could not fetch match data!");
      }
      const data = await response.json();
      return data;
    };

    try {
      const matchData = await fetchData();

      dispatch(
        matchActions.replaceMatch({
          matchItem: matchData.response[0] || [],
        })
      );
    } catch (error) {
      console.error(error.message);
    }
  };
};

export const fetchMatchAllData = (leagueId, dateFrom, dateTo) => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        `${url}&league=${leagueId}&season=${CURRENT_YEAR}&from=${dateFrom}&to=${dateTo}`,
        OPTIONS
      );
      if (!response.ok) {
        throw new Error("Could not fetch match all data!");
      }
      const data = await response.json();
      return data;
    };

    try {
      const matchAllData = await fetchData();

      dispatch(
        matchActions.addMatchAll({
          matchAll: matchAllData.response || [],
        })
      );
    } catch (error) {
      console.error(error.message);
    }
  };
};

export const fetchLiveMatchData = (leagueId) => {
  return async (dispatch) => {
    const fetchData = async () => {
      let URL = `${url}&season=${CURRENT_YEAR}&live=all`;
      if (!isEmpty(leagueId)) {
        URL += `&league=${leagueId}`;
      }
      const response = await fetch(URL, OPTIONS);
      if (!response.ok) {
        throw new Error("Could not fetch match all data!");
      }
      const data = await response.json();
      return data;
    };

    try {
      const matchLiveData = await fetchData();

      dispatch(
        matchActions.addMatchLive({
          matchLive: matchLiveData.response || [],
        })
      );
    } catch (error) {
      console.error(error.message);
    }
  };
};

// export const fetchLiveMatchDetailData = (leagueId) => {
//   return async (dispatch) => {
//     const fetchData = async () => {
//       let URL = `${url}&season=${CURRENT_YEAR}&live=all`;
//       if (!isEmpty(leagueId)) {
//         URL += `&league=${leagueId}`;
//       }
//       const response = await fetch(URL, OPTIONS);
//       if (!response.ok) {
//         throw new Error("Could not fetch match detail data!");
//       }
//       const data = await response.json();
//       return data;
//     };

//     try {
//       const matchLiveDetailData = await fetchData();
//       console.log(matchLiveDetailData);
//       dispatch(
//         matchActions.replaceMatchLive({
//           matchLiveDetail: matchLiveDetailData.response || [],
//         })
//       );
//     } catch (error) {
//       console.log(error.message);
//     }
//   };
// };
