module.exports = async (statusCode, result) => {
    console.log("Wrapper - result: " + JSON.stringify(result, null, 4));
    const response = {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify({
            code: statusCode,
            data: result
        }),
    };
    console.log("Wrapper - response: " + JSON.stringify(response,null,4));
    return response;
}