const BetPlacedHistoryModel = require("../models/betHistory");
const wrapper = require('../helpers/wrapper');
const DatabaseImpl = require("../database");

const getHistories = async (event, context) => {
    try {
        console.log("Body: " + event.body);
        const body = JSON.parse(event.body);
        const address = body?.address;
        const matchId = body?.matchId;
        const isSolved = body?.isSolved;
        const isPaid = body?.isPaid;
        const options = body?.options ?? {};
        const filters = {};
        if (address) {
            filters.address = address;
        }
        if (matchId) {
            filters.matchId = matchId;
        }
        if (isSolved !== undefined) {
            filters.isSolved = isSolved;
        }
        if (isPaid !== undefined) {
            filters.isPaid = isPaid;
        }
        const optionsObj = {};
        if (options?.limit) {
            optionsObj.limit = options.limit;
        }
        if (options?.skip) {
            optionsObj.skip = options.skip;
        }
        if (options?.sort) {
            optionsObj.sort = options.sort;
        }
        console.log(`filters: ${JSON.stringify(filters, null, 2)}`);
        console.log(`address: ${address}`);
        console.log(`OptionsObj: ${optionsObj}`);
        const histories = await BetPlacedHistoryModel.find(filters, optionsObj);
        console.log("Get histories successfully");
        console.log("Histories get from db: ", histories);
        return histories;
    } catch (error) {
        return new Promise((resolve, reject) => {
            reject(error);
        });
    }

}
exports.handler = async function (event, context) {
    const db = new DatabaseImpl();
    try {
        await db.connect();
        const result = await getHistories(event, context);
        await db.close();
        return wrapper(200, result);
    } catch (error) {
        await db.close();
        console.error(error);
        return wrapper(500, { error: error.message });
    }
}