
const DatabaseImpl = require("../database");
const BetPlacedHistoryModel = require("../models/betHistory");
const wrapper = require('../helpers/wrapper');

const updateHistory = async (event, context) => {
    try{
        console.log("Body: " + event.body);
        const body = JSON.parse(event.body);
        const matchId = body?.matchId;
        const address = body?.address;
        const isSolved = body?.isSolved;
        const isPaid = body?.isPaid;
        const updateObj = {};
        if(isSolved !== undefined){
            updateObj.isSolved = isSolved;
        }
        if(isPaid !== undefined){
            updateObj.isPaid = isPaid;
        }
        const filters = {}
        if(matchId){
            filters.matchId = matchId;
        }
        if(address){
            filters.address = address;
        }
        console.log(`matchId: ${matchId}`);
        console.log(`updateObj: ${JSON.stringify(updateObj, null, 2)}`);
        await BetPlacedHistoryModel.update(filters, updateObj);
        console.log("Update histories successfully");
    }catch(error){
        return new Promise((resolve, reject) => {
            reject(error);
        });
    }

}
exports.handler = async function(event, context) {
    const db = new DatabaseImpl();
    try {
        await db.connect();
        const result = await updateHistory(event, context);
        await db.close();
        return wrapper(200, result);
    } catch (error) {
        await db.close();
        console.error(error);
        return wrapper(500, {error: error.message});
    }
}