const wrapper = require('../helpers/wrapper');
const BetPlacedHistoryModel = require("../models/betHistory");
const DatabaseImpl = require("../database");
const getRanks = async (event, context) => {
    try {
        const {
            filter,
            sort,
            page,
            pageSize
        } = JSON.parse(event.body);
        const query = {};
        if (filter?.from) {
            query.placedBetTime = {
                $gte: filter.from
            }
        }
        if (filter?.addresses) {
            query.address = {
                $in: filter.addresses
            }
        }
        console.log("Query", query);
        const ranks = await BetPlacedHistoryModel.getRanks({
            filter: query,
            sort: sort,
            page: page,
            pageSize: pageSize
        });
        return ranks;
    } catch (error) {
        return new Promise((resolve, reject) => {
            reject(error);
        });
    }
}
exports.handler = async function (event, context) {
    const db = new DatabaseImpl();
    try {
        await db.connect();
        const result = await getRanks(event, context);
        await db.close();
        return wrapper(200, result);
    } catch (error) {
        await db.close();
        console.error(error);
        return wrapper(500, { error: error.message });
    }
}