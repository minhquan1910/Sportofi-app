
const BetPlacedHistoryModel = require("../models/betHistory");
const DatabaseImpl = require("../database");
const wrapper = require('../helpers/wrapper');

const saveHistory = async (event, context) => {
    try{
        console.log("Body: " + event.body);
        const {
            betId,
            user,
            matchId,
            side,
            settleStatus, 
            odd,
            amount,
        } = JSON.parse(event.body);
        console.log(`user: ${user}, matchId: ${matchId}, side: ${side}, settleStatus: ${settleStatus}, odd: ${odd}`);
        console.log("Saving history...");
        await BetPlacedHistoryModel.save(betId,user,matchId,side,odd, settleStatus, amount);
        console.log("History saved");
        console.log("Save history successfully");
        return { 
            user,
            matchId,
            side,
            settleStatus, 
            odd
        }
    }catch(error){
        return new Promise((resolve, reject) => {
            reject(error);
        });
    }

}
exports.handler = async function(event, context) {
    const db = new DatabaseImpl();
    try {
        await db.connect();
        const result = await saveHistory(event, context);
        await db.close();
        return wrapper(200, result);
    } catch (error) {
        await db.close();
        console.error(error);
        return wrapper(500, {error: error.message});
    }
}