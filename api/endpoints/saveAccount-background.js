
const wrapper = require('../helpers/wrapper');
const DatabaseImpl = require("../database");
const constants = require('../constants');
const AccountModel = require("../models/account");
const { default: randomCode } = require('../helpers/random');
const checkReferralCode = async (referralCode) => {
    if(!referralCode){
        const adminAddress = constants.ADDRESS_DEFAULT_REFERRAL.toLowerCase();
        const adminAccount = await AccountModel.findOne({address: adminAddress});
        if(!adminAccount){
            const account = await AccountModel.save({
                address: adminAddress,
            });
            return account;
        }
        return adminAccount;
    }else{
        const account = await AccountModel.findOne({code: referralCode});
        if(!account){
            throw new Error("Referral code is invalid");
        }
        return account;
    }
}
const getAllDescendants = async (address) => {
    console.log("Get all descendants of account: " + address);
    const accountData = await AccountModel.findOne({address: address.toLowerCase()});
    console.log("Account data: ", accountData);
    if(!accountData){
        console.log("Account not found");
        return {};
    }
    const children = await AccountModel.find({ref: accountData.address});
    const descendants = await Promise.all(children.map(async (child) => {
        const descendants = await getAllDescendants(child.address);
        return descendants;
    }));
    return {
        ...accountData,
        children: descendants,
    };
}
const saveAccount = async (event, context) => {
    try{
        
        console.log("Body: " + event.body);
        const {
            address,
            referralCode
        } = JSON.parse(event.body);
        if(!address){
            throw new Error("Address is required");
        }
        const filter = { address: address.toLowerCase() };
        const account = await AccountModel.findOne(filter);
        if(!account){
            console.log(`Account ${address} not found, create new account`);
            const accountCode = randomCode(constants.DEFAULT_LENGTH_CODE);
            console.log(`Random account code: ${accountCode}`);
            console.log(`Referral code: ${referralCode}`);
            const referralAccount = await checkReferralCode(referralCode);
            console.log("Referral account:", referralAccount);
            const referralAddress = referralAccount?.address;
            console.log("Referral address:", referralAddress);
            // save new account
            console.log("Save new account");
            const refsOfReferral = referralAccount?.refArray;
            const refsOfAccount = refsOfReferral ? [...refsOfReferral, referralAddress] : [referralAddress];
            const account = await AccountModel.save({
                address: address.toLowerCase(),
                ref: referralAddress,
                refArray: refsOfAccount,
                code: accountCode,
                updatingChildren: true,
            });
            console.log("Account saved: ", account);
            for (let i = 0; i < account.refArray.length; i++) {
                const refAddress = account.refArray[i];
                console.log("Update ref account: " + refAddress);
                const childrenOfRef = await getAllDescendants(refAddress);
                console.log("Children of ref: ", childrenOfRef);
                await AccountModel.updateOne({address: refAddress}, { children: childrenOfRef.children, updatingChildren: false });
            }
            return account;
        }else {
            console.log(`Account ${address} found`);
            return account
        }
    }catch(error){
        return new Promise((resolve, reject) => {
            reject(error);
        });
    }
}
exports.handler = async function(event, context) {
    const db = new DatabaseImpl();
    try {
        await db.connect();
        const result = await saveAccount(event, context);
        await db.close();
        return wrapper(200, result);
    } catch (error) {
        await db.close();
        console.error(error);
        return wrapper(500, {error: error.message});
    }
}