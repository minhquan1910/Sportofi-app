
const wrapper = require('../helpers/wrapper');
const DatabaseImpl = require("../database");
const AccountModel = require("../models/account");
const getAccount = async (event, context) => {
    try{
        const {
            address
        } = JSON.parse(event.body);
        if(!address){
            throw new Error("Address is required");
        }
        console.log("address", address);
        const account = await AccountModel.findOne({address: address.toLowerCase()});
        return account;
    }catch(error){
        return new Promise((resolve, reject) => {
            reject(error);
        });
    }
}
exports.handler = async function(event, context) {
    const db = new DatabaseImpl();
    try {
        await db.connect();
        const result = await getAccount(event, context);
        await db.close();
        return wrapper(200, result);
    } catch (error) {
        await db.close();
        console.error(error);
        return wrapper(500, {error: error.message});
    }
}