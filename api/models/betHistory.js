var mongoose = require("mongoose");
var constants = require("../constants");
const moment = require("moment");
var axios = require('axios');
const { ethers } = require("ethers");
const { toString, isEmpty } = require("lodash");
const BetPlacedHistory = new mongoose.Schema({
    betId: Number,
    address: String,
    matchId: Number,
    side: {
        code: Number,
        name: String
    },
    odd: Number,
    settleStatus: {
        code: Number,
        name: String
    },
    isSolved: Boolean,
    isPaid: Boolean,
    amount: Number,
    placedBetTime: Number,
    matchDetails: mongoose.Schema.Types.Mixed
}, {
    timestamps: true
});
const DBBetPlacedHistoryModel = mongoose.model(constants.mongoDb.collectionName.BET_PLACED_HISTORY, BetPlacedHistory);
class BetPlacedHistoryModel {
    static async save(betId, address, matchId, sideCode, odd, settleStatusCode, amount) {
        // get settle status obj

        const oddUrl = `${constants.data_api.DOMAIN_NAME}${constants.data_api.endpoints.ODDS}`;
        const optionsOddBet = {
            method: constants.data_api.method.GET,
            url: oddUrl,
            headers: {
                [constants.data_api.headers.X_RAPID_API_HOST]: constants.data_api.API_HOST,
                [constants.data_api.headers.X_RAPID_API_KEY]: process.env.REACT_APP_RAPID_API_KEY
            },
            params: {
                fixture: matchId,
                bet: settleStatusCode,
                bookmaker: constants.DEFAULTS.BOOKMARKER.code
            }
        }
        console.log("Options odd bets", optionsOddBet);
        const oddResponse = (await axios(optionsOddBet))?.data?.response;
        const oddResult = oddResponse?.[0] ?? {};
        console.log("Odd result", oddResult);
        const bet = oddResult?.bookmakers?.[0]?.bets?.[0] ?? {};
        const settleStatus = {
            code: bet?.id ?? "",
            name: bet?.name ?? ""
        }
        const fixtureUrl = `${constants.data_api.DOMAIN_NAME}${constants.data_api.endpoints.FIXTURES}`;
        console.log("Fixture url", fixtureUrl);
        const fixtureOptions = {
            url: fixtureUrl,
            method: "GET",
            params: { id: matchId },
            headers: {
                "X-RapidAPI-Key": process.env.REACT_APP_RAPID_API_KEY,
                "X-RapidAPI-Host": constants.data_api.API_HOST,
            },
        };
        console.log("Fixtures Options", fixtureOptions);
        const match = (await axios(fixtureOptions))?.data?.response?.[0] ?? {};
        delete match?.events;
        delete match?.lineups;
        delete match?.statistics;
        delete match?.players;
        console.log("Match", match);
        const sides = bet?.values ?? [];
        const betSide = (sideCode <= sides.length) ? sides[sideCode - 1] : {};
        const sideObj = {
            code: sideCode,
            name: betSide?.value ?? ""
        }
        const betPlacedHistory = new DBBetPlacedHistoryModel({
            betId,
            address,
            matchId,
            side: sideObj,
            odd,
            settleStatus,
            isSolved: false,
            isPaid: false,
            amount,
            placedBetTime: moment().unix(),
            matchDetails: match
        });
        console.log("Bet placed history", betPlacedHistory);
        const result = await betPlacedHistory.save();
        console.log("Result", result);
        return result;
    }
    static async find(filter, options) {
        return await DBBetPlacedHistoryModel.find(filter, null, options);
    }
    static async update(filter, update) {
        return await DBBetPlacedHistoryModel.updateMany(filter, update);
    }
    static async getRanks({ filter, page, pageSize, sort }) {
        const query = filter ? filter : {};
        const matchPipe = {
            $match: query
        }
        const sortPipe = sort ? {
            $sort: {
                totalAmount: -1,
                ...sort
            }
        } : {
            $sort: {
                totalAmount: -1
            }
        };
        console.log("Sort pipe", sortPipe);
        const pipeline = [
            matchPipe,
            {
                $group: {
                    _id: "$address",
                    total: {
                        $sum: "$amount"
                    }
                }
            },
            {
                $project: {
                    address: "$_id",
                    totalAmount: "$total",
                    _id: 0
                }
            },
            // sortPipe
        ];
        console.log("Pipeline", pipeline);
        const rankings = await DBBetPlacedHistoryModel.aggregate(pipeline);
        const contracts = new ethers.Contract(constants.contracts.GTOKEN.address, constants.contracts.GTOKEN.abi, constants.provider);
        console.log("contracts", contracts.address);
        const mapWithBalance = (await Promise.all(rankings?.map(async (rank) => {
            const balance = await contracts.balanceOf(rank.address);
            if (balance.gt(0)) {
                return {
                    ...rank,
                    balance: toString(ethers.utils.formatEther(balance))
                }
            }
        }))).filter(item => item).sort((a, b) => b?.balance - a?.balance);
        console.log("Map with balance", mapWithBalance);
        let result = [];
        console.log("Page and page size", page, pageSize);
        if(page && pageSize) {
            const startIndex = ((page - 1) * pageSize);
            const endIndex = (page * pageSize) > mapWithBalance.length ? mapWithBalance.length : (page * pageSize);
            result = mapWithBalance.slice(startIndex, endIndex);
        }
        return {
            totalCount: mapWithBalance.length ?? 0, 
            rankings: result,
        };
    }
}
module.exports = BetPlacedHistoryModel;