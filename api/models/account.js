var mongoose = require("mongoose");
var constants = require("../constants");
const { default: randomCode } = require("../helpers/random");
const Account = new mongoose.Schema({
    address: String,
    ref: String,
    commission: Number,
    refArray: Array,
    totalSystem: Number,
    amount: Number,
    code: String,
    children: Array,
    updatingChildren: Boolean,
}, {
    timestamps: true
});
const DBAccountModel = mongoose.model(constants.mongoDb.collectionName.ACCOUNT, Account);
class AccountModel {
    static async save({address, ref, commission, refArray, totalSystem, amount, code, children, updatingChildren}) {
        if(!address){
            throw new Error("Address is required");
        }
        // get settle status obj
        const account = new DBAccountModel({
            address: address.toLowerCase(),
            ref: ref ? ref.toLowerCase() : 0,
            commission: commission ? commission : 0,
            refArray: refArray ? refArray.map(item => item.toLowerCase()) : [],
            totalSystem: totalSystem ? totalSystem : 0,
            amount: amount ? amount : 0,
            code: code ? code : randomCode(constants.DEFAULT_LENGTH_CODE),
            children: children ? children : [],
            updatingChildren: updatingChildren ? updatingChildren : false,
        });
        const result = await account.save();
        return result;
    }
    static async find(filter, options){
        return await DBAccountModel.find(filter, null, options).lean();
    }
    static async findOne(filter, options){
        return await DBAccountModel.findOne(filter, null, options).lean();
    }
    static async update(filter, update){
        return await DBAccountModel.updateMany(filter, update);
    }
    static async updateOne(filter, update){
        return await DBAccountModel.updateOne(filter, update);
    }
}
module.exports = AccountModel;