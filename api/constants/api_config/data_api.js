const data_api = {
    DOMAIN_NAME: "https://api-football-v1.p.rapidapi.com/v3",
    endpoints: {
        FIXTURES: "/fixtures",
        ODD_BET: "/odds/bets",
        ODDS: "/odds",
    },
    headers: {
        X_RAPID_API_HOST: "X-RapidAPI-Host",
        X_RAPID_API_KEY: "X-RapidAPI-Key",
    },
    method: {
        GET: "get",
        POST: "post",
    },
    API_HOST: "api-football-v1.p.rapidapi.com",
};
module.exports = data_api
