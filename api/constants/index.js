const betTypeRaw = require('./business/bet_type_raw');
const market = require('./business/market');
const games = require('./business/games');
const match_statuses = require('./business/match_statuses');
const { typeAppliesTo, betTypeMapped } = require('./business/bet_type_mapped');
const data_api = require('./api_config/data_api');
const provider = require('./provider');
const GTokenABI = require('./contracts/abi/GTokenABI');
const GTokenAddress = require('./contracts/address/GTokenAddress');
const enviroment = "testnet";
const constants = {
    contracts: {
        GTOKEN: {
            address: GTokenAddress?.[enviroment],
            abi: JSON.parse(GTokenABI)
        }
    },
    mongoDb: {
        collectionName: {
            BET_PLACED_HISTORY: "bet_placed_histories_alpha_v1",
            ACCOUNT: "accounts_alpha_v1",
        },
        DB_NAME: "sportofi_db",
        configs: {
            USERNAME: "metapolis",
            CLUSTER: "clusterawsmetaposlis.kzpl3ab"
        }
    },
    sides: {
        HOME: 0,
        DRAW: 1,
        AWAY: 2,
    },
    settleStatus: {
        FIRST_HALF: 1,
        SECOND_HALF: 2,
        FULL_MATCH: 3,
    },
    data_api,
    DEFAULTS: {
        BOOKMARKER: {
            code: 1,
            name: "10Bet",
        },
        BET_TYPE: {
            code: 1,
            name: "Match Winner",
        }
    },
    ADDRESS_DEFAULT_REFERRAL: "0x3010350A72feC09a054c38F0938dE9772DF308A6",
    DEFAULT_LENGTH_CODE: 6,
    market,
    betTypeMapped,
    betTypeRaw,
    games,
    match_statuses,
    typeAppliesTo,
    provider
}
module.exports = constants