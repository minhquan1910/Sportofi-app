const typeAppliesTo = {
    WORDS: 1,
    NUMBERS: 2,
};
const betTypeMapped = {
    WIN: {
        id: 0,
        code: "win",
        name: "Win",
        applies: ["home", "away"],
        typeApply: typeAppliesTo.WORDS,
    },
    DRAW: {
        id: 1,
        code: "draw",
        name: "Draw",
        applies: ["draw"],
        typeApply: typeAppliesTo.WORDS,
    },
    UNDER: {
        id: 2,
        code: "under",
        name: "Under",
        applies: ["under"],
        typeApply: typeAppliesTo.WORDS,
    },
    OVER: {
        id: 3,
        code: "over",
        name: "Over",
        applies: ["over"],
        typeApply: typeAppliesTo.WORDS,
    },
    CORRECT_SCORE: {
        id: 4,
        code: "correct_score",
        name: "Correct Score",
        typeApply: typeAppliesTo.NUMBERS,
    },
    TOTAL: {
        id: 5,
        code: "total",
        name: "Total",
        applies: ["total"],
        typeApply: typeAppliesTo.NUMBERS,
    },
    HANDICAP: {
        id: 6,
        code: "handicap",
        name: "Handicap",
        typeApply: typeAppliesTo.WORDS,
    },
};
module.exports = {
    betTypeMapped,
    typeAppliesTo,
} ;
