const market = {
    FIRST_HALF: 1,
    SECOND_HALF: 2,
    FULL_MATCH: 3,
    EXTRA_TIME: 4,
    PENALTY: 5,
}
module.exports = market