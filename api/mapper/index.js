const constants = require('../constants');
const mapBetTypeObj = (betTypeCode) => {
    console.log("Map bet type obj")
    console.log("Bet type code", betTypeCode);
    const betTypeMapped = constants.betTypeMapped;
    const betTypeKey = Object.keys(betTypeMapped).find(key => betTypeMapped[key].id === betTypeCode);
    const betTypeObj = betTypeMapped[betTypeKey];
    return betTypeObj;
}
const mapSideObj = (sideCode, betType, teams) => {
    console.log("Map side obj")
    console.log("Side code", sideCode);
    console.log("Bet type", betType);
    console.log("Teams", teams);
    const { WIN, DRAW } = constants.betTypeMapped;
    const { HOME } = constants.sides;
    let sideObj = {};
    switch (betType.id) {
        case WIN.id:
            if (sideCode === HOME) {
                sideObj = {
                    code: betType.id,
                    name: teams?.home?.name,
                }
            }
            else {
                sideObj = {
                    code: betType.id,
                    name: teams?.away?.name,
                }
            }
            break;
        case DRAW.id:
            sideObj = {
                code: betType.id,
                name: "Draw",
            }
            break;
        default:
            console.log("DEFAULT");
            sideCode = {
                code: betType.id,
                name: "",
            }
            break;
    }
    return sideObj
}
module.exports = {
    mapSideObj,
    mapBetType: mapBetTypeObj,
};