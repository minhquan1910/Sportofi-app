const constants = require("../constants");
const mongoose = require("mongoose");
class DatabaseImpl {
    connect = async () => {
        if(mongoose.connection.readyState !== 1){
            console.log("Create new connection");
            const username = encodeURIComponent(constants.mongoDb.configs.USERNAME);
            const password = encodeURIComponent(process.env.MONGODB_PASSWORD);
            const cluster = encodeURIComponent(constants.mongoDb.configs.CLUSTER);
            const dbName = encodeURIComponent(constants.mongoDb.DB_NAME);
            const uri = `mongodb+srv://${username}:${password}@${cluster}.mongodb.net/${dbName}?retryWrites=true&w=majority`;
            console.log("uri: " + uri);
            await mongoose.connect(uri);
            console.log(`New connection created ${mongoose.connection.readyState}`);
            return mongoose
        }
        else{
            console.log("Use old connection");
            return mongoose;
        }
    }
    close = async () => {
        if(mongoose.connection.readyState === 1){
            console.log("Close connection");
            await mongoose.connection.close();
            console.log(`Connection closed ${mongoose.connection.readyState}`);
        }
    }

}
module.exports = DatabaseImpl;